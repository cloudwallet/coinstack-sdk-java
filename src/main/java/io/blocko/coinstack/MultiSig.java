package io.blocko.coinstack;

import java.util.ArrayList;
import java.util.List;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.ScriptException;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.util.Codecs;

/**
 * Redeem script to make a Multisig transaction
 * Generate redeem scipt and extract Bitcoin address from redeem scipt
 */
public class MultiSig {
	
	/**
	 * create Redeem script from given public keys
	 * n of m Multisig redeem script means n keys are required to 
	 * redeem the transaction which is consist of m public keys
	 * 
	 * @param threshold
	 *            threshold is a number of private keys which
	 *            are required to redeem the transaction        
	 * @param pubkeys
	 *            list of public keys to make a redeem script
	 * @return Hex encoded redeem scipt     	           
	 */
	public static String createRedeemScript(int threshold, List<byte[]> pubkeys) {
		List<ECKey> eckeys = new ArrayList<ECKey>();
		for (int i = 0; i < pubkeys.size(); i++) {
			eckeys.add(ECKey.fromPublicOnly(pubkeys.get(i)));
		}
		Script sc = ScriptBuilder.createRedeemScript(threshold, eckeys);

		return Codecs.HEX.encode(sc.getProgram());
	}

	/**
	 * extract Bitcoin address from redeem script(String object)
	 * 
	 * @param redeemScript
	 *            given redeem script which is generated by createRedeemScipt       
	 * @param isMainNet
	 *            true is Mainnet, false is Testnet
	 * @return Bitcoin address   	           
	 */
	
	public static String createAddressFromRedeemScript(Script redeemScript, boolean isMainNet) {
		Script sc = ScriptBuilder.createP2SHOutputScript(redeemScript);
		Address address = Address.fromP2SHScript(isMainNet ? MainNetParams.get() : RegTestParams.get(), sc);
		return address.toString();
	}

	/**
	 * extract Bitcoin address from redeem script(Script object)
	 * 
	 * @param redeemScript
	 *            given redeem script which is generated by createRedeemScipt       
	 * @param isMainNet
	 *            true is Mainnet, false is Testnet
	 * @return Bitcoin address   	           
	 */
	public static String createAddressFromRedeemScript(String redeemScript, boolean isMainNet) throws MalformedInputException {
		Script redeem = null;
		String from = null;
		try {
			redeem = new Script(Codecs.HEX.decode(redeemScript));
		} catch (ScriptException e) {
			throw new MalformedInputException("Invalid redeem script: Parsing redeem script failed");
		} catch (RuntimeException e) {
			throw new MalformedInputException("Invalid redeem script: Parsing redeem script failed");
		}
		from = createAddressFromRedeemScript(redeem, isMainNet);
		return from.toString();
	}

}
