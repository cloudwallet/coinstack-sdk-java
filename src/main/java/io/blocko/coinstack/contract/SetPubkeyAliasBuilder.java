package io.blocko.coinstack.contract;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Hex;

import io.blocko.coinstack.AbstractTransactionBuilder;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.TransactionBuilder;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.permission.model.Commands;
import io.blocko.coinstack.permission.model.Constant;
import io.blocko.coinstack.util.Codecs;

public class SetPubkeyAliasBuilder extends AbstractTransactionBuilder{

	private String aliasName = null;
	private String base64Encodedpublickey = null;

	public SetPubkeyAliasBuilder() {
		super();
	}

	public void setAlias(String aliasName) {
		this.aliasName = aliasName;
	}

	public void setPublickey(String base64Encodedpublickey) {
		this.base64Encodedpublickey = base64Encodedpublickey;
	}

	public String buildTransaction(CoinStackClient coinstack, String privateKeyWIF)
			throws IOException, CoinStackException {
		if (this.aliasName == null) {
			throw new MalformedInputException("aliasName is empty");
		}
		if (this.base64Encodedpublickey == null) {
			throw new MalformedInputException("publickey is empty");
		}
		
		allowLargePayload(true);
		shuffleOutputs(false);
		setFee(this.fee);

		// generate transaction OP_RETURN data's header
		StringBuffer hexBuffer = new StringBuffer();
		hexBuffer.append(Constant.Magic); // magic
		hexBuffer.append(Constant.Version); // version
		hexBuffer.append(Commands.SetAliasOp); // set alias command

		byte[] markerData = null;

		byte[] prefixData;
		try {
			prefixData = Hex.decodeHex(hexBuffer.toString().toCharArray());
		} catch (org.apache.commons.codec.DecoderException e1) {
			throw new MalformedInputException("Fail to decode cmd header: " + e1.getMessage());
		}

		// convert and append data
		byte[] pubkeyData = Codecs.BASE64.decode(base64Encodedpublickey);
		if (pubkeyData.length != 33)
			throw new MalformedInputException("Invalid Public key: Length of compressed public key must be 33 byte.");

		byte[] aliasData = aliasName.getBytes(Charset.forName("UTF-8"));

		// convert to byte
		markerData = ByteBuffer.allocate(prefixData.length + pubkeyData.length + aliasData.length).put(prefixData)
				.put(pubkeyData).put(aliasData).array();

		setData(markerData);

		BuildResult buildResult = preBuild(coinstack, privateKeyWIF);
		
		return buildResult.getRawTxStr();
	}
}
