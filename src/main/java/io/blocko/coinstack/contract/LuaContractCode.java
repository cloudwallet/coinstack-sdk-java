package io.blocko.coinstack.contract;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.exception.SecurityException;
import io.blocko.coinstack.util.Codecs;
import io.blocko.coinstack.util.Crypto;

public class LuaContractCode extends AbstractContractCode {
	/********************
	 * !!!WARNING!!! DON'T change the byte order.
	 ********************/
	public static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;

	/********************
	 * !!!WARNING!!! DON'T change an enumeration value. MUST add a new type.
	 ********************/
	public enum ParamType {
		// if need to add more type, insert after the last.
		// @formatter:off
		INT(1), 
		LONG(2), 
		DOUBLE(3), 
		STRING(4),
		MAX(0);
		// @formatter:on

		private int value = 0;

		ParamType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	// parameterized code
	private String code;

	// parameters map: [name, index list]
	// the same name parameter could be located multiple times.
	private HashMap<String, ArrayList<Integer>> paramMap;
	// total parameters count
	private int totalParamCnt;
	// parameter list contained user input
	private ArrayList<Parameter> paramList;

	public LuaContractCode(String code) throws CoinStackException {
		super();

		this.code = code;

		// initialize parameters map
		this.paramMap = new HashMap<String, ArrayList<Integer>>();

		// find parameters and parameterize code
		this.totalParamCnt = parameterizeCode();

		// initialize parameter list
		this.paramList = new ArrayList<LuaContractCode.Parameter>();
		for (int i = 0; i < this.totalParamCnt; i++) {
			paramList.add(null);
		}
	}

	private boolean isWhitespace(char ch) {
		if (ch == ' ' || ch == '\t' || ch == '\n')
			return true;
		return false;
	}

	/**
	 * find parameters and parameterize code
	 * 
	 * @return total parameters count
	 * @throws CoinStackException
	 */
	private int parameterizeCode() throws CoinStackException {
		char[] codeCharArr = code.toCharArray();
		StringBuilder newCode = new StringBuilder(code.length());
		int paramIdx = 0;
		for (int i = 0; i < codeCharArr.length; i++) {
			char ch = codeCharArr[i];

			switch (ch) {
			case '"':
				newCode.append(ch);

				// skip all characters inside double quotations
				for (int j = i + 1; j < codeCharArr.length; j++) {
					newCode.append(codeCharArr[j]);

					if (codeCharArr[j] == '"') {
						i = j;
						break;
					}
				}
				break;
			case '\'':
				newCode.append(ch);

				// skip all characters inside single quotations
				for (int j = i + 1; j < codeCharArr.length; j++) {
					newCode.append(codeCharArr[j]);

					if (codeCharArr[j] == '\'') {
						i = j;
						break;
					}
				}
				break;
			case '?':
				newCode.append(ch);

				if (i != 0) {
					// check previous character
					char prev = codeCharArr[i - 1];
					if (!isWhitespace(prev) && prev != ',' && prev != '(')
						throw new MalformedInputException("fail to parse a parameter"
								+ ": syntax error of a parameter variable at " + (i - 1));
				}
				if (i != codeCharArr.length) {
					// check next character
					char after = codeCharArr[i + 1];
					if (!isWhitespace(after) && after != ',' && after != ')')
						throw new MalformedInputException("fail to parse a parameter"
								+ ": syntax error of a parameter variable at " + (i + 1));
				}
				paramIdx++;
				break;
			case '$':
				newCode.append('?');

				if (i != 0) {
					// check previous character
					char prev = codeCharArr[i - 1];
					if (!isWhitespace(prev) && prev != ',' && prev != '(' && prev != '=')
						throw new MalformedInputException("fail to parse a parameter"
								+ ": syntax error of a parameter variable at " + (i - 1));
				}

				// extract a parameter name
				StringBuilder paramNameSB = new StringBuilder();
				for (int j = i + 1; j < codeCharArr.length; j++) {
					char ch2 = codeCharArr[j];

					// check end of parameter name
					if (isWhitespace(ch2) || ch2 == ',' || ch2 == ')' || ch2 == '(') {
						i = j - 1;
						break;
					}

					paramNameSB.append(ch2);
				}
				if (paramNameSB.length() == 0) {
					throw new MalformedInputException("fail to parse a parameter"
								+ ": syntax error of a parameter variable at " + i);
				}

				String paramName = paramNameSB.toString();
				if (!paramMap.containsKey(paramName)) {
					paramMap.put(paramName, new ArrayList<Integer>());
				}
				paramMap.get(paramName).add(paramIdx);
				paramIdx++;
				break;
			default:
				newCode.append(ch);
				break;
			}
		}
		code = newCode.toString();
		return paramIdx;
	}

	private ArrayList<Integer> getParamIdxListOfMap(String paramName) throws CoinStackException {
		if (!paramMap.containsKey(paramName)) {
			throw new MalformedInputException("ineligible parameter"
					+ ": cannot find a parameter: " + paramName);
		}
		return paramMap.get(paramName);
	}

	private void checkEligibleArg(int idx) throws CoinStackException {
		if (idx <= 0) {
			throw new MalformedInputException("ineligible parameter"
					+ ": the index should be bigger than 0");
		}

		if (totalParamCnt < idx) {
			throw new MalformedInputException("ineligible parameter"
					+ ": the index exceeded total size: " + totalParamCnt);
		}
	}

	private void setParamInternal(ParamType type, int idx, String value, boolean encrypt) throws CoinStackException {
		checkEligibleArg(idx);

		Parameter param = new Parameter();
		param.type = type;
		param.index = idx;
		param.isEncrypted = encrypt;
		param.value = value;

		paramList.set(idx - 1, param);
	}

	/**
	 * Set a value into parameter(s) with name.
	 * 
	 * @param name
	 *            parameter name
	 * @param value
	 *            input value
	 * @throws CoinStackException
	 */
	public void setParam(String name, long value) throws CoinStackException {
		setParam(name, value, false);
	}

	public void setParam(String name, double value) throws CoinStackException {
		setParam(name, value, false);
	}

	public void setParam(String name, int value) throws CoinStackException {
		setParam(name, value, false);
	}

	public void setParam(String name, String value) throws CoinStackException {
		setParam(name, value, false);
	}

	/**
	 * Set a value into parameter(s) with name. If want to encrypt the value of the
	 * parameter(s), set 'true' at the last.
	 * 
	 * @param name
	 *            parameter name
	 * @param value
	 *            input value
	 * @param encrypt
	 *            encrypt value, or not
	 * @throws CoinStackException
	 */
	public void setParam(String name, long value, boolean encrypt) throws CoinStackException {
		for (Integer i : getParamIdxListOfMap(name)) {
			setParam(i + 1, value, encrypt);
		}
	}

	public void setParam(String name, double value, boolean encrypt) throws CoinStackException {
		for (Integer i : getParamIdxListOfMap(name)) {
			setParam(i + 1, value, encrypt);
		}
	}

	public void setParam(String name, int value, boolean encrypt) throws CoinStackException {
		for (Integer i : getParamIdxListOfMap(name)) {
			setParam(i + 1, value, encrypt);
		}
	}

	public void setParam(String paramName, String value, boolean encrypt) throws CoinStackException {
		setParam(paramName, value, encrypt, false);
	}

	/**
	 * Set a value into parameter(s) with name. If want to encrypt the value of the
	 * parameter(s), set 'true' for 'encrypt' parameter. If want to set the value
	 * without quotations, set 'true' for 'noquot' parameter.
	 * 
	 * ex) setParam(1, "123", false, true); getParam(1) --> 123
	 * 
	 * ex) setParam(1, "123", false, false); getParam(1) --> "123"
	 * 
	 * @param name
	 *            parameter name
	 * @param value
	 *            input value
	 * @param encrypt
	 *            encrypt value, or not
	 * @param noquot
	 *            except quotations, or not
	 * @throws CoinStackException
	 */
	public void setParam(String name, String value, boolean encrypt, boolean noquot) throws CoinStackException {
		for (Integer i : getParamIdxListOfMap(name)) {
			setParam(i + 1, value, encrypt, noquot);
		}
	}

	/**
	 * Set a value into the 'idx'-th parameter, index starts at 1.
	 * 
	 * @param idx
	 *            index of parameter(s)
	 * @param value
	 *            input value
	 * @throws CoinStackException
	 */
	public void setParam(int idx, long value) throws CoinStackException {
		setParam(idx, value, false);
	}

	public void setParam(int idx, double value) throws CoinStackException {
		setParam(idx, value, false);
	}

	public void setParam(int idx, int value) throws CoinStackException {
		setParam(idx, value, false);
	}

	public void setParam(int idx, String value) throws CoinStackException {
		setParam(idx, value, false);
	}

	/**
	 * Set a value into the 'idx'-th parameter, index starts at 1. If want to
	 * encrypt the value of the parameter, set 'true' at the last.
	 * 
	 * @param idx
	 *            index of parameter(s)
	 * @param value
	 *            input value
	 * @param encrypt
	 *            encrypt value, or not
	 * @throws CoinStackException
	 */
	public void setParam(int idx, long value, boolean encrypt) throws CoinStackException {
		setParamInternal(ParamType.LONG, idx, Long.toString(value), encrypt);
	}

	public void setParam(int idx, double value, boolean encrypt) throws CoinStackException {
		setParamInternal(ParamType.DOUBLE, idx, Double.toString(value), encrypt);
	}

	public void setParam(int idx, int value, boolean encrypt) throws CoinStackException {
		setParamInternal(ParamType.INT, idx, Integer.toString(value), encrypt);
	}

	public void setParam(int idx, String value, boolean encrypt) throws CoinStackException {
		setParam(idx, value, encrypt, false);
	}

	/**
	 * Set a value into the 'idx'-th parameter, index starts at 1. If want to
	 * encrypt the value of the parameter, set 'true' for 'encrypt' parameter. If
	 * want to set the value without quotations, set 'true' for 'noquot' parameter.
	 * 
	 * @param name
	 *            parameter name
	 * @param value
	 *            input value
	 * @param encrypt
	 *            encrypt value, or not
	 * @param noquot
	 *            set quotations, or not
	 * @throws CoinStackException
	 */
	public void setParam(int idx, String value, boolean encrypt, boolean noquot) throws CoinStackException {
		if (noquot) {
			setParamInternal(ParamType.STRING, idx, value, encrypt);
		} else {
			setParamInternal(ParamType.STRING, idx, "\"" + value + "\"", encrypt);
		}
	}

	/**
	 * get an instance of the 'Parameter' class at the 'idx'-th parameter.
	 * 
	 * @param idx
	 *            index of parameter
	 * @return instance of 'Parameter' class
	 */
	public Parameter getParam(int idx) {
		if (idx <= 0) {
			return null;
		}
		return paramList.get(idx - 1);
	}

	/**
	 * get instances of the 'Parameter' class with the name.
	 * 
	 * @param name
	 *            parameter name
	 * @return instance array of 'Parameter' class
	 */
	public Parameter[] getParams(String name) {
		if (!paramMap.containsKey(name)) {
			return null;
		}

		ArrayList<Integer> idxList = paramMap.get(name);
		if (idxList == null || idxList.isEmpty()) {
			return null;
		}

		Parameter[] paramArr = new Parameter[idxList.size()];
		for (int i = 0; i < idxList.size(); i++) {
			paramArr[i] = paramList.get(idxList.get(i));
		}
		return paramArr;
	}

	public ArrayList<Parameter> getParams() throws CoinStackException {
		return paramList;
	}

	public String getCode() throws CoinStackException {
		return this.code;
	}

	/**
	 * get parameters header bytes and byte-chunk
	 * 
	 * @return ParamsBytes contains header bytes and byte-chunk
	 * @throws CoinStackException
	 */
	private ParamsBytes getParamsBytes() throws CoinStackException {
		int paramBufSize = 0;
		int eparamBufSize = 0;
		ArrayList<byte[]> params = new ArrayList<byte[]>();
		ArrayList<byte[]> eparams = new ArrayList<byte[]>();

		for (int i = 0; i < paramList.size(); i++) {
			Parameter param = paramList.get(i);
			if (param == null) {
				throw new MalformedInputException("ineligible parameter(s)"
						+ ": miss parameter at: " + i);
			}

			// get bytes of parameter
			byte[] bytes = param.toBytes();

			if (isWholeEnc) {
				// if 'isWholeEnc', ignore encryption of the parameter.
				params.add(bytes);
				paramBufSize += bytes.length;
			} else {
				if (!param.isEncrypted) {
					params.add(bytes);
					paramBufSize += bytes.length;
				} else {
					eparams.add(bytes);
					eparamBufSize += bytes.length;
				}
			}
		}

		// for non-encrypted parameters
		ByteBuffer bbuf = ByteBuffer.allocate(paramBufSize);
		int paramsCnt = 0;
		for (byte[] bytes : params) {
			bbuf.put(bytes);
			paramsCnt++;
		}
		byte[] paramsBytes = bbuf.array();

		// for encrypted parameters
		byte[] eparamsBytes = null;
		int eparamsCnt = 0;
		if (!eparams.isEmpty()) {
			if (eck == null) {
				throw new SecurityException("fail to encrypt parameters: need password to encrypt");
			}

			bbuf = ByteBuffer.allocate(eparamBufSize);
			for (byte[] bytes : eparams) {
				bbuf.put(bytes);
				eparamsCnt++;
			}

			// encrypt byte buffer
			try {
				eparamsBytes = Crypto.encryptAES256CBC(bbuf.array(), eck);
			} catch (GeneralSecurityException e) {
				throw new SecurityException("fail to encrypt parameters:" + e.getMessage());
			}
		}

		// generate a header
		bbuf = ByteBuffer.allocate(2+2);
		bbuf.order(BYTE_ORDER);
		bbuf.putShort((short) paramsCnt);
		bbuf.putShort((short) eparamsCnt);
		byte[] header = bbuf.array();


		// size = paramsLen(4) + paramsBytes + eparamsLen(4) + eparamsBytes
		int size = 0;
		if (paramsCnt > 0) {
			size += 4 + paramsBytes.length;
		}
		if (eparamsCnt > 0) {
			size += 4 + eparamsBytes.length;
		}

		// generate a byte-chunk
		bbuf = ByteBuffer.allocate(size);
		bbuf.order(BYTE_ORDER);
		// set parameters chunk
		if (paramsCnt > 0) {
			bbuf.putInt(paramsBytes.length);
			bbuf.put(paramsBytes);
		}
		// set encrypted parameters chunk
		if (eparamsCnt > 0) {
			bbuf.putInt(eparamsBytes.length);
			bbuf.put(eparamsBytes);
		}
		byte[] bchunk = bbuf.array();

		return new ParamsBytes(header, bchunk);
	}
	private class ParamsBytes {
		byte[] header;
		byte[] bchunk;
		ParamsBytes(byte[] header, byte[] bchunk) {
			this.header = header;
			this.bchunk = bchunk;
		}
	}

	/**
	 * generate a byte-chunk with the code, parameters, and encrypted parameters.
	 * 
	 * @return byte-chunk
	 * @throws CoinStackException
	 */
	public byte[] toBytes() throws CoinStackException {
		byte[] codeBytes = getCode().getBytes(Charset.forName("UTF-8"));
		ParamsBytes pb = getParamsBytes();
		byte[] paramsHeader = pb.header;
		byte[] paramsBChunk = pb.bchunk;

		// total size = codeLen(4) + codeBytes + paramsHeader + parametersChunk
		int size = 4 + codeBytes.length;
		size += (paramsHeader == null) ? 0 : paramsHeader.length;
		size += (paramsBChunk == null) ? 0 : paramsBChunk.length;

		// generate a byte-chunk
		ByteBuffer bbuf = ByteBuffer.allocate(size);
		bbuf.order(BYTE_ORDER);
		// set parameters header
		bbuf.put(paramsHeader);
		// set code
		bbuf.putInt(codeBytes.length);
		bbuf.put(codeBytes);
		// set parameters chunk
		if (paramsBChunk != null) {
			bbuf.put(paramsBChunk);
		}

		if (isWholeEnc) {
			// encrypt byte buffer
			try {
				return Crypto.encryptAES256CBC(bbuf.array(), eck);
			} catch (GeneralSecurityException e) {
				throw new SecurityException("fail to encrypt parameters: " + e.getMessage());
			}
		}

		return bbuf.array();
	}

	@Override
	public String toString() {
		try {
			byte[] bytes = toBytes();
			return toString(bytes, isWholeEnc);
		} catch (CoinStackException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String toString(byte[] bytes, boolean isWholeEnc) {
		StringBuilder sb = new StringBuilder();

		ByteBuffer bbuf = null;
		bbuf = ByteBuffer.wrap(bytes);
		bbuf.order(BYTE_ORDER);

		if (isWholeEnc) {
			return Codecs.BASE64.encode(bytes);
		}

		// number of non-encrypted parameters = 2 bytes
		int paramsCnt = bbuf.getShort();
		sb.append("Number of Non-encrypted Parameters: ").append(paramsCnt).append("\n");

		// number of encrypted parameters = 2 bytes
		int eparamsCnt = bbuf.getShort();
		sb.append("Number of Encrypted Parameters: ").append(eparamsCnt).append("\n");

		// read code
		int len = bbuf.getInt();
		byte[] code = new byte[len];
		bbuf.get(code);
		sb.append("Code:\n").append(new String(code)).append("\n");

		if (paramsCnt > 0) {
			// read non-encrypted parameters chunk length
			len = bbuf.getInt();

			// read non-encrypted parameters chunk
			bytes = new byte[len];
			bbuf.get(bytes);
			ByteBuffer argsBbuf = ByteBuffer.wrap(bytes);
			argsBbuf.order(BYTE_ORDER);

			// non-encrypted parameters
			for (int i = 0; i < paramsCnt; i++) {
				// read index
				int index = argsBbuf.getShort();
				// read type
				int type = argsBbuf.getShort();
				// read length of value
				len = argsBbuf.getInt();
				// read value
				byte[] value = new byte[len];
				argsBbuf.get(value);

				// parameter
				sb.append("Parameter [").append(index).append("] ");
				if (type == ParamType.INT.getValue()) {
					sb.append("INT = ");
				} else if (type == ParamType.LONG.getValue()) {
					sb.append("LONG = ");
				} else if (type == ParamType.DOUBLE.getValue()) {
					sb.append("DOUBLE = ");
				} else if (type == ParamType.STRING.getValue()) {
					sb.append("STRING = ");
				} else {
					// ERROR
					return null;
				}
				sb.append(new String(value)).append("\n");
			}
		}

		if (eparamsCnt > 0) {
			// read encrypted parameters chunk length
			len = bbuf.getInt();

			// read encrypted parameters chunk
			bytes = new byte[len];
			bbuf.get(bytes);

			// encrypted parameters chunk
			sb.append("Encrypted Parameters Chunk(BASE64):\n");
			sb.append(Codecs.BASE64.encode(bytes));
		}

		return sb.toString();
	}

	/**
	 * Parameter is a class for a parameter in a LUA smart contract code.
	 */
	public class Parameter {
		private boolean isEncrypted;
		private ParamType type;
		private int index;
		private String value;

		public boolean isEncrypted() {
			return isEncrypted;
		}

		public ParamType getType() {
			return type;
		}

		public int getIndex() {
			return index;
		}

		public String getValue() {
			return value;
		}

		public byte[] toBytes() {
			byte[] vbytes = value.getBytes(Charset.forName("UTF-8"));

			// buffer size = index (2) + type (2) + length (4) + value
			int size = 2 + 2 + 4 + vbytes.length;

			ByteBuffer bbuf = ByteBuffer.allocate(size);
			bbuf.order(BYTE_ORDER);
			// index (2)
			bbuf.putShort((short) index);
			// type (2)
			bbuf.putShort((short) type.value);
			// value length (4)
			bbuf.putInt(vbytes.length);
			// value
			bbuf.put(vbytes);

			return bbuf.array();
		}
	}
}