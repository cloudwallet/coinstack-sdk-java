package io.blocko.coinstack.contract;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.util.BlockoRandom;

public abstract class AbstractContractCode {
	// encrypt whole bytes (code + parameters), or not
	protected boolean isWholeEnc;
	// the encryption key (ECK)
	protected String eck;

	protected AbstractContractCode() {
		this.isWholeEnc = false;
		this.eck = null;
	}

	public String getECK() {
		return eck;
	}

	public void setECK(String eck) {
		this.eck = eck;
	}
	
	public void setRandomEck() {
		BlockoRandom brand = new BlockoRandom(32);
		this.eck = brand.getString();
	}

	public boolean isWholeEnc() {
		return isWholeEnc;
	}

	public void setWholeEnc(boolean isWholeEnc) {
		this.isWholeEnc = isWholeEnc;
	}

	/**
	 * Should implement to generate a byte-chunk with code, parameters, and so on.
	 * 
	 * If support the encryption, should implement in here.
	 * 
	 * @return
	 * @throws CoinStackException
	 */
	public abstract byte[] toBytes() throws CoinStackException;
}
