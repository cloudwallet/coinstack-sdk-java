package io.blocko.coinstack.backendadaptor;

import java.io.IOException;
import java.util.Map;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Block;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.ContractResult;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack.model.Subscription;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.permission.model.Grantee;
import io.blocko.coinstack.permission.model.Role;

public abstract class AbstractCoinStackAdaptor {

	public abstract void init();

	public abstract void fini();

	public abstract boolean isMainnet();

	public abstract int getBestHeight() throws IOException, CoinStackException;

	public abstract String getBestBlockHash() throws IOException, CoinStackException;

	public abstract BlockchainStatus getBlockchainStatus() throws IOException, CoinStackException;

	public abstract Block getBlock(String blockId) throws IOException, CoinStackException;

	public abstract String[] getBlockHash(int blockHeight) throws IOException, CoinStackException;

	public abstract Transaction getTransaction(String transactionId) throws IOException, CoinStackException;

	public abstract String getRawTransaction(String transactionId, String format) throws IOException, CoinStackException;

	public abstract String[] getTransactions(String address) throws IOException, CoinStackException;

	public abstract String[] getTransactions(String address, int skip, int limit, int confirmed)
			throws IOException, CoinStackException;

	public abstract long getBalance(String address) throws IOException, CoinStackException;

	public abstract Output[] getUnspentOutputs(String address, long amount) throws IOException, CoinStackException;

	public abstract void sendTransaction(String rawTransaction) throws IOException, CoinStackException;

	public abstract ContractResult queryContract(String contractId, String type, String body)
			throws IOException, CoinStackException;

	public abstract Subscription[] listSubscriptions() throws IOException, CoinStackException;

	public abstract void deleteSubscription(String id) throws IOException, CoinStackException;

	public abstract String addSubscription(Subscription newSubscription) throws IOException, CoinStackException;

	public abstract byte[] getNodegroupPublicKey() throws IOException, CoinStackException;

	@Deprecated
	public abstract String stampDocument(String hash) throws IOException, CoinStackException;

	@Deprecated
	public abstract Stamp getStamp(String stampId) throws IOException, CoinStackException;

	// for alias management
	public abstract byte[] getPublickey(String aliasName) throws IOException, CoinStackException;

	public abstract Map<String, byte[]> listAllPubkey() throws IOException, CoinStackException;

	// for permission management
	public abstract Map<String, Role> listAllRole() throws IOException, CoinStackException;

	public abstract Role getRole(String address) throws IOException, CoinStackException;

	// to get permissions which is turned on server
	public abstract Role getEnabledRole() throws IOException, CoinStackException;

	public abstract Grantee[] getGrantees(String contractId) throws IOException, CoinStackException;
}
