package io.blocko.coinstack.backendadaptor;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import javax.websocket.CloseReason.CloseCodes;

import org.json.JSONException;
import org.json.JSONObject;

import io.blocko.coinstack.AbstractEndpoint;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.InvalidResponseException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.exception.TimeoutException;
import io.blocko.coinstack.model.Block;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.ContractBody;
import io.blocko.coinstack.model.ContractResult;
import io.blocko.coinstack.model.CredentialsProvider;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack.model.Subscription;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.util.JsonToModel;

@ClientEndpoint
public class WebsocketAdaptor extends CoreBackEndAdaptor {

	static final String JSONKEY_THREAD_ID = "ti";
	static final String JSONKEY_CMD = "cmd";
	static final String JSONKEY_STATUS = "st";
	static final String JSONKEY_RESULT = "result";
	static final String JSONKEY_BLOCKHASH = "blockhash";
	static final String JSONKEY_TXHASH = "txhash";
	static final String JSONKEY_ADDRESS = "address";
	static final String JSONKEY_AMOUNT = "amount";
	static final String JSONKEY_RAWTX = "rawtx";
	static final String JSONKEY_QUERY = "query";
	static final String JSONKEY_QUERY_TYPE = "type";
	static final String JSONKEY_QUERY_BODY = "body";
	
	static class CMD {
		public static final int FETCH_BLOCKCHAIN_STATUS = 1;
		public static final int FETCH_BLOCK_TRANSACTIONS = 2;
		public static final int FETCH_BLOCK = 3;
		public static final int FETCH_TRANSACTION = 4;
		public static final int FETCH_TRANSACTION_HISTORY = 5;
		public static final int FETCH_BALANCE = 6;
		public static final int FETCH_UNSPENTOUTPUTS = 7;
		public static final int PUSH_TRANSACTION = 8;
		public static final int FETCH_CONTRACT_STATUS = 9;
		public static final int QUERY_CONTRACT = 10;
	}

	private URI endpointUri = null;
	private Session userSession = null;
	private long timeoutMil = 5000; // FIXME we have no construct for setting
									// timeout
	private ConcurrentHashMap<Long, JSONObject> threadidResultMap = new ConcurrentHashMap<Long, JSONObject>();

	public WebsocketAdaptor(CredentialsProvider credentialsProvider, AbstractEndpoint endpoint) {
		super(credentialsProvider, endpoint);
		try {
			if (endpoint.endpoint().endsWith("/"))
				this.endpointUri = new URI(endpoint.endpoint() + "sock");
			else
				this.endpointUri = new URI(endpoint.endpoint() + "/sock");

		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void init() {
		super.init();
		WebSocketContainer container = null;

		container = ContainerProvider.getWebSocketContainer();

		try {
			container.connectToServer(this, this.endpointUri);
		} catch (DeploymentException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void fini() {
		try {
			this.userSession.close(
					new CloseReason(CloseCodes.NORMAL_CLOSURE, "disconnect"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		super.fini();
	}

	/**
	 * Callback hook for Connection open events.
	 *
	 * @param userSession
	 *            the userSession which is opened.
	 */
	@OnOpen
	public void onOpen(Session userSession) {
		this.userSession = userSession;
		this.userSession.getAsyncRemote().setSendTimeout(this.timeoutMil);
	}

	/**
	 * Callback hook for Connection close events.
	 *
	 * @param userSession
	 *            the userSession which is getting closed.
	 * @param reason
	 *            the reason for connection close
	 */
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		this.userSession = null;
		threadidResultMap.clear();
	}

	/**
	 * Callback hook for Message Events. This method will be invoked when a
	 * client send a message.
	 *
	 * @param message
	 *            The text message
	 */
	@OnMessage
	public void onMessage(String message) {
		JSONObject resultJson;
		try {
			resultJson = new JSONObject(message);
			long thread_id = resultJson.getLong(WebsocketAdaptor.JSONKEY_THREAD_ID);

			threadidResultMap.put(thread_id, resultJson);
		} catch (JSONException e) {
			e.printStackTrace();
			// FIXME handling parsing error & global error
		}
	}

	@OnError
	public void onError(Session session, Throwable t) {
		// FIXME
		t.printStackTrace();
	}

	protected CoinStackException processError(JSONObject wsResJson, int status) 
			throws InvalidResponseException {
		try {
			JSONObject resJson = wsResJson.getJSONObject(JSONKEY_RESULT);
			return JsonToModel.parseError(resJson, status);
		} catch (JSONException e) {
			String resJsonString = wsResJson.toString();
			throw new InvalidResponseException("Invalid server response"
					+ ": failed to parse error information"
					+ ", response is " + JsonToModel.shorten(resJsonString),
					resJsonString);
		}
	}
	
	/**
	 * Send a message.
	 *
	 * @param jsonMessage
	 * @throws IOException
	 * @throws CoinStackException
	 * @throws InvalidResponseException
	 * @throws TimeoutException
	 */
	public JSONObject sendMessage(JSONObject jsonMessage)
			throws IOException, InvalidResponseException, CoinStackException, TimeoutException {
		JSONObject returnJsonObj = null;
		long my_tid = Thread.currentThread().getId();
		try {
			// attach current thread id to trace it's result
			// FIXME remove map use future
			jsonMessage.put(WebsocketAdaptor.JSONKEY_THREAD_ID, my_tid);

			// send request
			this.userSession.getAsyncRemote().sendText(jsonMessage.toString());
			long startTime = System.currentTimeMillis();

			// wait result from server
			while (System.currentTimeMillis() - startTime <= this.timeoutMil) {
				try {
					if (null != (returnJsonObj = this.threadidResultMap.remove(my_tid))) {
						//TODO check status and handle error
						int status = returnJsonObj.getInt(JSONKEY_STATUS);
						if(status != 200)
							throw processError(returnJsonObj, status);
						
						return returnJsonObj;
					}
					
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// do nothing
				}
			}

			// timeout
			throw new TimeoutException("Timeout to send a message through an websocket: " + jsonMessage.toString());

		} catch (JSONException e1) {
			throw new MalformedInputException("Invalid Json Input:" + e1.getMessage());
		}
	}

	@Override
	public BlockchainStatus getBlockchainStatus() throws IOException, CoinStackException {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put(JSONKEY_CMD, CMD.FETCH_BLOCKCHAIN_STATUS);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getBlockchainStatus Request");
		}
		try {
			JSONObject retJson = sendMessage(jsonObj);

			return JsonToModel.parseStatus(retJson.getJSONObject(JSONKEY_RESULT));

		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid blockchain status response: Parsing response failed");
		}
	}

	@Override
	public Block getBlock(String blockId) throws IOException, CoinStackException {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put(JSONKEY_CMD, CMD.FETCH_BLOCK);
			jsonObj.put(JSONKEY_BLOCKHASH, blockId);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getBlock Request");
		}
		try {
			JSONObject retJson = sendMessage(jsonObj);

			return JsonToModel.parseBlock(retJson.getJSONObject(JSONKEY_RESULT));

		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse block information");
		}
	}

	@Override
	public Transaction getTransaction(String transactionId) throws IOException, CoinStackException {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put(JSONKEY_CMD, CMD.FETCH_TRANSACTION);
			jsonObj.put(JSONKEY_TXHASH, transactionId);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getTransaction Request");
		}
		try {
			JSONObject retJson = sendMessage(jsonObj);

			return JsonToModel.parseTransaction(transactionId, retJson.getJSONObject(JSONKEY_RESULT));

		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid transaction response: Parsing response failed");
		}
	}

	@Override
	public String[] getTransactions(String address) throws IOException, CoinStackException {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put(JSONKEY_CMD, CMD.FETCH_TRANSACTION_HISTORY);
			jsonObj.put(JSONKEY_ADDRESS, address);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getTransactions Request");
		}
		try {
			JSONObject retJson = sendMessage(jsonObj);

			return JsonToModel.parseTransactionArray(retJson.getJSONArray(JSONKEY_RESULT));

		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse address history");
		}

	}

	@Override
	public long getBalance(String address) throws IOException, CoinStackException {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put(JSONKEY_CMD, CMD.FETCH_BALANCE);
			jsonObj.put(JSONKEY_ADDRESS, address);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getBalance Request");
		}
		try {
			JSONObject retJson = sendMessage(jsonObj);

			return JsonToModel.parseBalance(retJson.getJSONObject(JSONKEY_RESULT));
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid balance response: Parsing response failed");
		}
	}

	@Override
	public Output[] getUnspentOutputs(String address, long amount) throws IOException, CoinStackException {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put(JSONKEY_CMD, CMD.FETCH_UNSPENTOUTPUTS);
			jsonObj.put(JSONKEY_ADDRESS, address);
			if (amount > 0) {
				jsonObj.put(JSONKEY_AMOUNT, amount);
			}
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getBalance Request");
		}
		try {
			JSONObject retJson = sendMessage(jsonObj);

			return JsonToModel.parseUnspentOutputs(address, retJson.getJSONArray(JSONKEY_RESULT));
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid balance response: Parsing response failed");
		}
	}

	@Override
	public void sendTransaction(String rawTransaction) throws IOException, CoinStackException {
		JSONObject txRequest = new JSONObject();
		try {
			txRequest.put(JSONKEY_CMD, CMD.PUSH_TRANSACTION);
			txRequest.put(JSONKEY_RAWTX, rawTransaction);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate getBalance Request");
		}
		try {
			JSONObject retJson = sendMessage(txRequest);

			int status = retJson.getInt(JSONKEY_STATUS);

			if (status != 200) {
				throw processError(retJson.getString(JSONKEY_RESULT), status);
			}
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid balance response: Parsing response failed");
		}
	}

	@Override
	public String[] getTransactions(String address, int skip, int limit, int confirmed)
			throws IOException, CoinStackException {
		// FIXME
		throw new RuntimeException("Under Construction");
	}

	@Override
	public ContractResult queryContract(String contractId, String type, String body)
			throws IOException, CoinStackException {
		JSONObject txRequest = new JSONObject();
		try {
			txRequest.put(JSONKEY_CMD, CMD.QUERY_CONTRACT);
			txRequest.put(JSONKEY_ADDRESS, contractId);
			JSONObject contractBody = new JSONObject();
			contractBody.put(JSONKEY_QUERY_TYPE, type);
			contractBody.put(JSONKEY_QUERY_BODY, body);			
			txRequest.put(JSONKEY_QUERY, contractBody);
			
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid Request Type: failed to generate queryContract Request");
		}
		
		try {
			JSONObject retJson = sendMessage(txRequest);

			int status = retJson.getInt(JSONKEY_STATUS);

			if (status != 200) {
				throw processError(retJson.getString(JSONKEY_RESULT), status);
			}
			
			return new ContractResult(retJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid balance response: Parsing response failed");
		}
	}

	@Override
	public Subscription[] listSubscriptions() throws IOException, CoinStackException {
		// FIXME
		throw new RuntimeException("Under Construction");
	}

	@Override
	public void deleteSubscription(String id) throws IOException, CoinStackException {
		// FIXME
		throw new RuntimeException("Under Construction");
	}

	@Override
	public String addSubscription(Subscription newSubscription) throws IOException, CoinStackException {
		// FIXME
		throw new RuntimeException("Under Construction");
	}

	@Override
	public String stampDocument(String hash) throws IOException, CoinStackException {
		// FIXME
		throw new RuntimeException("Under Construction");
	}

	@Override
	public Stamp getStamp(String stampId) throws IOException, CoinStackException {
		// FIXME
		throw new RuntimeException("Under Construction");
	}

}
