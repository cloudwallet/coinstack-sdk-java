package io.blocko.coinstack.backendadaptor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.blocko.coinstack.AbstractEndpoint;
import io.blocko.coinstack.exception.AuthSignException;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.InvalidResponseException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.model.Block;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.ContractBody;
import io.blocko.coinstack.model.ContractResult;
import io.blocko.coinstack.model.CredentialsProvider;
import io.blocko.coinstack.model.Input;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Output.MetaData;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack.model.Subscription;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.openassets.model.AssetOutput;
import io.blocko.coinstack.permission.model.Grantee;
import io.blocko.coinstack.permission.model.Role;
import io.blocko.coinstack.util.Codecs;
import io.blocko.coinstack.util.HMAC;
import io.blocko.coinstack.util.HMAC.HMACSigningException;
import io.blocko.coinstack.util.JsonToModel;
import io.blocko.coinstack.util.PublicKeyVerifier;

public class CoreBackEndAdaptor extends AbstractCoinStackAdaptor {
	private static final String[] defaultProtocols = new String[] { "TLSv1" };
	private static final String[] defaultCipherSuites = new String[] { "TLS_DHE_RSA_WITH_AES_128_CBC_SHA" };
	private CloseableHttpClient httpClient;
	private AbstractEndpoint endpoint;
	private String[] protocols;

	private String[] cipherSuites;

	private CredentialsProvider credentialProvider;
	private String requestSignature = null;
	private boolean hmacSign = false;
	private boolean sslEnabled = false;

	public CoreBackEndAdaptor(CredentialsProvider credentialsProvider, AbstractEndpoint endpoint) {
		this(credentialsProvider, endpoint, defaultProtocols, defaultCipherSuites);
	}

	public CoreBackEndAdaptor(CredentialsProvider provider, AbstractEndpoint endpoint, String[] protocols,
			String[] cipherSuites) {
		super();
		this.credentialProvider = provider;
		this.endpoint = endpoint;
		this.protocols = protocols;
		this.cipherSuites = cipherSuites;
		this.hmacSign = false;
		if (this.credentialProvider != null && this.credentialProvider.getSecretKey() != null) {
			this.hmacSign = true;
		}
		this.sslEnabled = false;
		if (this.endpoint != null && this.endpoint.endpoint().startsWith("https://")) {
			this.sslEnabled = true;
		}
	}

	public String getLastRequestSignature() {
		return this.requestSignature;
	}

	private String requestGet(String url) throws IOException, CoinStackException {
		String response = null;
		CloseableHttpResponse res = null;
		try {
			HttpGet httpGet = new HttpGet(this.endpoint.endpoint() + url);
			if (hmacSign) {
				signRequest(httpGet);
			}
			res = httpClient.execute(httpGet);

			int status = res.getStatusLine().getStatusCode();
			HttpEntity entity = res.getEntity();
			response = EntityUtils.toString(entity, "UTF8");

			if (status != 200) {
				throw processError(response, status);
			}
		} finally {
			if (null != res)
				res.close();
		}
		return response;
	}

	private String requestPost(String url, String payloads) throws IOException, CoinStackException {
		String response = null;
		CloseableHttpResponse res = null;
		try {
			HttpPost httpPost = new HttpPost(this.endpoint.endpoint() + url);
			byte[] payload = payloads.getBytes("UTF8");
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			httpPost.setEntity(new ByteArrayEntity(payload));
			if (hmacSign) {
				signPostRequest(httpPost, payload);
			}
			res = httpClient.execute(httpPost);

			int status = res.getStatusLine().getStatusCode();
			HttpEntity entity = res.getEntity();
			response = EntityUtils.toString(entity, "UTF8");

			if (status != 200) {
				throw processError(response, status);
			}
		} finally {
			if (null != res)
				res.close();
		}
		return response;
	}

	private String requestDelete(String url) throws IOException, CoinStackException {
		String response = null;
		CloseableHttpResponse res = null;
		try {
			HttpDelete httpDelete = new HttpDelete(this.endpoint.endpoint() + url);
			if (hmacSign) {
				signRequest(httpDelete);
			}
			res = httpClient.execute(httpDelete);

			int status = res.getStatusLine().getStatusCode();
			response = EntityUtils.toString(res.getEntity(), "UTF8");

			if (status != 200) {
				throw processError(response, status);
			}
		} finally {
			if (null != res)
				res.close();
		}
		return response;
	}

	protected CoinStackException processError(String resJsonString, int status)
			throws InvalidResponseException {
		try {
			JSONObject resJson = new JSONObject(resJsonString);
			return JsonToModel.parseError(resJson, status);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response"
					+ ": failed to parse error information"
					+ ", response is " + JsonToModel.shorten(resJsonString),
					resJsonString);
		}
	}

	@Override
	public String addSubscription(Subscription newSubscription) throws IOException, CoinStackException {
		String payloads;
		try {
			payloads = newSubscription.toJsonString();
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid subscription: failed to marshal subscription object");
		}
		String resJsonString = requestPost("/subscriptions", payloads);

		// read result to extract id
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			return resJson.getString("id");
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid add subscription response: Parsing response failed");
		}
	}

	@Override
	public void deleteSubscription(String id) throws IOException, CoinStackException {
		requestDelete("/subscriptions/" + id);
	}

	@Override
	public void fini() {
		try {
			httpClient.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public long getBalance(String address) throws IOException, CoinStackException {
		String resJsonString = requestGet("/addresses/" + address + "/balance");
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			return JsonToModel.parseBalance(resJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid balance response: Parsing response failed");
		}
	}

	@Override
	public String getBestBlockHash() throws IOException, CoinStackException {
		return getBlockchainStatus().getBestBlockHash();
	}

	@Override
	public int getBestHeight() throws IOException, CoinStackException {
		return getBlockchainStatus().getBestHeight();
	}

	@Override
	public BlockchainStatus getBlockchainStatus() throws IOException, CoinStackException {
		String resJsonString = requestGet("/blockchain");
		try {
			JSONObject resJson = new JSONObject(resJsonString);
			return JsonToModel.parseStatus(resJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid blockchain status response: Parsing response failed");
		}
	}

	@Override
	public Block getBlock(String blockId) throws IOException, CoinStackException {
		String resJsonString = requestGet("/blocks/" + blockId);
		try {
			JSONObject resJson = new JSONObject(resJsonString);
			return JsonToModel.parseBlock(resJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse block information");
		}
	}

	@Override
	public String[] getBlockHash(int blockHeight) throws IOException, CoinStackException {
		if (blockHeight < 0) {
			throw new MalformedInputException("Invalid block height request: block height is greater than zero");
		}
		String resJsonString = requestGet("/blocks/height/" + blockHeight);
		List<String> blockhash = new LinkedList<String>();
		try {
			JSONArray resJson = new JSONArray(resJsonString);
			for (int i = 0; i < resJson.length(); i++) {
				blockhash.add(resJson.getString(i));
			}
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse block hash");
		}
		return blockhash.toArray(new String[0]);
	}

	@Override
	public Transaction getTransaction(String transactionId) throws IOException, CoinStackException {
		String resJsonString = requestGet("/transactions/" + transactionId);
		try {
			JSONObject resJson = new JSONObject(resJsonString);
			return JsonToModel.parseTransaction(transactionId, resJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid transaction response: Parsing response failed");
		}
	}
	@Override
	public String getRawTransaction(String transactionId, String format) throws IOException,
			CoinStackException {
		StringBuilder query = new StringBuilder();
		query.append("/transactions/");
		query.append(transactionId);
		if (format != null && !format.isEmpty()) {
			query.append("?format=").append(format);
		}
		String resp = requestGet(query.toString());
		return resp;
	}

	@Override
	public String[] getTransactions(String address) throws IOException, CoinStackException {
		return getTransactions(address, 0, -1, -1);
	}

	@Override
	public String[] getTransactions(String address, int skip, int limit, int confirmed)
			throws IOException, CoinStackException {
		String reqUrl = "/addresses/" + address + "/history";
		StringBuilder sb = new StringBuilder();
		if (skip != 0) {
			sb.append('?');
			sb.append("skip=").append(skip);
		}
		if (limit != -1) {
			sb.append((sb.length() == 0) ? '?' : '&');
			sb.append("limit=").append(limit);
		}
		if (confirmed != -1) {
			sb.append((sb.length() == 0) ? '?' : '&');
			sb.append("confirmed=").append(confirmed);
		}
		try {
			String reqParams = sb.toString();
			String resJsonString = requestGet(reqUrl + reqParams);
			JSONArray resJson = new JSONArray(resJsonString);
			return JsonToModel.parseTransactionArray(resJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse address history");
		}
	}

	@Override
	public Output[] getUnspentOutputs(String address, long amount) throws IOException, CoinStackException {
		String reqUrl = "/addresses/" + address + "/unspentoutputs";
		if (amount > 0) {
			reqUrl += "?amount="+amount;
		}
		try {
			String resJsonString = requestGet(reqUrl);
			JSONArray resJson = new JSONArray(resJsonString);
			return JsonToModel.parseUnspentOutputs(address, resJson);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse unspent output information");
		}
	}
	
	protected
	HttpClientConnectionManager
	createHttpClientConnectionManager() {
		RegistryBuilder<ConnectionSocketFactory> builder = RegistryBuilder.<ConnectionSocketFactory>create();
		if (this.endpoint.getPublicKey() != null) {
			// initialize public key verifier
			HostnameVerifier hostnameVerifier = new PublicKeyVerifier(this.endpoint);
			builder.register("https", new SSLConnectionSocketFactory(
					SSLContexts.createDefault(),
					protocols, cipherSuites, hostnameVerifier));
		}
		else if (this.sslEnabled) {
			builder.register("https", SSLConnectionSocketFactory.getSocketFactory());
		}
		builder.register("http", PlainConnectionSocketFactory.getSocketFactory());
		PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(builder.build());
		return connManager;
	}
	
	protected
	CloseableHttpClient
	createHttpClient() {
        HttpClientBuilder builder = HttpClientBuilder.create();
    	builder.setConnectionManager( createHttpClientConnectionManager() );
        return builder.build();
	}

	@Override
	public void init() {
	    httpClient = createHttpClient();
	}

	@Override
	public boolean isMainnet() {
		return this.endpoint.mainnet();
	}

	@Override
	public Subscription[] listSubscriptions() throws IOException, CoinStackException {
		String resJsonString = requestGet("/subscriptions");
		List<Subscription> subscriptions = new LinkedList<Subscription>();
		try {
			JSONArray resJson = new JSONArray(resJsonString);
			for (int i = 0; i < resJson.length(); i++) {
				JSONObject subscription = resJson.getJSONObject(i);
				subscriptions.add(new Subscription(subscription.getString("id"), subscription.getString("address"),
						subscription.getString("action")));

			}
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse subscription information");
		}
		return subscriptions.toArray(new Subscription[0]);
	}

	@Override
	public void sendTransaction(String rawTransaction) throws IOException, CoinStackException {
		JSONObject txRequest = new JSONObject();
		try {
			txRequest.put("tx", rawTransaction);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid subscription: failed to marshal subscription object");
		}
		String payloads = txRequest.toString();
		requestPost("/transactions", payloads);
	}

	@Override
	public ContractResult queryContract(String contractId, String type, String body)
			throws IOException, CoinStackException {
		String payloads = "{\"query\":" + body + "}";
		String resp = requestPost("/contracts/" + contractId + "/query", payloads);
		if (resp == null || resp.isEmpty()) {
			return null;
		}
		try {
			JSONObject json = new JSONObject(resp);
			return new ContractResult(json);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse query result");
		}
	}

	private void fetchRequestSignature(HttpRequestBase req) {
		this.requestSignature = null; // reset signature
		Header[] headers = req.getHeaders("Authorization");
		if (headers.length > 0) {
			try {
				String authHeader = headers[0].getValue();
				String result = Codecs.SHA256.digestEncodeBase64(authHeader.getBytes("UTF-8"));
				this.requestSignature = result;
			} catch (UnsupportedEncodingException e) {
				// do nothing
			}
		}
	}

	private void signPostRequest(HttpPost req, byte[] content) throws CoinStackException {
		try {
			String md5 = Codecs.MD5.digestEncodeBase64(content);
			req.addHeader(HMAC.CONTENT_MD5, md5);
			HMAC.signRequest(req, this.credentialProvider.getAccessKey(), this.credentialProvider.getSecretKey(),
					HMAC.generateTimestamp());
			fetchRequestSignature(req);
		} catch (HMACSigningException e) {
			throw new AuthSignException("Failed to sign auth header: failed to generate HMAC");
		}
	}

	private void signRequest(HttpRequestBase req) throws CoinStackException {
		try {
			HMAC.signRequest(req, this.credentialProvider.getAccessKey(), this.credentialProvider.getSecretKey(),
					HMAC.generateTimestamp());
			fetchRequestSignature(req);
		} catch (HMACSigningException e) {
			throw new AuthSignException("Failed to sign auth header: failed to generate HMAC");
		}
	}

	@Deprecated
	@Override
	public String stampDocument(String hash) throws IOException, CoinStackException {
		JSONObject txRequest = new JSONObject();
		try {
			txRequest.put("hash", hash);
		} catch (JSONException e) {
			throw new MalformedInputException("Invalid stamp request: failed to marshal stamp request");
		}
		String payloads = txRequest.toString();
		String resJsonString = requestPost("/stamps", payloads);
		// read result to extract id
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			return resJson.getString("stampid");
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid server response: failed to parse stamp respnose");
		}
	}

	@Deprecated
	@Override
	public Stamp getStamp(String stampId) throws IOException, CoinStackException {
		String resJsonString = requestGet("/stamps/" + stampId);
		try {
			JSONObject resJson = new JSONObject(resJsonString);
			String sts = resJson.optString("timestamp", null);
			Date ts = (sts == null) ? null : DateTime.parse(sts).toDate();
			return new Stamp(resJson.getString("tx"), resJson.getInt("vout"), resJson.getInt("confirmations"), ts,
					resJson.optString("hash", null));
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid stamp response: Parsing response failed");
		}
	}

	public AssetOutput[] getUnspentAssetOutputs(String address) throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getNodegroupPublicKey() throws IOException, CoinStackException {
		String resJsonString = requestGet("/contracts/nodegroupkey");
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			String pubkey64 = resJson.getString("pubkey");
			if (pubkey64 == null || pubkey64.equals("null"))
				return null;
			return Codecs.BASE64.decode(pubkey64);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid pubkey response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}

	@Override
	public byte[] getPublickey(String aliasName) throws IOException, CoinStackException {
		String resJsonString = requestGet("/aliases/" + aliasName);
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			String pubkey64 = resJson.getString("pubkey");
			if (pubkey64 == null || pubkey64.equals("null"))
				return null;
			return Codecs.BASE64.decode(pubkey64);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid publickey response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
    @Override
	public Map<String, byte[]> listAllPubkey() throws IOException, CoinStackException {
		String resJsonString = requestGet("/aliases");
		Map<String, byte[]> ret = new HashMap<String, byte[]>();
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			Iterator<String> iter = resJson.keys();
			while (iter.hasNext()) {
				String key = iter.next();
				byte[] value = Codecs.BASE64.decode(resJson.getString(key));

				ret.put(key, value);
			}

			return ret;
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid list all publickkey response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
    @Override
	public Map<String, Role> listAllRole() throws IOException, CoinStackException {
		String resJsonString = requestGet("/roles");
		Map<String, Role> ret = new HashMap<String, Role>();

		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			Iterator<String> iter = resJson.keys();
			while (iter.hasNext()) {
				String key = iter.next();
				int value = resJson.getInt(key);

				ret.put(key, new Role((byte) value));
			}

			return ret;
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid list all permission response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}

	@Override
	public Role getRole(String address) throws IOException, CoinStackException {
		String resJsonString = requestGet("/roles/" + address);
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			int permission = resJson.getInt("permission");

			return new Role(permission);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid permission response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}

	@Override
	public Role getEnabledRole() throws IOException, CoinStackException {
		String resJsonString = requestGet("/roles/enabled");
		JSONObject resJson;
		try {
			resJson = new JSONObject(resJsonString);
			int permission = resJson.getInt("permission");

			return new Role(permission);
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid permission response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}

	@Override
	public Grantee[] getGrantees(String contractId) throws IOException, CoinStackException {
		String resJsonString = requestGet("/contracts/" + contractId + "/grantees");
		try {
			JSONArray jsonArray = new JSONArray(resJsonString);
			Grantee[] grantees = new Grantee[jsonArray.length()];
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject grantee = jsonArray.getJSONObject(i);
				grantees[i] = new Grantee(contractId, grantee.getString("address"), grantee.getString("token"));
			}
			return grantees;
		} catch (JSONException e) {
			throw new InvalidResponseException("Invalid contract grantee response"
					+ ": Parsing response failed. " + e.getMessage());
		}
	}
}
