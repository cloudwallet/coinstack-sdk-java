package io.blocko.coinstack;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import io.blocko.coinstack.contract.LuaContractCode;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.model.ContractBody;
import io.blocko.coinstack.tx.Constant;
import io.blocko.coinstack.util.Codecs;

public class LuaContractBuilder extends AbstractTransactionBuilder {

	private int OP_NONE = 0;
	private int OP_DEFINITION = 1;
	private int OP_EXECUTION = 2;

	private String contractId;
	private int opCode;
	
	private int utxoThreshold = Constant.DEFAULT_UTXO_THRESHOLD;

	// generating JSON string for a smart contract
	private ContractBody contractBody;
	
	private String majorVersion;
	private String minorVersion;
	private boolean lightEncode;

	// encryption variables for LuaContractCode
	private String eck;
	private boolean isRandEck;
	private boolean isWholeEnc;

	public LuaContractBuilder() {
		super();
		
		this.contractBody = new ContractBody(ContractBody.TYPE_LSC);
		this.contractId = null;
		this.opCode = OP_NONE;
		this.majorVersion = "01";
		this.minorVersion = "00";
		this.lightEncode = false;
		this.eck = null;
		this.isRandEck = false;
		this.isWholeEnc = false;
	}

	/*
	 * use light encoded contract body without base64 encoding.
	 */
	public void setLightEncode() {
		this.lightEncode = true;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	@Deprecated
	/**
	 * It has a character set encoding problem.
	 *
	 * @return
	 * @throws CoinStackException
	 */
	public byte[] getDefinition() throws CoinStackException {
		String code = getDefinitionCode();
		if (code == null) {
			return null;
		}
		return code.getBytes(Charset.forName("UTF-8"));
	}

	public String getDefinitionCode() throws CoinStackException {
		if (opCode == OP_DEFINITION) {
			return ((LuaContractCode) contractBody.getCode()).getCode();
		}
		return null;
	}

	@Deprecated
	/**
	 * This method is deprecated. Use setDefinition(String) instead. There might be
	 * a character set encoding problem.
	 * 
	 * @deprecated
	 * @param definition
	 * @throws CoinStackException
	 */
	public void setDefinition(byte[] definition) throws CoinStackException {
		setDefinition(new String(definition));
	}

	public void setDefinition(String code) throws CoinStackException {
		setDefinition(new LuaContractCode(code));
	}

	public void setDefinition(LuaContractCode code) throws CoinStackException {
		if (code == null) {
			throw new MalformedInputException("Invalid input: Null definition");
		}
		if (contractBody.getCode() != null) {
			throw new MalformedInputException("Invalid input: Duplicate definition");
		}
		this.opCode = OP_DEFINITION;
		contractBody.setCode(code);
	}

	@Deprecated
	/**
	 * It has a character set encoding problem.
	 *
	 * @return
	 * @throws CoinStackException
	 */
	public byte[] getExecution() throws CoinStackException {
		String code = getExecutionCode();
		if (code == null) {
			return null;
		}
		return code.getBytes(Charset.forName("UTF-8"));
	}

	public String getExecutionCode() throws CoinStackException {
		if (opCode == OP_EXECUTION) {
			return ((LuaContractCode) contractBody.getCode()).getCode();
		}
		return null;
	}

	@Deprecated
	/**
	 * This method is deprecated. Use setExecution(String) instead. There might be a
	 * character set encoding problem.
	 * 
	 * @deprecated
	 * @param execution
	 * @throws CoinStackException
	 */
	public void setExecution(byte[] execution) throws CoinStackException {
		setExecution(new String(execution));
	}

	public void setExecution(String code) throws CoinStackException {
		setExecution(new LuaContractCode(code));
	}

	public void setExecution(LuaContractCode code) throws MalformedInputException {
		if (code == null) {
			throw new MalformedInputException("Invalid input: Null execution");
		}
		if (contractBody.getCode() != null) {
			throw new MalformedInputException("Invalid input: Duplicate execution code");
		}
		this.opCode = OP_EXECUTION;
		contractBody.setCode(code);
	}

	public void setPermission(String address, String tokenName, boolean grant) throws CoinStackException {
		String permissionExecutionCode = "";
		if (grant) {
			permissionExecutionCode = "grant(\"" + address + "\", \"" + tokenName + "\")";
		} else {
			permissionExecutionCode = "revoke(\"" + address + "\", \"" + tokenName + "\")";
		}
		setExecution(permissionExecutionCode);
	}
		
	protected void setMajorVersion(String majorVersion) {
		this.majorVersion = majorVersion;
	}

	protected byte[] buildPayload() throws CoinStackException {
		if (contractBody.getCode() == null || opCode == OP_NONE) {
			return null;
		}

		StringBuilder hexBuffer = new StringBuilder();
		hexBuffer.append("4f43");
		hexBuffer.append(majorVersion);
		hexBuffer.append(minorVersion);
		if (opCode == OP_DEFINITION) {
			hexBuffer.append("0001");
		} else if (opCode == OP_EXECUTION) {
			hexBuffer.append("0002");
		}

		if (eck != null && !eck.isEmpty()) {
			contractBody.getCode().setECK(eck);
		}
		if (isRandEck) {
			contractBody.getCode().setRandomEck();
		}
		if (isWholeEnc) {
			contractBody.getCode().setWholeEnc(true);
		}

		byte[] bodyBytes = null;
		try {
			if (lightEncode) {
				// light encoded dataUrl without base64
				String scheme = "data:text/plain;charset=utf-8,";
				String dataStr = contractBody.marshal();
				String bodyStr = scheme + URLEncoder.encode(dataStr, "UTF-8");
				bodyBytes = bodyStr.getBytes(Charset.forName("UTF-8"));
			}
			else {
				// base64 encoded dataUrl
				String scheme = "data:text/plain;charset=utf-8;base64,";
				String dataStr = contractBody.marshal();
				byte[] dataBytes = dataStr.getBytes(Charset.forName("UTF-8"));
				String bodyStr = scheme + Codecs.BASE64.encode(dataBytes);
				bodyBytes = bodyStr.getBytes(Charset.forName("UTF-8"));
			}
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException(e);
		}
		
		String hash = Codecs.SHA256.digestEncodeHex(bodyBytes);
		hexBuffer.append(hash);
		hexBuffer.append(Codecs.HEX.encode(bodyBytes));

		return Codecs.HEX.decode(hexBuffer.toString());
	}

	protected BuildResult buildUsingPayload(CoinStackClient coinstack, byte[] payload, String privateKeyWIF)
			throws IOException, CoinStackException {
		
		String contractId = getContractId();
		if (contractId == null || contractId.isEmpty()) {
			throw new MalformedInputException("Invalid Contract: Contract ID required.");
		}
		/*
		if (!contractId.equals(address)) {
			throw new MalformedInputException("Invalid Contract", "Contract ID is not ");
		}
		*/

		if (payload == null) {
			throw new MalformedInputException("Invalid Contract: Contract payload required.");
		}

		shuffleOutputs(false);
		allowDustyOutput(true);
		allowLargePayload(true);
		setUtxoThreshold(this.utxoThreshold);
		setFee(getFee());
		
		if (coinstack.isUseAddrData()) {
			addDataAddr(contractId);
		} else {
			addOutput(contractId, Constant.DEFAULT_DATA_ADDRESSING_AMOUNT);
		}
		
		setData(payload);
		
		return build(coinstack, privateKeyWIF);
	}


	public String buildTransaction(CoinStackClient coinstack, String privateKeyWIF)
			throws IOException, CoinStackException {
		
		if(coinstack.isUseAddrData()) {
			setMajorVersion("02");
		} else {
			setMajorVersion("01");
		}
		
		byte[] payload = buildPayload();
		
		return buildUsingPayload(coinstack, payload, privateKeyWIF).getRawTxStr();
	}

	public String getEck() {
		return eck;
	}

	public void setEck(String eck) {
		this.eck = eck;
	}

	public boolean isRandEck() {
		return isRandEck;
	}

	public void setRandEck(boolean isRandEck) {
		this.isRandEck = isRandEck;
	}

	public boolean isWholeEnc() {
		return isWholeEnc;
	}

	public void setWholeEnc(boolean isWholeEnc) {
		this.isWholeEnc = isWholeEnc;
	}

	public void addSharePubKey(byte[] pubkey) {
		this.contractBody.addSharePubKey(pubkey);
	}
}
