package io.blocko.coinstack;

/**
 * A Pre-defined Set of TransactionBuildTip
 * 
 * @author BeomjunJeon
 * @since 3.1
 */
public enum TransactionBuildTip implements AbstractTransactionBuildTip {
	/**
	 * If your network follows a standard bitcoin protocol, use this tip at
	 * {@link TransactionSender#TransactionSender(CoinStackClient, String, AbstractTransactionBuildTip)}
	 * to make transactions easily
	 */
	STANDARD() {
		@Override
		public long defaultFee() {
			return io.blocko.coinstack.Math.convertToSatoshi("0.00001");
		}

		@Override
		public long feePerKb() {
			return io.blocko.coinstack.Math.convertToSatoshi("0.00001");
		}

		@Override
		public int utxoThreashold() {
			return 100;
		}
	},
	/**
	 * If your network allows transactions without fee and you does not want to
	 * pay fee, then use this tip at
	 * {@link TransactionSender#TransactionSender(CoinStackClient, String, AbstractTransactionBuildTip)}
	 * to make transactions easily.
	 */
	ZEROFEE() {
		@Override
		public long defaultFee() {
			return 0;
		}

		@Override
		public long feePerKb() {
			return 0;
		}

		@Override
		public int utxoThreashold() {
			return 10;
		}
	};
}
