package io.blocko.coinstack;

public class Math {
	/**
	 * Convert human-readable bitcoin string (e.g. 0.0001 BTC) to satoshi unit
	 * 
	 * @param bitcoinAmount
	 *            in human-friendly, string format (e.g. 0.0001 BTC)
	 * @return bitcoin amount in satoshi
	 */
	public static long convertToSatoshi(String bitcoinAmount) {
		// Math class will be deprecated
		// due to the inconvenience of using the same name as java.lang.Math class
		// and frequent import errors.
		return CoinMath.convertToSatoshi(bitcoinAmount);
	}

}
