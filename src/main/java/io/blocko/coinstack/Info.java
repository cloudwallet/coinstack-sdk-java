package io.blocko.coinstack;

import java.io.InputStream;
import java.util.Properties;

import io.blocko.coinstack.util.Crypto;

public class Info {
	private static enum Option {
		//@formatter:off
		USE_CRYPTO_CACHE("COINSTACK_SDK_USE_CRYPTO_CACHE",
				"sdk.use.crypto.cache",
				"Turn on the crypto cache."),
		CRYPTO_TYPE("COINSTACK_SDK_USE_CRYPTO_TYPE",
				"sdk.crypto.type",
				"Set the crypto type for encryption. (HMAC=1, PBKDF2=2, Simple=3)"),
		NULL(null, null, null);
		//@formatter:on

		private String env;
		private String prop;
		private String desc;

		private Option(String env, String prop, String desc) {
			this.env = env;
			this.prop = prop;
			this.desc = desc;
		}
	}

	private Properties prop;

	Info() throws Exception {
		prop = new Properties();
		InputStream is = getClass().getResourceAsStream("info.properties");
		prop.load(is);
		is.close();
	}

	public String get(String name) {
		return prop.getProperty(name);
	}

	private static Info info;

	public static Info getInstance() throws Exception {
		if (info == null) {
			info = new Info();
		}
		return info;
	}

	public static void main(String[] args) throws Exception {
		System.out.printf("Coinstack Java SDK");
		System.out.printf(" version %s\n", Info.getInstance().get("build.version"));

		for (String arg : args) {
			if (arg.equals("-h") || arg.equals("?") || arg.equals("--help")) {
				System.out.println("<command option>");
				System.out.println("  -h | ? | --help : print this help message.");
				System.out.println("  -v | --version  : print version detail.");
				System.out.println("<env option>");
				for (Option o : Option.values()) {
					if (o == Option.NULL)
						break;
					if (o.env == null || o.env.length() == 0)
						continue;
					System.out.printf("  %s\n", o.env);
					System.out.printf("    %s (DEFAULT: %s)\n", o.desc, Info.getInstance().get(o.prop));
				}
			} else if (arg.equals("-v") || arg.equals("--version")) {
				System.out.printf("%s\n%s\n", Info.getInstance().get("build.scm.branch"),
						Info.getInstance().get("build.commit.hash"));
			}
		}
	}

	public static boolean useCryptCache() {
		try {
			if ("true".equals(System.getenv(Option.USE_CRYPTO_CACHE.env))) {
				return true;
			} else if ("true".equals(Info.getInstance().get(Option.USE_CRYPTO_CACHE.prop))) {
				return true;
			}
		} catch (Exception e) {
			// ignore
		}
		return false;
	}

	public static Crypto.Type getCryptoType() {
		try {
			String type = System.getenv(Option.CRYPTO_TYPE.env);
			int itype = 0;
			if (type != null) {
				itype = Integer.parseInt(type);
			} else {
				type = Info.getInstance().get(Option.CRYPTO_TYPE.prop);
				if (type != null) {
					itype = Integer.parseInt(type);
				}
			}

			if (itype <= 0) {
				return Crypto.DEFAULT_TYPE;
			}

			Crypto.Type found = Crypto.Type.NULL;
			for (Crypto.Type t : Crypto.Type.values()) {
				if (t.getValue() == itype) {
					found = t;
					break;
				}
			}
			if (found == Crypto.Type.NULL) {
				found = Crypto.DEFAULT_TYPE;
			}
			return found;
		} catch (Exception e) {
			// ignore
		}
		return Crypto.DEFAULT_TYPE;
	}
}