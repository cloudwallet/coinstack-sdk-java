package io.blocko.coinstack.permission.model;

public class Grantee {

    private String contractId;
    private String address;
    private String token;

    public Grantee(String contractId, String address, String token) {
        this.setContractId(contractId);
        this.setAddress(address);
        this.setToken(token);
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return this.getContractId() + "_" + this.getAddress() + "_" + this.getToken();
    }
}
