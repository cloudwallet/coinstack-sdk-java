package io.blocko.coinstack.permission.model;

public class Role {

	public static int ADMIN = 1;
	public static int WRITER = 1 << 1;
	public static int MINER = 1 << 2;
	public static int NODE = 1 << 3;

	int role = 0;

	public Role(int role) {
		this.role = role;
	}

	public boolean isAdmin() {
		return 0 != (role & ADMIN);
	}

	public boolean isWriter() {
		return 0 != (role & WRITER);
	}

	public boolean isMiner() {
		return 0 != (role & MINER);
	}

	public boolean isNode() {
		return 0 != (role & NODE);
	}

	public byte getByte() {
		return (byte) role;
	}

	@Override
	public String toString() {

		StringBuffer strBuf = new StringBuffer();

		if (0 != (this.role & ADMIN)) {
			strBuf.append("ADMIN");
		}
		if (0 != (this.role & WRITER)) {
			if (strBuf.length() != 0)
				strBuf.append("|");

			strBuf.append("WRITER");
		}
		if (0 != (this.role & MINER)) {
			if (strBuf.length() != 0)
				strBuf.append("|");

			strBuf.append("MINER");
		}
		if (0 != (this.role & NODE)) {
			if (strBuf.length() != 0)
				strBuf.append("|");

			strBuf.append("NODE");
		}

		if (strBuf.length() == 0)
			strBuf.append("(EMPTY)");

		return strBuf.toString();
	}

}
