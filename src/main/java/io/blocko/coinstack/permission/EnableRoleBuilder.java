package io.blocko.coinstack.permission;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.commons.codec.binary.Hex;

import io.blocko.coinstack.AbstractTransactionBuilder;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.permission.model.Commands;
import io.blocko.coinstack.permission.model.Constant;
import io.blocko.coinstack.permission.model.Role;

public class EnableRoleBuilder extends AbstractTransactionBuilder {

	private Role role = null;
	
	public EnableRoleBuilder() {
		super();
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String buildTransaction(CoinStackClient coinstack, String privateKeyWIF)
			throws IOException, CoinStackException {
		if (this.role == null) {
			throw new MalformedInputException("role is empty");
		}

		allowLargePayload(true);
		shuffleOutputs(false);
		setFee(this.fee);

		// generate transaction OP_RETURN data's header
		StringBuffer hexBuffer = new StringBuffer();
		hexBuffer.append(Constant.Magic); // magic
		hexBuffer.append(Constant.Version); // version
		hexBuffer.append(Commands.SetEnabledRoleOp); // set enabled role command marker

		byte[] markerData = null;

		byte[] prefixData;
		try {
			prefixData = Hex.decodeHex(hexBuffer.toString().toCharArray());
		} catch (org.apache.commons.codec.DecoderException e1) {
			throw new MalformedInputException("Fail to decode cmd header: " + e1.getMessage());
		}

		// convert to byte; header + permission byte + address
		markerData = ByteBuffer.allocate(prefixData.length + 1).put(prefixData)
				.put(role.getByte()).array();

		setData(markerData);

		BuildResult buildResult = preBuild(coinstack, privateKeyWIF);
		
		return buildResult.getRawTxStr();
	}
}
