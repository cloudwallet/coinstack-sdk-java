package io.blocko.coinstack.permission;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Hex;

import io.blocko.coinstack.AbstractTransactionBuilder;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.permission.model.Commands;
import io.blocko.coinstack.permission.model.Constant;
import io.blocko.coinstack.permission.model.Role;

public class SetRoleBuilder extends AbstractTransactionBuilder {

	private Role role = null;
	private String address = null;

	public SetRoleBuilder() {
		super();
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}

	public String buildTransaction(CoinStackClient coinstack, String privateKeyWIF)
			throws IOException, CoinStackException {
		if (this.address == null) {
			throw new MalformedInputException("address is empty");
		}
		if (this.role == null) {
			throw new MalformedInputException("role is empty");
		}

		allowLargePayload(true);
		shuffleOutputs(false);
		setFee(this.fee);

		// generate transaction OP_RETURN data's header
		StringBuffer hexBuffer = new StringBuffer();
		hexBuffer.append(Constant.Magic); // magic
		hexBuffer.append(Constant.Version); // version
		hexBuffer.append(Commands.SetRoleOp); // set role command marker

		byte[] markerData = null;

		byte[] prefixData;
		try {
			prefixData = Hex.decodeHex(hexBuffer.toString().toCharArray());
		} catch (org.apache.commons.codec.DecoderException e1) {
			throw new MalformedInputException("Fail to decode cmd header: " + e1.getMessage());
		}

		byte[] addressData = address.getBytes(Charset.forName("UTF-8"));

		// convert to byte; header + permission byte + address
		markerData = ByteBuffer.allocate(prefixData.length + 1 + addressData.length).put(prefixData)
				.put(role.getByte()).put(addressData).array();

		setData(markerData);

		BuildResult buildResult = preBuild(coinstack, privateKeyWIF);
		
		return buildResult.getRawTxStr();
	}
}
