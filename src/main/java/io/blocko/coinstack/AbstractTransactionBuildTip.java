package io.blocko.coinstack;

/**
 * This interface provides a common information (fee, utxo) to build
 * transactions using the
 * {@link TransactionSender#TransactionSender(CoinStackClient, String, AbstractTransactionBuildTip)
 * TransactionSender} class. You can implement this interface according to your
 * network policies.
 * 
 * @author BeomjunJeon
 * @since 3.1
 */
public interface AbstractTransactionBuildTip {
	/**
	 * @return a default transaction fee in satoshi
	 */
	public abstract long defaultFee();

	/**
	 * @return a transaction fee per kilobyte in satoshi
	 */
	public abstract long feePerKb();

	/**
	 * @return a recommended number of unspent outputs for transaction input
	 */
	public abstract int utxoThreashold();
}
