package io.blocko.coinstack.openassets.util;

import io.blocko.coinstack.ECKey;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.util.Codecs;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.ScriptException;
import org.bitcoinj.core.Utils;
import org.bitcoinj.core.VersionedChecksummedBytes;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

public class Util {

	private final static byte openAssetsNamespace = 19;

	private final static byte bitcoinNamespaceTestnet = 111;

	private final static byte ionedChecksummedBytes = 23;

	private static final class VersionedChecksummedBytesExtension extends
			VersionedChecksummedBytes {
		private static final long serialVersionUID = 1989886938171152676L;

		private VersionedChecksummedBytesExtension(int version, byte[] bytes) {
			super(version, bytes);
		}
	}

	/**
	 * concat two input bytes and return it 
	 * 
	 * @param a a byte array to append first
	 * @param b a byte array to append second 
	 * @return a merged byte array, a + b
	 */
	public static byte[] byteConcat(byte[] a, byte[] b) {
		byte[] result = new byte[a.length + b.length];
		// copy a to result
		System.arraycopy(a, 0, result, 0, a.length);
		// copy b to result
		System.arraycopy(b, 0, result, a.length, b.length);
		return result;
	}

	/**
	 * concat second input bytes at first one 
	 * 
	 * @param a a byte array to be appended
	 * @param b a byte array to append at a
	 */
	/*public static void byteConcat2(byte[] a, byte[] b) {
		byte[] result = new byte[a.length + b.length];
		// copy a to result
		System.arraycopy(a, 0, result, 0, a.length);
		// copy b to result
		System.arraycopy(b, 0, result, a.length, b.length);
		a = result;
	}*/

	/**
	 * create Asset ID from input script
	 * 
	 * 
	 * @param inputScript
	 *            input script byte array which is input of Asset ID
	 */
	private static String createAssetID(byte[] inputScript) {
		byte[] hashResult = Utils.sha256hash160(inputScript);
		return new VersionedChecksummedBytesExtension(ionedChecksummedBytes,
				hashResult).toString();

	}


	/**
	 * derive Bitcoin address from Open Asset address.
	 * 
	 * 
	 * @param assetAddress
	 *            Open Asset address
	 */
	public static String deriveAddressFromAssetAddress(String assetAddress
			) throws MalformedInputException,
			AddressFormatException, UnsupportedEncodingException {
		byte[] decodedAddress = Base58.decode(assetAddress);
		byte[] forBitcoinAddress = new byte[20];
		byte[] version = new byte[4];

		System.arraycopy(decodedAddress, 1, version, 3, 1);
		int versionInt = java.nio.ByteBuffer.wrap(version).getInt();
		
		System.arraycopy(decodedAddress, 2, forBitcoinAddress, 0, 20);
		
		String bitCoinAddress = null;

		bitCoinAddress = new VersionedChecksummedBytesExtension(
					versionInt, forBitcoinAddress).toString();

		return bitCoinAddress;
	}
	
	/**
	 * derive Open Asset address from BitCoin address.
	 * default : MainNet
	 * 
	 * 
	 * @param address
	 *            Bitcoin address          
	 */
	public static String deriveAssetAddressFromAddress(String address)
			throws MalformedInputException, AddressFormatException {
		return deriveAssetAddressFromAddress(address, true);
	}


	/**
	 * derive Open Asset address from BitCoin address.
	 * 
	 * 
	 * @param address
	 *            Bitcoin address 
	 * @param isMainNet
	 *            true is MainNet, false is TestNet                      
	 */
	public static String deriveAssetAddressFromAddress(String address,
			boolean isMainNet) throws MalformedInputException,
			AddressFormatException {
		org.bitcoinj.core.Address bitcoinjAddress;
		if (isMainNet)
			bitcoinjAddress = new org.bitcoinj.core.Address(
					MainNetParams.get(), address);
		else
			bitcoinjAddress = new org.bitcoinj.core.Address(
					RegTestParams.get(), address);
		byte[] forAssetsAddress = new byte[21];
		System.arraycopy(bitcoinjAddress.getHash160(), 0, forAssetsAddress, 1,
				20);
		forAssetsAddress[0] = (byte) bitcoinjAddress.getVersion();

		String receiveAssetsAddress = new VersionedChecksummedBytesExtension(
				openAssetsNamespace, forAssetsAddress).toString();

		return receiveAssetsAddress;
	}
	
	public static String deriveAssetAddressFromRedeemScipt(String redeemScript,
			boolean isMainNet) throws MalformedInputException,
			AddressFormatException {
		org.bitcoinj.core.Address bitcoinjAddress;
		
		Script redeem = null;
		String from = null;
		try {
			redeem = new Script(Codecs.HEX.decode(redeemScript));
		} catch (ScriptException e) {
			throw new MalformedInputException("Invalid redeem script: Parsing redeem script failed");
		} catch (RuntimeException e) {
			throw new MalformedInputException("Invalid redeem script: Parsing redeem script failed");
		}
		from = createAddressFromRedeemScript(redeem, isMainNet);
				
		if (isMainNet)
			bitcoinjAddress = new org.bitcoinj.core.Address(
					MainNetParams.get(), from);
		else
			bitcoinjAddress = new org.bitcoinj.core.Address(
					RegTestParams.get(), from);
		byte[] forAssetsAddress = new byte[21];
		System.arraycopy(bitcoinjAddress.getHash160(), 0, forAssetsAddress, 1,
				20);
		forAssetsAddress[0] = (byte) bitcoinjAddress.getVersion();

		String receiveAssetsAddress = new VersionedChecksummedBytesExtension(
				openAssetsNamespace, forAssetsAddress).toString();

		return receiveAssetsAddress;
	}	 
	
	public static String createAddressFromRedeemScript(Script redeemScript, boolean isMainNet) {
		Script sc = ScriptBuilder.createP2SHOutputScript(redeemScript);
		Address address = Address.fromP2SHScript(isMainNet ? MainNetParams.get() : RegTestParams.get(), sc);
		return address.toString();
	}

	/**
	 * derive Open Asset address from BitCoin private key WIF.
	 * default : MainNet
	 * 
	 * 
	 * @param privateKey
	 *            Bitcoin private key WIF         
	 */
	public static String deriveAssetAddressFromPrivateKey(String privateKey)
			throws MalformedInputException, AddressFormatException {
		return deriveAssetAddressFromPrivateKey(privateKey, true);
	}

	/**
	 * derive Open Asset address from BitCoin private key WIF.
	 * 
	 * 
	 * @param privateKey
	 *            Bitcoin private key WIF     
	 * @param isMainNet
	 *            true is MainNet, false is TestNet           
	 */
	public static String deriveAssetAddressFromPrivateKey(String privateKey,
			boolean isMainNet) throws MalformedInputException,
			AddressFormatException {
		String addressString = ECKey.deriveAddress(privateKey, isMainNet);
		org.bitcoinj.core.Address address = null;
		if (isMainNet) {
			address = new org.bitcoinj.core.Address(MainNetParams.get(),
					addressString);
		} else {
			address = new org.bitcoinj.core.Address(RegTestParams.get(),
					addressString);
		}
		byte[] forAssetsAddress = new byte[21];
		System.arraycopy(address.getHash160(), 0, forAssetsAddress, 1, 20);
		forAssetsAddress[0] = (byte) address.getVersion();

		String receiveAssetsAddress = new VersionedChecksummedBytesExtension(
				openAssetsNamespace, forAssetsAddress).toString();

		return receiveAssetsAddress;
	}

	/**
	 * derive Open Asset ID from Bitcoin Address
	 * default : MainNet
	 * 
	 * 
	 * @param address
	 *            Bitcoin address          
	 */
	public static String deriveAssetIDFromAddress(String address)
			throws MalformedInputException, UnsupportedEncodingException {
		return deriveAssetIDFromAddress(address, true);
	}

	/**
	 * derive Open Asset ID from Bitcoin Address
	 * 
	 * 
	 * @param address
	 *            Bitcoin address    
	 * @param isMainNet
	 *            true is MainNet, false is TestNet        
	 */
	public static String deriveAssetIDFromAddress(String address,
			boolean isMainNet) throws MalformedInputException,
			UnsupportedEncodingException {
		NetworkParameters network;
		if (isMainNet) {
			network = MainNetParams.get();
		} else {
			network = RegTestParams.get();
		}
		org.bitcoinj.core.Address destinationBitcoinAddress;
		try {
			destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
					address);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}
		Script script = ScriptBuilder
				.createOutputScript(destinationBitcoinAddress);
		byte[] hashResult = Utils.sha256hash160(script.getProgram());
		return new VersionedChecksummedBytesExtension(ionedChecksummedBytes,
				hashResult).toString();
	}

	/**
	 * derive Open Asset ID from Bitcoin private key WIF
	 * default : MainNet
	 * 
	 * parameters
	 * 
	 * @param privateKey
	 *            Bitcoin private key WIF           
	 */
	public static String deriveAssetIDFromPrivateKey(String privateKey)
			throws MalformedInputException {
		return deriveAssetIDFromPrivateKey(privateKey, true);
	}

	/**
	 * derive Open Asset ID from Bitcoin private key WIF
	 * 
	 * parameters
	 * 
	 * @param privateKey
	 *            Bitcoin private key WIF      
	 * @param isMainNet
	 *            true is MainNet, false is TestNet      
	 */
	public static String deriveAssetIDFromPrivateKey(String privateKey,
			boolean isMainNet) throws MalformedInputException {
		String address = ECKey.deriveAddress(privateKey, isMainNet);

		NetworkParameters network;
		if (isMainNet) {
			network = MainNetParams.get();
		} else {
			network = RegTestParams.get();
		}
		org.bitcoinj.core.Address destinationBitcoinAddress;
		try {
			destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
					address);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}
		Script script = ScriptBuilder
				.createOutputScript(destinationBitcoinAddress);
		byte[] hashResult = Utils.sha256hash160(script.getProgram());
		return new VersionedChecksummedBytesExtension(ionedChecksummedBytes,
				hashResult).toString();
	}

	public static byte[] littleEndian(byte[] payload) {
		ByteBuffer buffer = ByteBuffer.wrap(payload);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		return buffer.array();
	}
}
