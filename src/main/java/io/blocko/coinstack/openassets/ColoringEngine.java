package io.blocko.coinstack.openassets;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.CoinMath;
import io.blocko.coinstack.MultiSig;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.InsufficientFundException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.model.DataTransactionOutput;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Output.MetaData;
import io.blocko.coinstack.openassets.model.AssetOutput;
import io.blocko.coinstack.openassets.model.OpenAssetOpCodes;
import io.blocko.coinstack.openassets.util.Leb128;
import io.blocko.coinstack.openassets.util.Util;
import io.blocko.coinstack.util.Codecs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Transaction.SigHash;
import org.bitcoinj.core.TransactionConfidence;
import org.bitcoinj.core.TransactionOutput;
import org.bitcoinj.core.VarInt;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.script.ScriptOpCodes;

/**
 * a class to issue and transfer open asset and to inquire balance of open asset
 */
public class ColoringEngine {

	private NetworkParameters network;
	private CoinStackClient coinStackClient;
	private boolean isMainNet;
	private static final long defaultAssetBTC = CoinMath.convertToSatoshi("0.000006");

	/**
	 * Create a ColoringEngine constructor with MainNet
	 */
	public ColoringEngine(CoinStackClient coinStackClient) {
		this.coinStackClient = coinStackClient;
		this.network = MainNetParams.get();
		this.isMainNet = true;
	}

	/**
	 * Create a ColoringEngine constructor with either MainNet or TestNet
	 */
	public ColoringEngine(CoinStackClient coinStackClient, boolean isMainNet) {
		this.coinStackClient = coinStackClient;
		if (isMainNet) {
			this.network = MainNetParams.get();
			this.isMainNet = isMainNet;
		} else {
			this.network = RegTestParams.get();
			this.isMainNet = isMainNet;
		}

	}

	/**
	 * Issue Open Asset with a private key parameters
	 * 
	 * @param privateKeyWIF
	 *            private key with WIF form which will create Open Asset ID
	 * @param assetAmount
	 *            Asset amount that user wants to issue
	 * @param toAddress
	 *            Open Asset address(different from Bitcoin address form) which
	 *            receive issued open asset for the first time. It can be same
	 *            as the issuer's address.
	 * @param fee
	 *            fee that user will pay for this Open Asset transaction.
	 */
	public String issueAsset(String privateKeyWIF, long assetAmount,
			String toAddress, long fee) throws IOException, CoinStackException {
		org.bitcoinj.core.Transaction txTemplate = new org.bitcoinj.core.Transaction(
				network);

		org.bitcoinj.core.Address destinationBitcoinAddress;
		String bitcoinAddress = null;
		try {
			bitcoinAddress = Util.deriveAddressFromAssetAddress(toAddress);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid input: Malformed address");
		}

		try {
			destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
					bitcoinAddress);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid input: Malformed address");
		}
		txTemplate.addOutput(Coin.valueOf(defaultAssetBTC),
				destinationBitcoinAddress);

		final ECKey signingKey;
		try {
			signingKey = new DumpedPrivateKey(network, privateKeyWIF).getKey();
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid private key: Malformed private key");
		}
		org.bitcoinj.core.Address fromAddress = signingKey.toAddress(network);
		String from = fromAddress.toString();

		Output[] tempOutputs = coinStackClient.getUnspentOutputs(from);
		Output[] outputs = defaultCoinSelection(Coin.valueOf(defaultAssetBTC + fee), tempOutputs);
		Arrays.sort(outputs, CoinStackClient.outputComparator);
		long totalValue = 0;

		org.bitcoinj.core.Transaction tx = null;

		for (Output output : outputs) {
			if (output.getMetaData() == null
					|| output.getMetaData().getAsset_id() == null) {

				Sha256Hash outputHash = new Sha256Hash(
						CoinStackClient.convertEndianness(output
								.getTransactionId()));

				if (tx == null || !tx.getHash().equals(outputHash)) {
					tx = new TemporaryTransaction(MainNetParams.get(),
							outputHash);
					tx.getConfidence().setConfidenceType(
							TransactionConfidence.ConfidenceType.BUILDING);
				}

				txTemplate.getConfidence().setConfidenceType(
						TransactionConfidence.ConfidenceType.BUILDING);

				// fill hole between indexes with dummies
				while (tx.getOutputs().size() < output.getIndex()) {
					tx.addOutput(new TransactionOutput(MainNetParams.get(), tx,
							Coin.NEGATIVE_SATOSHI, new byte[] {}));
				}

				tx.addOutput(new TransactionOutput(MainNetParams.get(), tx,
						Coin.valueOf(output.getValue()),
						Codecs.HEX.decode(output.getScript())));

				txTemplate.addInput(tx.getOutput(tx.getOutputs().size() - 1));
				totalValue += output.getValue();

			}
		}

		if (outputs.length == 0) {
			throw new InsufficientFundException("Insufficient fund");
		}

		int[] amountList = { (int) assetAmount };
		String meta = "00";
		byte[] res = createMarkerOutput(amountList, meta);

		Script script = new ScriptBuilder().op(ScriptOpCodes.OP_RETURN)
				.data(res).build();

		TransactionOutput outputOp = new DataTransactionOutput(this.network,
				txTemplate, Coin.ZERO, script.getProgram());
		txTemplate.addOutput(outputOp);

		long rest = totalValue - fee - defaultAssetBTC;
		if (rest < 0) {
			throw new InsufficientFundException("Insufficient fund");
		}

		if (rest > 0) {
			try {
				txTemplate.addOutput(Coin.valueOf(rest),
						new org.bitcoinj.core.Address(network, from));
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid output: Malformed output address");
			}
		}

		for (int i = 0; i < txTemplate.getInputs().size(); i++) {
			Script sc = new Script(
					Codecs.HEX.decode(outputs[i].getScript()));
			Sha256Hash sighash = txTemplate.hashForSignature(i, sc,
					SigHash.ALL, false);
			ECKey.ECDSASignature mySignature = signingKey.sign(sighash);
			TransactionSignature signature = new TransactionSignature(
					mySignature, SigHash.ALL, false);
			Script scriptSig = ScriptBuilder.createInputScript(signature,
					signingKey);
			txTemplate.getInput(i).setScriptSig(scriptSig);
		}

		byte[] rawTx = txTemplate.bitcoinSerialize();
		// convert to string encoded hex and return
		return Codecs.HEX.encode(rawTx);
	}

	/**
	 * Transfer Open Asset which user possesses with a private key
	 * 
	 * parameters
	 * 
	 * @param privateKeyWIF
	 *            private key with WIF form which create Open Asset address
	 * @param assetID
	 *            Asset ID that can distinguish the asset from the other
	 * @param assetAmount
	 *            Asset amount that user wants to transfer
	 * @param toAddress
	 *            Open Asset address(different from Bitcoin address form) which
	 *            receive open asset from the current user.
	 * @param fee
	 *            fee that user will pay for this Open Asset transaction.
	 */
	public String transferAsset(String privateKeyWIF, String assetID,
			long assetAmount, String toAddress, long fee) throws Exception {
		org.bitcoinj.core.Transaction txTemplate = new org.bitcoinj.core.Transaction(
				network);

		final ECKey signingKey;
		try {
			signingKey = new DumpedPrivateKey(network, privateKeyWIF).getKey();
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid private key: Malformed private key");
		}
		org.bitcoinj.core.Address fromAddress = signingKey.toAddress(network);
		String from = fromAddress.toString();

		Output[] tempOutputs = coinStackClient.getUnspentOutputs(from);
		Output[] outputs = defaultOpenAssetCoinSelection(Coin.valueOf(defaultAssetBTC + defaultAssetBTC + fee), tempOutputs);
		Arrays.sort(outputs, CoinStackClient.outputComparator);
		long totalAssetAmount = 0;
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null) {
				if (outputs[i].getMetaData().getAsset_id().equals(assetID)) {
					totalAssetAmount += outputs[i].getMetaData().getQuantity();
				}
			}
		}

		if (totalAssetAmount < assetAmount)
			throw new InsufficientFundException("Insufficient fund");

		long totalValue = 0;
		org.bitcoinj.core.Transaction tx = null;
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null
					&& !outputs[i].getMetaData().getAsset_id().equals(assetID)) {
				// filter out inputs with different asset ID, but include
				// uncolored ones
				continue;
			}
			Sha256Hash outputHash = new Sha256Hash(
					CoinStackClient.convertEndianness(outputs[i]
							.getTransactionId()));

			if (tx == null || !tx.getHash().equals(outputHash)) {
				tx = new TemporaryTransaction(MainNetParams.get(), outputHash);
				tx.getConfidence().setConfidenceType(
						TransactionConfidence.ConfidenceType.BUILDING);
			}

			txTemplate.getConfidence().setConfidenceType(
					TransactionConfidence.ConfidenceType.BUILDING);

			// fill hole between indexes with dummies
			while (tx.getOutputs().size() < outputs[i].getIndex()) {
				tx.addOutput(new TransactionOutput(MainNetParams.get(), tx,
						Coin.NEGATIVE_SATOSHI, new byte[] {}));
			}

			tx.addOutput(new TransactionOutput(MainNetParams.get(), tx, Coin
					.valueOf(outputs[i].getValue()),
					Codecs.HEX.decode(outputs[i].getScript())));

			txTemplate.addInput(tx.getOutput(tx.getOutputs().size() - 1));
			totalValue += outputs[i].getValue();
		}
		int[] amountList = null;
		if((totalAssetAmount - assetAmount) > 0) {
			amountList = new int[2];
			amountList[0] = (int) assetAmount;
			amountList[1] = (int) (totalAssetAmount - assetAmount) ;
		} else {
			amountList = new int[1];
			amountList[0] = (int) assetAmount;
		}
		String meta = "00";
		byte[] res = createMarkerOutput(amountList, meta);
		Script script = new ScriptBuilder().op(ScriptOpCodes.OP_RETURN)
				.data(res).build();

		TransactionOutput outputOp = new DataTransactionOutput(this.network,
				txTemplate, Coin.ZERO, script.getProgram());
		txTemplate.addOutput(outputOp);

		org.bitcoinj.core.Address destinationBitcoinAddress;
		String bitcoinAddress = null;

		try {
			bitcoinAddress = Util.deriveAddressFromAssetAddress(toAddress);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}

		try {
			destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
					bitcoinAddress);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}
		txTemplate.addOutput(Coin.valueOf(defaultAssetBTC),
				destinationBitcoinAddress);

		if((totalAssetAmount - assetAmount) > 0) {
			try {
				destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
						from);
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid output: Malformed address");
			}
			txTemplate.addOutput(Coin.valueOf(defaultAssetBTC),
				destinationBitcoinAddress);
		}
		long rest;
		if((totalAssetAmount - assetAmount) > 0)
			rest = totalValue - fee - defaultAssetBTC - defaultAssetBTC;
		else
			rest = totalValue - fee - defaultAssetBTC;
		
		if (rest < 0) {
			throw new InsufficientFundException("Insufficient fund");
		}

		if (rest > 0) {
			try {
				txTemplate.addOutput(Coin.valueOf(rest),
						new org.bitcoinj.core.Address(network, from));
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid output: Malformed output address");
			}
		}

		for (int i = 0; i < txTemplate.getInputs().size(); i++) {
			Script sc = new Script(
					Codecs.HEX.decode(outputs[i].getScript()));
			Sha256Hash sighash = txTemplate.hashForSignature(i, sc,
					SigHash.ALL, false);
			ECKey.ECDSASignature mySignature = signingKey.sign(sighash);
			TransactionSignature signature = new TransactionSignature(
					mySignature, SigHash.ALL, false);
			Script scriptSig = ScriptBuilder.createInputScript(signature,
					signingKey);
			txTemplate.getInput(i).setScriptSig(scriptSig);
		}

		byte[] rawTx = txTemplate.bitcoinSerialize();
		// convert to string encoded hex and return
		return Codecs.HEX.encode(rawTx);
	}

	public String transferMultisigAsset(List<String> privateKeyWIFs,
			String redeemScript, String assetID, long assetAmount,
			String toAddress, long fee) throws Exception {
		org.bitcoinj.core.Transaction txTemplate = new org.bitcoinj.core.Transaction(
				network);

		// derive address from reddemScript
		String from = null;
		Script redeem = null;
		try {
			redeem = new Script(Codecs.HEX.decode(redeemScript));
			from = MultiSig.createAddressFromRedeemScript(redeem, isMainNet());
		} catch (RuntimeException e1) {
			e1.printStackTrace();
		}

		Output[] tempOutputs = coinStackClient.getUnspentOutputs(from);
		Output[] outputs = defaultOpenAssetCoinSelection(Coin.valueOf(defaultAssetBTC + defaultAssetBTC + fee), tempOutputs);
		Arrays.sort(outputs, CoinStackClient.outputComparator);
		long totalAssetAmount = 0;
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null) {
				if (outputs[i].getMetaData().getAsset_id().equals(assetID)) {
					totalAssetAmount += outputs[i].getMetaData().getQuantity();
				}
			}
		}
		if (totalAssetAmount < assetAmount)
			throw new InsufficientFundException("Insufficient fund");

		long totalValue = 0;
		org.bitcoinj.core.Transaction tx = null;
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null
					&& !outputs[i].getMetaData().getAsset_id().equals(assetID)) {
				// filter out inputs with different asset ID, but include
				// uncolored ones
				continue;
			}
			Sha256Hash outputHash = new Sha256Hash(
					CoinStackClient.convertEndianness(outputs[i]
							.getTransactionId()));

			if (tx == null || !tx.getHash().equals(outputHash)) {
				tx = new TemporaryTransaction(MainNetParams.get(), outputHash);
				tx.getConfidence().setConfidenceType(
						TransactionConfidence.ConfidenceType.BUILDING);
			}

			txTemplate.getConfidence().setConfidenceType(
					TransactionConfidence.ConfidenceType.BUILDING);

			// fill hole between indexes with dummies
			while (tx.getOutputs().size() < outputs[i].getIndex()) {
				tx.addOutput(new TransactionOutput(MainNetParams.get(), tx,
						Coin.NEGATIVE_SATOSHI, new byte[] {}));
			}

			tx.addOutput(new TransactionOutput(MainNetParams.get(), tx, Coin
					.valueOf(outputs[i].getValue()),
					Codecs.HEX.decode(outputs[i].getScript())));

			txTemplate.addInput(tx.getOutput(tx.getOutputs().size() - 1));
			totalValue += outputs[i].getValue();
		}

		int[] amountList = null;
		if((totalAssetAmount - assetAmount) > 0) {
			amountList = new int[2];
			amountList[0] = (int) assetAmount;
			amountList[1] = (int) (totalAssetAmount - assetAmount) ;
		} else {
			amountList = new int[1];
			amountList[0] = (int) assetAmount;
		}
		String meta = "00";
		byte[] res = createMarkerOutput(amountList, meta);
		Script script = new ScriptBuilder().op(ScriptOpCodes.OP_RETURN)
				.data(res).build();

		TransactionOutput outputOp = new DataTransactionOutput(this.network,
				txTemplate, Coin.ZERO, script.getProgram());
		txTemplate.addOutput(outputOp);

		org.bitcoinj.core.Address destinationBitcoinAddress;
		String bitcoinAddress = null;
		try {
			bitcoinAddress = Util.deriveAddressFromAssetAddress(toAddress);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}

		try {
			destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
					bitcoinAddress);
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}
		txTemplate.addOutput(Coin.valueOf(defaultAssetBTC),
				destinationBitcoinAddress);

		if((totalAssetAmount - assetAmount) > 0) {
			try {
				destinationBitcoinAddress = new org.bitcoinj.core.Address(network,
						from);
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid output: Malformed address");
			}
			txTemplate.addOutput(Coin.valueOf(defaultAssetBTC),
			destinationBitcoinAddress);
		}
		long rest;
		if((totalAssetAmount - assetAmount) > 0)
			rest = totalValue - fee - defaultAssetBTC - defaultAssetBTC;
		else
			rest = totalValue - fee - defaultAssetBTC;
		
		if (rest < 0) {
			throw new InsufficientFundException("Insufficient fund");
		}

		if (rest > 0) {
			try {
				txTemplate.addOutput(Coin.valueOf(rest),
						new org.bitcoinj.core.Address(network, from));
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid output: Malformed output address");
			}
		}

		List<TransactionSignature> signatures = new ArrayList<TransactionSignature>();
		List<ECKey> eckeys = new ArrayList<ECKey>();
		for (int i = 0; i < privateKeyWIFs.size(); i++) {
			ECKey eckey = null;
			try {
				eckey = new DumpedPrivateKey(getNetwork(),
						privateKeyWIFs.get(i)).getKey();
				eckeys.add(eckey);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// ECKey [] ecKeyArrays = eckeys.toArray(new ECKey [0]);
		// Arrays.sort(ecKeyArrays, ECKeyComparator);
		List<ECKey> pubkeys = new ArrayList<ECKey>(eckeys);
		Collections.sort(pubkeys, ECKey.PUBKEY_COMPARATOR);
		for (int j = 0; j < outputs.length; j++) {
			Sha256Hash sighash = txTemplate.hashForSignature(j, redeem,
					SigHash.ALL, false);
			for (int i = 0; i < privateKeyWIFs.size(); i++) {

				ECKey.ECDSASignature mySignature = pubkeys.get(i).sign(sighash);
				TransactionSignature signature = new TransactionSignature(
						mySignature, SigHash.ALL, false);
				signatures.add(signature);
			}
			Script inputScript = ScriptBuilder.createP2SHMultiSigInputScript(
					signatures, redeem);

			txTemplate.getInput(j).setScriptSig(inputScript);
			signatures.clear();
		}

		byte[] rawTx = txTemplate.bitcoinSerialize();
		// convert to string encoded hex and return
		return Codecs.HEX.encode(rawTx);
	}

	/**
	 * Get balance of Open Asset which user possesses in his address with asset
	 * ID
	 * 
	 * parameters
	 * 
	 * @param assetAddress
	 *            Open Asset address
	 * @param assetID
	 *            Asset ID that can distinguish the asset from the other
	 */
	public long getAssetBalance(String assetAddress, String assetID)
			throws Exception {

		String from = Util.deriveAddressFromAssetAddress(assetAddress);

		Output[] outputs = coinStackClient.getUnspentOutputs(from);
		long totalAssetAmount = 0;
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null) {
				if (outputs[i].getMetaData().getAsset_id().equals(assetID)) {
					totalAssetAmount += outputs[i].getMetaData().getQuantity();
				}
			}
		}

		return totalAssetAmount;
	}


	/**
	 * Get Unspent Asset outputs of Open Asset which user possesses in his
	 * address with asset ID
	 * 
	 * parameters
	 * 
	 * @param assetAddress
	 *            Open Asset address
	 */
	public AssetOutput[] getUnspentAssetOutputs(String assetAddress)
			throws IOException, CoinStackException, AddressFormatException {

		String from = Util.deriveAddressFromAssetAddress(assetAddress);

		ArrayList<AssetOutput> assetOutputList = new ArrayList<AssetOutput>();
		Output[] outputs = coinStackClient.getUnspentOutputs(from);
		long totalAssetAmount = 0;
		MetaData meta = null;
		for (int i = 0; i < outputs.length; i++) {
			if ((meta = outputs[i].getMetaData()) != null) {
				assetOutputList.add(new AssetOutput(outputs[i].getTransactionId(), outputs[i].getIndex(), assetAddress, false,
						outputs[i].getValue(), outputs[i].getScript(), meta.getAsset_id(), meta.getQuantity()));

			}
		}

		return assetOutputList.toArray(new AssetOutput[] {});
	}

	private void uint16ToByteArrayLE(long val, byte[] out, int offset) {
		out[offset + 1] = (byte) (0xFF & (val >> 0));
		out[offset + 0] = (byte) (0xFF & (val >> 8));
	}
	
	private byte[] createMarkerOutput(int[] assetNumbers, String meta) {
		byte[] MARKER = new byte[2];
		byte[] OAVERSION = new byte[2];
		uint16ToByteArrayLE(OpenAssetOpCodes.OP_MARKER, MARKER, 0);
		uint16ToByteArrayLE(OpenAssetOpCodes.OP_OAVERSION, OAVERSION, 0);
		
		byte[] res = Util.byteConcat(MARKER, OAVERSION);
		byte[] encodedAssetCount = Util.littleEndian(new VarInt(
				assetNumbers.length).encode());
		res = Util.byteConcat(res, encodedAssetCount);

		for (int i = 0; i < assetNumbers.length; i++) {
			byte[] assetAmountLEB128 = Leb128
					.writeUnsignedLeb128(assetNumbers[i]);
			res = Util.byteConcat(res, assetAmountLEB128);
		}
		res = Util.byteConcat(res, Codecs.HEX.decode(meta));

		return res;
	}

	private static class TemporaryTransaction extends
			org.bitcoinj.core.Transaction {
		private static final long serialVersionUID = -6832934294927540476L;
		private final Sha256Hash hash;

		public TemporaryTransaction(final NetworkParameters params,
				final Sha256Hash hash) {
			super(params);
			this.hash = hash;
		}

		@Override
		public Sha256Hash getHash() {
			return hash;
		}
	}

	public boolean isMainNet() {
		return isMainNet;
	}

	private NetworkParameters getNetwork() {
		return network;
	}
	
	public static Comparator<Output> outputComparator = new Comparator<Output>() {
		public int compare(Output output1, Output output2) {
			long valueCompare = output1.getValue() - output2.getValue();
			if (valueCompare > 0) {
				return 1;
			} else if (valueCompare <= 0) {
				return -1;
			} else {
				return 0;
			}
		}
	};
	
	/*
	 * defaultCoinSelection method is simple implementation of
	 * coin selection.
	 * It sorts outputs by its values first.
	 * It searches output just bigger than the required value.
	 * If there is no output of which value is bigger than the required one,
	 * it adds all value from the start until the sum is bigger than the one.
	 * Transaction build method will decide the value is lower than the required one,
	 * which is an insufficient value exception.
	 */
	public Output[] defaultCoinSelection(Coin value, Output[] outputs) {
		boolean found = false;
		
		Arrays.sort(outputs, outputComparator);
		List<Output> outputList = new ArrayList<Output>();
		for(int i = 0 ; i < outputs.length ; i++) {
			if(value.longValue() <= outputs[i].getValue()) {
				outputList.add(outputs[i]);
				found = true;
				break;
			}
		}

		if(!found) {
			long sumValue = 0l;
			for(int i = 0 ; i < outputs.length ; i++) {
				if(sumValue <= value.longValue()) {
					sumValue += outputs[i].getValue();
					outputList.add(outputs[i]);
				} else break;
			}
		}
		Output[] resOutput = new Output[outputList.size()];
		resOutput = outputList.toArray(resOutput);
		return resOutput;
	}
	
	/*
	 * coin selection is only for Bitcoin selection.
	 * In fact, Open asset is also needed to be selected
	 * to make a transaction.
	 * The following method is abount bitcoin UTXO selection for
	 * the fee and default BTCs, and includes all the open asset outputs
	 * for the implementation convenience.
	 * Transfer method body will exclude open asset outputs by open asset ID later.
	 */
	public Output[] defaultOpenAssetCoinSelection(Coin value, Output[] outputs) {
		boolean found = false;
		
		Arrays.sort(outputs, outputComparator);
		List<Output> outputList = new ArrayList<Output>();
		for(int i = 0 ; i < outputs.length ; i++) {
			if(value.longValue() <= outputs[i].getValue()) {
				outputList.add(outputs[i]);
				found = true;
				break;
			}
		}

		if(!found) {
			long sumValue = 0l;
			for(int i = 0 ; i < outputs.length ; i++) {
				if(outputs[i].getMetaData() == null && sumValue <= value.longValue()) {
					sumValue += outputs[i].getValue();
					outputList.add(outputs[i]);
				} else break;
			}
		}
		
		for(int i = 0 ; i < outputs.length ; i++) {
			if(outputs[i].getMetaData() != null) {
				outputList.add(outputs[i]);
			} 
		}
		Output[] resOutput = new Output[outputList.size()];
		resOutput = outputList.toArray(resOutput);
		return resOutput;
	}
}
