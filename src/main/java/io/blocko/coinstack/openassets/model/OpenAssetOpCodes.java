package io.blocko.coinstack.openassets.model;

public class OpenAssetOpCodes {
	public static final long OP_MARKER = 0x4f41;
	public static final long OP_OAVERSION = 0x0100;
}
