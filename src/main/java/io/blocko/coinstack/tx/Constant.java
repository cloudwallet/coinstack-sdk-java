package io.blocko.coinstack.tx;

public class Constant {
	public static final long DEFAULT_FEE = 10000;
	public static final int DEFAULT_UTXO_THRESHOLD = -1; // unlimited
	
	public static final int DEFAULT_MAX_TX_SIZE = 100 * 1000;
	public static final int DEFAULT_MAX_META_SIZE = 80;
	
	public static final int EXTENDED_MAX_TX_SIZE = 5 * 1024 * 1024;
	public static final int EXTENDED_MAX_META_SIZE = 2 * 1024 * 1024;
	
	public static final long DEFAULT_DATA_ADDRESSING_AMOUNT = 600;
}
