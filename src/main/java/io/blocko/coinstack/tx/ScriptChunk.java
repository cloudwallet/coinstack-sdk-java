package io.blocko.coinstack.tx;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static org.bitcoinj.script.ScriptOpCodes.OP_PUSHDATA1;
import static org.bitcoinj.script.ScriptOpCodes.OP_PUSHDATA2;
import static org.bitcoinj.script.ScriptOpCodes.OP_PUSHDATA4;

import java.io.IOException;
import java.io.OutputStream;

import org.bitcoinj.core.Utils;

public class ScriptChunk extends org.bitcoinj.script.ScriptChunk {

	public ScriptChunk(int opcode, byte[] data) {
		super(opcode, data);
	}
	public ScriptChunk(int opcode, byte[] data, int startLocationInProgram) {
		super(opcode, data, startLocationInProgram);
	}
	
    public void write(OutputStream stream) throws IOException {
        if (isOpCode()) {
            checkState(data == null);
            stream.write(opcode);
        } else if (data != null) {
            checkNotNull(data);
            if (opcode < OP_PUSHDATA1) {
                checkState(data.length == opcode);
                stream.write(opcode);
            } else if (opcode == OP_PUSHDATA1) {
                checkState(data.length <= 0xFF);
                stream.write(OP_PUSHDATA1);
                stream.write(data.length);
            } else if (opcode == OP_PUSHDATA2) {
                checkState(data.length <= 0xFFFF);
                stream.write(OP_PUSHDATA2);
                stream.write(0xFF & data.length);
                stream.write(0xFF & (data.length >> 8));
            } else if (opcode == OP_PUSHDATA4) {
                //checkState(data.length <= Script.MAX_SCRIPT_ELEMENT_SIZE);
                checkState(data.length <= ScriptBuilder.LIMIT_PUSHDATA4_SIZE);
                stream.write(OP_PUSHDATA4);
                Utils.uint32ToByteStreamLE(data.length, stream);
            } else {
                throw new RuntimeException("Unimplemented");
            }
            stream.write(data);
        } else {
            stream.write(opcode); // smallNum
        }
    }

}
