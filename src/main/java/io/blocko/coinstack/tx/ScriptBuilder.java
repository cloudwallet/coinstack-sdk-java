package io.blocko.coinstack.tx;

import static com.google.common.base.Preconditions.checkArgument;
import static org.bitcoinj.script.ScriptOpCodes.OP_0;
import static org.bitcoinj.script.ScriptOpCodes.OP_1;
import static org.bitcoinj.script.ScriptOpCodes.OP_1NEGATE;
import static org.bitcoinj.script.ScriptOpCodes.OP_PUSHDATA1;
import static org.bitcoinj.script.ScriptOpCodes.OP_PUSHDATA2;
import static org.bitcoinj.script.ScriptOpCodes.OP_PUSHDATA4;

import java.util.Arrays;

public class ScriptBuilder extends org.bitcoinj.script.ScriptBuilder {
	
	public static final int MAX_PUSHDATA1_SIZE = 256; // 2^8
	public static final int MAX_PUSHDATA2_SIZE = 65536; // 2^16
	public static final int LIMIT_PUSHDATA4_SIZE = Integer.MAX_VALUE; // (max=4gb=2^32)
	
    /** Adds a copy of the given byte array as a data element (i.e. PUSHDATA) at the given index in the program. */
	@Override
    public org.bitcoinj.script.ScriptBuilder data(int index, byte[] data) {
        // implements BIP62
        byte[] copy = Arrays.copyOf(data, data.length);
        int opcode;
        if (data.length == 0) {
            opcode = OP_0;
        } else if (data.length == 1) {
            byte b = data[0];
            if (b >= 1 && b <= 16)
                opcode = encodeToOpN(b);
            else
                opcode = 1;
        } else if (data.length < OP_PUSHDATA1) {
            opcode = data.length;
        } else if (data.length < MAX_PUSHDATA1_SIZE) {
            opcode = OP_PUSHDATA1;
        } else if (data.length < MAX_PUSHDATA2_SIZE) {
            opcode = OP_PUSHDATA2;
        } else if (data.length <= LIMIT_PUSHDATA4_SIZE) {
            opcode = OP_PUSHDATA4;
        } else {
            throw new RuntimeException("Unimplemented");
        }
        return addChunk(index, new ScriptChunk(opcode, copy));
    }

	
    static int encodeToOpN(int value) {
        checkArgument(value >= -1 && value <= 16, "encodeToOpN called for " + value + " which we cannot encode in an opcode.");
        if (value == 0)
            return OP_0;
        else if (value == -1)
            return OP_1NEGATE;
        else
            return value - 1 + OP_1;
    }

}
