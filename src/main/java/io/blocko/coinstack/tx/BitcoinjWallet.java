package io.blocko.coinstack.tx;

import java.util.LinkedList;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionInput;
import org.bitcoinj.core.TransactionOutput;
import org.bitcoinj.core.Wallet;
import org.bitcoinj.wallet.CoinSelection;
import org.bitcoinj.wallet.CoinSelector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitcoinjWallet extends Wallet {

	private static final Logger log = LoggerFactory.getLogger(BitcoinjWallet.class);
	private static final long serialVersionUID = -597029543999240041L;
	
	public int maxTxSize = Constant.DEFAULT_MAX_TX_SIZE;
	
	@Deprecated
	/**
	 * Deprecated. Use {@link #setMaxTxSize(int)} instead.
	 * @param allowLargePayload
	 */
	public void allowLargePayload(boolean allowLargePayload) {
		this.maxTxSize = allowLargePayload
				? Constant.EXTENDED_MAX_TX_SIZE
				: Constant.DEFAULT_MAX_TX_SIZE;
	}
	
	/**
	 * Set maximum size of transaction.
	 * @param maxTxSize
	 * 			size of bytes to set
	 * 
	 * @see Constant.DEFAULT_MAX_TX_SIZE
	 * @see ExceededMaxTransactionSize
	 */
	public void setMaxTxSize(int maxTxSize) {
		this.maxTxSize = maxTxSize;
	}
	
	
	public BitcoinjWallet(NetworkParameters params) {
		super(params);
	}
	
	
	@Override
	public void completeTx(SendRequest req) throws InsufficientMoneyException {
		Coin value = Coin.ZERO;
		for (TransactionOutput output : req.tx.getOutputs()) {
			value = value.add(output.getValue());
		}

		log.info("Completing send tx with {} outputs totalling {} (not including fees)",
				req.tx.getOutputs().size(), value.toFriendlyString());

		// If any inputs have already been added, we don't need to get their value from wallet
		Coin totalInput = Coin.ZERO;
		for (TransactionInput input : req.tx.getInputs())
			if (input.getConnectedOutput() != null)
				totalInput = totalInput.add(input.getConnectedOutput().getValue());
			else
				log.warn("SendRequest transaction already has inputs but we don't know how much they are worth - they will be added to fee.");
		value = value.subtract(totalInput);

		ChangeCalculation result = calculateChangeOutput(req, value);
		CoinSelection coinSelection = result.coinSelection;
		TransactionOutput changeOutput = result.changeOutput;

		for (TransactionOutput output : coinSelection.gathered) {
			req.tx.addInput(output);
		}

		if (changeOutput != null) {
			req.tx.addOutput(changeOutput);
			log.info("  with {} change", changeOutput.getValue().toFriendlyString());
		}

		// Now sign the inputs, thus proving that we are entitled to redeem the connected outputs.
		if (req.signInputs) {
			signTransaction(req);
		}

		// Check size.
		int size = req.tx.bitcoinSerialize().length;
		if (size > maxTxSize) {
			throw new ExceededMaxTransactionSize();
		}

		final Coin calculatedFee = req.tx.getFee();
		if (calculatedFee != null) {
			log.info("  with a fee of {} BTC", calculatedFee.toFriendlyString());
		}
	}

	public static class ChangeCalculation {
		public CoinSelection coinSelection;
		public TransactionOutput changeOutput;
	}
	public ChangeCalculation calculateChangeOutput(SendRequest req, Coin value)
			throws InsufficientMoneyException {
		Coin fee = req.fee != null ? req.fee : Transaction.REFERENCE_DEFAULT_MIN_TX_FEE;
		Coin valueNeeded = value.add(fee);
		
		LinkedList<TransactionOutput> candidates = calculateAllSpendCandidates(true);
		CoinSelector selector = req.coinSelector == null ? coinSelector : req.coinSelector;
		CoinSelection selection = selector.select(valueNeeded, candidates);
		
		Coin change = selection.valueGathered.subtract(valueNeeded);
		long changeValue = change.getValue();
		TransactionOutput changeOutput = null; // change.eaquals(Coin.ZERO)
		if (changeValue > 0) {
			Address changeAddress = req.changeAddress;
			if (changeAddress == null)
				changeAddress = getChangeAddress();
			changeOutput = new TransactionOutput(params, req.tx, change, changeAddress);
		}
		else if (changeValue < 0) {
			Coin valueMissing = valueNeeded.subtract(selection.valueGathered);
			throw new InsufficientMoneyException(valueMissing);
		}
		
		ChangeCalculation result = new ChangeCalculation();
		result.coinSelection = selection;
		result.changeOutput = changeOutput;
		return result;
	}

}
