package io.blocko.coinstack.auth;

import io.blocko.coinstack.model.CredentialsProvider;

public class SimpleCredentialsProvider extends CredentialsProvider {
	private final String accessKey;
	private final String secretKey;
	
	public SimpleCredentialsProvider(String accessKey, String secretKey) {
		this.accessKey = accessKey;
		this.secretKey = secretKey;
	}
	
	@Override
	public String getAccessKey() {
		return accessKey;
	}
	
	@Override
	public String getSecretKey() {
		return secretKey;
	}
}
