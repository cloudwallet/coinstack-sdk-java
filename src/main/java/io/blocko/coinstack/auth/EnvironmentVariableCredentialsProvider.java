package io.blocko.coinstack.auth;

import java.util.Map;

import io.blocko.coinstack.model.CredentialsProvider;

public class EnvironmentVariableCredentialsProvider extends CredentialsProvider {
	
	public static final String COINSTACK_ACCESS_KEY = "COINSTACK_ACCESS_KEY";
	public static final String COINSTACK_SECRET_KEY = "COINSTACK_SECRET_KEY";
	public static final String LEGACY_ACCESS_KEY = "COINSTACK_ACCESS_KEY_ID";
	public static final String LEGACY_SECRET_KEY = "COINSTACK_SECRET_ACCESS_KEY";
	
	@Override
	public String getAccessKey() {
		Map<String, String> env = System.getenv();
		String access_key = env.get(COINSTACK_ACCESS_KEY);
		if (access_key == null || access_key.isEmpty()) {
			access_key = env.get(LEGACY_ACCESS_KEY);
		}
		return access_key;
	}

	@Override
	public String getSecretKey() {
		Map<String, String> env = System.getenv();
		String secret_key = env.get(COINSTACK_SECRET_KEY);
		if (secret_key == null || secret_key.isEmpty()) {
			secret_key = env.get(LEGACY_SECRET_KEY);
		}
		return secret_key;
	}

}
