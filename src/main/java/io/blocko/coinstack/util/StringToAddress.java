package io.blocko.coinstack.util;

import java.io.UnsupportedEncodingException;

import org.bitcoinj.core.Base58;
import org.bitcoinj.core.Utils;

public class StringToAddress {

	/*
	 * This function generates a Bitcoin Address using a given information as a
	 * seed. This can be used to get like a hash of user information. For
	 * example, in case of openkeychain, you can generate an unique user's
	 * address using the user's social security number.
	 */
	public static String convert(String id) {
		try {
			byte[] hashedInfo = Utils.sha256hash160(id.getBytes("utf-8"));
			// A stringified buffer is:
			// 1 byte version + data bytes + 4 bytes check code (a truncated hash)
			byte[] addressBytes = new byte[1 + hashedInfo.length + 4];
			// append mainnet header
			addressBytes[0] = (byte) 0;
			// append an address body, which is derived from user id
			System.arraycopy(hashedInfo, 0, addressBytes, 1, hashedInfo.length);

			// calculate checksum
			byte[] partialAddressBytes = new byte[1 + hashedInfo.length];
			System.arraycopy(addressBytes, 0, partialAddressBytes, 0, partialAddressBytes.length);
			byte[] checksum = Utils.doubleDigest(partialAddressBytes);
			// append checksum
			System.arraycopy(checksum, 0, addressBytes, hashedInfo.length + 1, 4);

			return Base58.encode(addressBytes);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}
}