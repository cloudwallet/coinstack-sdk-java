package io.blocko.coinstack.util;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Block;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.Input;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.model.Output.MetaData;

public class JsonToModel {
	
	public static CoinStackException parseError(JSONObject resJson, int status)
			throws JSONException {
		String type = resJson.getString("error_type");
		int code = resJson.getInt("error_code");
		String message = resJson.getString("error_message");
		boolean retry = resJson.getBoolean("retry");
		String cause = resJson.optString("error_cause", "");
		return new CoinStackException(type, code, status,
				message + " : " + shorten(cause), retry, "");
	}

	public static String shorten(String msg) {
		return shorten(msg, 512);
	}
	public static String shorten(String msg, int max) {
		if (msg == null) {
			return msg;
		}
		int len = msg.length();
		if (len < max || len < (max + (max/8))) {
			return msg;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(msg.substring(0, max));
		sb.append(" ... (len=").append(len).append(")");
		return sb.toString();
	}
	

	public static BlockchainStatus parseStatus(JSONObject resJson) throws JSONException {

		String bestHash = resJson.getString("best_block_hash");
		int bestHeight = resJson.getInt("best_height");

		return new BlockchainStatus(bestHeight, bestHash);
	}

	public static Block parseBlock(JSONObject resJson) throws JSONException {
		String[] txIds;
		JSONArray childJsons = resJson.getJSONArray("transaction_list");
		txIds = new String[childJsons.length()];
		for (int i = 0; i < childJsons.length(); i++) {
			txIds[i] = childJsons.getString(i);
		}
		String parentId;
		if (resJson.isNull("parent")) {
			parentId = null;
		} else {
			parentId = resJson.getString("parent");
		}
		JSONArray children = resJson.optJSONArray("children");
		String[] childIds = null;
		if (children != null) {
			int len = children.length();
			childIds = new String[len];
			for (int x = 0; x < len; x++) {
				childIds[x] = children.optString(x, null);
			}
		}
		return new Block(DateTime.parse(resJson.getString("confirmation_time")).toDate(),
				resJson.getString("block_hash"), childIds, resJson.getInt("height"), parentId, txIds);
	}

	public static Transaction parseTransaction(String transactionId, JSONObject resJson) throws JSONException {
		String[] blockIds;
		int[] blockHeights;
		Input[] inputs;
		Output[] outputs;

		JSONArray transactionBlockId = resJson.getJSONArray("block_hash");
		blockIds = new String[transactionBlockId.length()];
		for (int i = 0; i < transactionBlockId.length(); i++) {
			blockIds[i] = transactionBlockId.getJSONObject(i).getString("block_hash");
		}

		blockHeights = new int[transactionBlockId.length()];
		for (int i = 0; i < transactionBlockId.length(); i++) {
			blockHeights[i] = transactionBlockId.getJSONObject(i).getInt("block_height");
		}

		JSONArray transactionInputs = resJson.getJSONArray("inputs");
		inputs = new Input[transactionInputs.length()];
		for (int i = 0; i < transactionInputs.length(); i++) {
			JSONObject txInput = transactionInputs.getJSONObject(i);
			int outIndex = txInput.getInt("output_index");
			String outTxId = txInput.getString("transaction_hash");
			JSONArray addrs = txInput.optJSONArray("address");
			String addr = null;
			if (addrs != null && addrs.length() > 0) {
				addr = addrs.optString(0, null);
			}
			String sval = txInput.optString("value");
			long value = 0;
			if (sval != null && !sval.isEmpty()) {
				value = Long.valueOf(sval);
			}
			inputs[i] = new Input(outIndex, addr, outTxId, value);
		}

		JSONArray transactionOutputs = resJson.getJSONArray("outputs");
		outputs = new Output[transactionOutputs.length()];
		for (int i = 0; i < transactionOutputs.length(); i++) {
			JSONObject transactionOutput = transactionOutputs.getJSONObject(i);
			if (transactionOutput.has("address")) {
				outputs[i] = new Output(transactionId, i, transactionOutput.getJSONArray("address").getString(0),
						transactionOutput.getBoolean("used"), transactionOutput.getLong("value"),
						transactionOutput.getString("script"));
			} else {
				outputs[i] = new Output(transactionId, i, null, transactionOutput.getBoolean("used"),
						transactionOutput.getLong("value"), transactionOutput.getString("script"));
			}

		}

		Transaction tx = new Transaction(resJson.getString("transaction_hash"), blockIds,
				DateTime.parse(resJson.getString("time")).toDate(), resJson.getBoolean("coinbase"), inputs, outputs);
		tx.setBlockHeights(blockHeights);

		return tx;
	}

	public static String[] parseTransactionArray(JSONArray resJson) throws JSONException {
		List<String> transactions = new LinkedList<String>();
		for (int i = 0; i < resJson.length(); i++) {
			transactions.add(resJson.getString(i));
		}

		return transactions.toArray(new String[0]);
	}

	public static long parseBalance(JSONObject resJson) throws JSONException {

		return resJson.getLong("balance");
	}

	public static Output[] parseUnspentOutputs(String address, JSONArray resJson) throws JSONException {
		List<Output> outputs = new LinkedList<Output>();
		
		for (int i = 0; i < resJson.length(); i++) {
			JSONObject output = resJson.getJSONObject(i);
			// System.out.println(output.toString());
			MetaData metadata = null;
			if (!output.isNull("metadata")) {
				JSONObject metadataJson1 = output.getJSONObject("metadata");
				if (!metadataJson1.isNull("openassets")) {
					JSONObject metadataJson = metadataJson1.getJSONObject("openassets");
					if (!metadataJson.getString("output_type").equals("UNCOLORED")) {
						metadata = new MetaData(metadataJson.getString("output_type"),
								metadataJson.getString("asset_id"), metadataJson.getLong("quantity"));
					}
				}
			}
			outputs.add(new Output(output.getString("transaction_hash"), output.getInt("index"), address, false,
					output.getLong("value"), output.getString("script"), metadata));
		}
		
		return outputs.toArray(new Output[0]);
	}
}
