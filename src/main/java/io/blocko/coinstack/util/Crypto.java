package io.blocko.coinstack.util;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import io.blocko.coinstack.Info;

public class Crypto {
	/*
	 * !!!WARNING!!!
	 * Do not change constant values without upgrading a logic.
	 * It must go with btcd.
	 */
	public static final int CRYPTO_VERSION = 3;

	/*
	 * 2017 NIST guide (sec5.1.1.2) : SALT at least 32 bits
	 */
	public static final int SALT_SIZE = 32;
	/*
	 * 2017 NIST guide (sec5.1.1.2) : at least 10,000 iterations
	 */
	public static final int PBKDF2_ITER_CNT = 10000;
	public static final int AES_BLOCK_SIZE = 16;
	/*
	 * AES Key Length (bytes)
	 * 
	 * 	- AES128 = 16 = 128 bits
	 * 	- AES192 = 24 = 192 bits 
	 * 	- AES256 = 32 = 256 bits
	 */
	public static final int AES256_KEY_LENGTH = 32;

	/*
	 * !!!WARNING!!!
	 * Do not change enumeration values.
	 * Can add a value and a type, BUT don't change.
	 */
	public enum Type {
		//@formatter:off
		AES_256_CBC_HMAC(1), 				// hash(password) in the cipher text
		AES_256_CBC_PBKDF2(2), 				// loop(hash(password)) in the cipher text
		AES_256_CBC_SIMPLE(3), 				// hash(password) in the cipher text, no hash for encryption

		// !! start DEPRECATED !!
		AES_256_CBC_ANCIENT(-1), 			// version 1, which has a bug when try to decipher
		AES_256_CBC_ANCIENT_NODIGEST(-2), 	// version 2, which has a bug when try to decipher
		// !! end DEPRECATED !!

		NULL(0);
		//@formatter:on

		private int val;

		Type(int val) {
			this.val = val;
		}

		public int getValue() {
			return val;
		}
	}

	public static Type DEFAULT_TYPE = Type.AES_256_CBC_HMAC;

	String password;
	private SecureRandom srand;
	private Type type;

	private byte[] salt;
	private IvParameterSpec ivSpec;
	private byte[] skhash;
	private Cipher cipher;
	// deciphered type value
	private Type decipheredType;

	public Crypto(String password) {
		this(password, Type.NULL);
	}

	public Crypto(String password, Type type) {
		this.password = password;
		this.srand = new SecureRandom();

		switch (type) {
		case AES_256_CBC_HMAC:
		case AES_256_CBC_PBKDF2:
		case AES_256_CBC_SIMPLE:
			this.type = type;
			break;
		default:
			this.type = Type.NULL;
		}
	}

	public byte[] encrypt(byte[] plain) throws GeneralSecurityException {
		if (password == null || password.isEmpty()) {
			throw new GeneralSecurityException("need password to encrypt");
		}

		if (type == Type.NULL) {
			type = Info.getCryptoType();
		}

		switch (type) {
		case AES_256_CBC_HMAC:
			preparePBKDF2(1, AES256_KEY_LENGTH * 8);
			return encryptAES256withCBC(plain);
		case AES_256_CBC_PBKDF2:
			preparePBKDF2(PBKDF2_ITER_CNT, AES256_KEY_LENGTH * 8);
			return encryptAES256withCBC(plain);
		case AES_256_CBC_SIMPLE:
			prepareSimple256();
			return encryptAES256withCBC(plain);

		default:
			throw new GeneralSecurityException("unsupported crypto type");
		}
	}

	/*
	 * PBKDF2 is a standard in RFC2898 and PKCS#5.
	 * http://www.ietf.org/rfc/rfc2898.txt
	 */
	private void preparePBKDF2(int loop, int keyLen) throws NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		if (salt == null || salt.length != SALT_SIZE) {
			// prepare salt
			salt = new byte[SALT_SIZE];
			srand.nextBytes(salt);
		}

		if (ivSpec == null) {
			// prepare IV
			ivSpec = generateRandomIVSpec();
		}

		// derive hash value to compare the password
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, loop, keyLen);
		skhash = skf.generateSecret(keySpec).getEncoded();

		// derive the secret key for the cipher key
		byte[] pwbytes = password.getBytes(Charset.forName("UTF-8"));
		byte[] inputs = new byte[pwbytes.length + salt.length];
		System.arraycopy(pwbytes, 0, inputs, 0, pwbytes.length);
		System.arraycopy(salt, 0, inputs, pwbytes.length, salt.length);
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		SecretKey secretKey = new SecretKeySpec(digest.digest(inputs), "AES");

		// prepare cipher to encrypt
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
	}

	private void prepareSimple256() throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		byte[] pwbytes = password.getBytes(Charset.forName("UTF-8"));
		int pwlen = pwbytes.length;
		if (pwlen > 32) {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			pwbytes = digest.digest(pwbytes);
		} else if (pwlen < 32) {
			// make 32 bytes new password                                                                                                                                                                                                                         
			byte[] pw32bytes = new byte[32];
			System.arraycopy(pwbytes, 0, pw32bytes, 0, pwlen);

			// append a random salt to fit 32 bytes                                                                                                                                                                                                               
			salt = new byte[32 - pwbytes.length];
			srand.nextBytes(salt);
			System.arraycopy(salt, 0, pw32bytes, pwlen, salt.length);

			pwbytes = pw32bytes;
		}

		if (ivSpec == null) {
			// prepare IV                                                                                                                                                                                                                                         
			ivSpec = generateRandomIVSpec();
		}

		if (pwlen > 32) {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 1, 32 * 8);
			skhash = skf.generateSecret(keySpec).getEncoded();
		} else {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			skhash = digest.digest(pwbytes);
		}

		// derive the secret key for the cipher key                                                                                                                                                                                                                   
		SecretKey secretKey = new SecretKeySpec(pwbytes, "AES");

		// prepare cipher to encrypt
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
	}

	private IvParameterSpec generateRandomIVSpec() {
		// get a random IV
		byte[] iv = new byte[AES_BLOCK_SIZE];
		srand.nextBytes(iv);
		return new IvParameterSpec(iv);
	}

	public byte[] getSalt() {
		return salt;
	}

	public byte[] getIv() {
		if (ivSpec == null) {
			return null;
		}
		return ivSpec.getIV();
	}
	
	public byte[] getSkHash() {
		return skhash;
	}

	public Type getType() {
		return type;
	}

	private byte[] encryptAES256withCBC(byte[] plain) throws GeneralSecurityException {
		// encrypt the text
		byte[] cbytes = cipher.doFinal(plain);

		// salt
		int saltLen = SALT_SIZE;
		if (type == Type.AES_256_CBC_SIMPLE) {
			// if 'SIMPLE', salt length is changeable.
			saltLen = salt.length;
		}

		// iv
		byte[] iv = ivSpec.getIV();

		// data chunk size = crypto_version (4) + crypto_type (4)
		//					 + salt (saltLen)
		//					 + secret_key_hash (AES256_KEY_LENGTH)
		//					 + iv (AES_BLOCK_SIZE)
		//					 + encrypted_data
		int size = 4 + 4 + saltLen + AES256_KEY_LENGTH + AES_BLOCK_SIZE + cbytes.length;

		// generate encrypted data chunk
		ByteBuffer bbuf = ByteBuffer.allocate(size);
		bbuf.order(ByteOrder.LITTLE_ENDIAN);
		bbuf.putInt(CRYPTO_VERSION);
		bbuf.putInt(type.getValue());
		if (saltLen > 0) {
			bbuf.put(salt);
		}
		bbuf.put(skhash);
		bbuf.put(iv);
		bbuf.put(cbytes);
		return bbuf.array();
	}

	public byte[] decrypt(byte[] encbytes) throws GeneralSecurityException {
		if (password == null || password.isEmpty()) {
			throw new GeneralSecurityException("need password to encrypt");
		}

		byte[] cbytes = null;
		try {
			cbytes = prepareDecipher(encbytes);
			type = decipheredType;
			return decryptAES256withCBC(cbytes);
		} catch (Exception e) {
			decipheredType = Type.NULL;
			throw new GeneralSecurityException("fail to decrypt: " + e.getMessage(), e);
		}
	}

	private byte[] prepareDecipher(byte[] encbytes) throws BufferUnderflowException, GeneralSecurityException {
		ByteBuffer bbuf = ByteBuffer.wrap(encbytes);
		bbuf.order(ByteOrder.LITTLE_ENDIAN);

		// strip off crypto version
		int cryptoVer = bbuf.getInt();
		if (cryptoVer != CRYPTO_VERSION) {
			throw new GeneralSecurityException("crypto version is wrong");
		}

		// strip off type
		int itype = bbuf.getInt();

		// verify crypto type                                                                                                                                                                                                                                         
		byte[] pwbytes = password.getBytes(Charset.forName("UTF-8"));
		int pwlen = pwbytes.length;
		int loop = 0;
		int keyLen = 0;
		int saltSize = 0;
		if (type == Type.NULL || type.getValue() == itype) {
			decipheredType = Type.NULL;
			for (Type t : Type.values()) {
				if (t.getValue() == itype) {
					decipheredType = t;
					break;
				}
			}

			if (decipheredType == Type.AES_256_CBC_HMAC) {
				loop = 1;
				keyLen = AES256_KEY_LENGTH;
				saltSize = SALT_SIZE;
			} else if (decipheredType == Type.AES_256_CBC_PBKDF2) {
				loop = PBKDF2_ITER_CNT;
				keyLen = AES256_KEY_LENGTH;
				saltSize = SALT_SIZE;
			} else if (decipheredType == Type.AES_256_CBC_SIMPLE) {
				loop = 1;
				keyLen = AES256_KEY_LENGTH;
				if (pwlen < 32) {
					saltSize = 32 - pwlen;
				}
			} else {
				decipheredType = Type.NULL;
				throw new GeneralSecurityException("cannot recognize the crypto type");
			}
		} else {
			throw new GeneralSecurityException("different crypto type");
		}

		// strip off salt
		salt = new byte[saltSize];
		bbuf.get(salt);

		// strip off secret key hash
		skhash = new byte[keyLen];
		bbuf.get(skhash);

		if (decipheredType == Type.AES_256_CBC_SIMPLE) {
			// generate the secret key                                                                                                                                                                                                                            
			if (pwlen > 32) {
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				pwbytes = digest.digest(pwbytes);
			} else if (pwlen < 32) {
				if (pwlen + salt.length != 32) {
					throw new GeneralSecurityException("password is improper");
				}

				// make 32 bytes new password                                                                                                                                                                                                                 
				byte[] pw32bytes = new byte[32];
				System.arraycopy(pwbytes, 0, pw32bytes, 0, pwlen);

				// append the salt to fit 32 bytes                                                                                                                                                                                                            
				System.arraycopy(salt, 0, pw32bytes, pwlen, salt.length);

				pwbytes = pw32bytes;
			}

			// verify the secret key hash                                                                                                                                                                                                                         
			if (pwlen > 32) {
				SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
				PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 1, 32 * 8);
				if (!Arrays.equals(skhash, skf.generateSecret(keySpec).getEncoded())) {
					throw new GeneralSecurityException("the secret key is different");
				}
			} else {
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				if (!Arrays.equals(skhash, digest.digest(pwbytes))) {
					throw new GeneralSecurityException("the secret key is different");
				}
			}
		} else {
			// generate the secret key                                                                                                                                                                                                                            
			byte[] inputs = new byte[pwbytes.length + salt.length];
			System.arraycopy(pwbytes, 0, inputs, 0, pwbytes.length);
			System.arraycopy(salt, 0, inputs, pwbytes.length, salt.length);
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			pwbytes = digest.digest(inputs);

			// verify the secret key hash                                                                                                                                                                                                                         
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, loop, keyLen * 8);
			if (!Arrays.equals(skhash, skf.generateSecret(keySpec).getEncoded())) {
				throw new GeneralSecurityException("the secret key is different");
			}
		}

		// strip off iv
		byte[] iv = new byte[AES_BLOCK_SIZE];
		bbuf.get(iv);
		ivSpec = new IvParameterSpec(iv);

		// get cipher text (encrypted text bytes)
		int cbytesSize = bbuf.capacity() - 4 - 4 - saltSize - keyLen - AES_BLOCK_SIZE;
		byte[] cbytes = new byte[cbytesSize];
		bbuf.get(cbytes);

		// derive the secret key for the cipher key
		SecretKey secretKey = new SecretKeySpec(pwbytes, "AES");

		// prepare cipher to decipher
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);

		return cbytes;
	}

	private byte[] decryptAES256withCBC(byte[] cbytes) throws GeneralSecurityException {
		return cipher.doFinal(cbytes);
	}

	private static boolean onCryptoCache = false;
	private static HashMap<String, Crypto> cryptoCache = new HashMap<String, Crypto>();

	public static void turnOnCryptoCache() {
		onCryptoCache = true;
	}

	public static void turnOffCryptoCache() {
		onCryptoCache = false;
	}

	public static boolean isOnCryptoCache() {
		if (Info.useCryptCache()) {
			onCryptoCache = true;
		}
		return onCryptoCache;
	}

	/*
	 * FIXME:
	 *  		'password' is using as a key of the crypto cache.
	 * 		It could occur a security issue.
	 * 		Need to hide 'password'.
	 */
	public static Crypto getCrypto(String password) throws GeneralSecurityException {
		if (onCryptoCache || Info.useCryptCache()) {
			onCryptoCache = true;
			synchronized (cryptoCache) {
				if (cryptoCache.containsKey(password))
					return cryptoCache.get(password);
				else {
					Crypto c = new Crypto(password);
					cryptoCache.put(password, c);
					return c;
				}
			}
		}
		return new Crypto(password);
	}

	public static byte[] encryptAES256CBC(byte[] plain, String password) throws GeneralSecurityException {
		Crypto crypto = getCrypto(password);
		return crypto.encrypt(plain);
	}

	public static byte[] decryptAES256CBC(byte[] encbytes, String password) throws GeneralSecurityException {
		Crypto crypto = getCrypto(password);
		return crypto.decrypt(encbytes);
	}
}