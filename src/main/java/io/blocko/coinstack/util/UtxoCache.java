package io.blocko.coinstack.util;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bitcoinj.core.TransactionInput;
import org.bitcoinj.core.Utils;
import org.bitcoinj.params.MainNetParams;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.tx.BitcoinjWallet;

public class UtxoCache {

	private static final int DEFAULT_CACHE_SIZE = 1000;
	private static final int PROPOSE_MAP_SIZE_MULTIPLY = 2;
	private static final int CACHE_ITEM_EXP_MIN = 1;

	private class ProposeItem {
		protected String address;
		protected String txId;
		protected List<org.bitcoinj.core.TransactionInput> inputs;
		protected List<org.bitcoinj.core.TransactionOutput> utxos;

		protected ProposeItem(String address, String txId, List<org.bitcoinj.core.TransactionInput> inputs,
				List<org.bitcoinj.core.TransactionOutput> utxos) {
			this.address = address;
			this.txId = txId;
			this.inputs = inputs;
			this.utxos = utxos;
		}
	}

	private Cache<String, Output[]> addrUtxoCache;
	private Cache<Integer, ProposeItem> proposeCache;
	private Cache<String, BitcoinjWallet> walletCache;

	public UtxoCache() {
		this(DEFAULT_CACHE_SIZE);
	}
	
	public UtxoCache(int cache_size) {
		addrUtxoCache = CacheBuilder.newBuilder().expireAfterAccess(CACHE_ITEM_EXP_MIN, TimeUnit.MINUTES)
				.maximumSize(cache_size).build();
		proposeCache = CacheBuilder.newBuilder().expireAfterAccess(CACHE_ITEM_EXP_MIN, TimeUnit.MINUTES)
				.maximumSize(cache_size * PROPOSE_MAP_SIZE_MULTIPLY).build();
		walletCache = CacheBuilder.newBuilder().expireAfterAccess(CACHE_ITEM_EXP_MIN, TimeUnit.MINUTES)
				.maximumSize(cache_size).build();
	}

	private boolean isEnough(Output[] utxo, long requiredValue, long fee) {
		// cache is empty
		if (null == utxo)
			return false;

		// calculate total balance to fetch more utxos when more balance is
		// required
		long totalValue = 0L;
		for (Output output : utxo) {
			totalValue += output.getValue();
		}

		return (totalValue >= (requiredValue + fee));
	}

	public void put(String address, Output[] outputs) {
		addrUtxoCache.put(address, outputs);
	}

	public Output[] get(CoinStackClient client, String address, long totalValue, long fee)
			throws IOException, CoinStackException {
		Output[] utxo = addrUtxoCache.getIfPresent(address);

		// when a cache does not have
		if (false == isEnough(utxo, totalValue, fee)) {
			return client.getUnspentOutputs(address);
		}

		return utxo;
	}

	public long size() {
		return addrUtxoCache.size();
	}

	public void propose(String address, String txId, String rawTxStr,
			List<org.bitcoinj.core.TransactionInput> txInputList,
			List<org.bitcoinj.core.TransactionOutput> txOutputList) {
		// generate hash code from rawTx
		int hashKey = rawTxStr.hashCode();

		proposeCache.put(hashKey, new ProposeItem(address, txId, txInputList, txOutputList));
	}

	public void commit(String rawTxStr) {

		int hashKey = rawTxStr.hashCode();

		ProposeItem propose = proposeCache.getIfPresent(hashKey);

		if (propose != null) {
			// convert type
			List<Output> outputList = new LinkedList<Output>();

			Output[] previousOuts = addrUtxoCache.getIfPresent(propose.address);
			for (Output output : previousOuts) {

				boolean isUsed = false;
				String convTxId = CoinStackClient.convertEndianness(output.getTransactionId());
				
				for (TransactionInput txInput : propose.inputs) {
					if(0 == txInput.getOutpoint().getHash().toString().compareTo(convTxId) &&
							txInput.getOutpoint().getIndex() == output.getIndex()) {
						isUsed = true;
						break;
					}
				}
				
				if(!isUsed) {
					outputList.add(output);
				}
			}

			for (int i = 0; i < propose.utxos.size(); i++) {

				org.bitcoinj.core.TransactionOutput utxo = propose.utxos.get(i);

				// skip data tx
				if(false == utxo.getScriptPubKey().isSentToAddress())
					continue;
				
				String targetAddress = utxo.getScriptPubKey().getToAddress(MainNetParams.get()).toString();

				// add to cache when its belongs to me
				if (0 == propose.address.compareTo(targetAddress)) {
					String endianConv = CoinStackClient.convertEndianness(propose.txId);
					outputList.add(new Output(endianConv, i, targetAddress, false, utxo.getValue().value,
							Utils.HEX.encode(utxo.getScriptBytes())));
				}
			}

			Output[] convertedArray = outputList.toArray(new Output[0]);
			addrUtxoCache.put(propose.address, convertedArray);

		}
	}
	
	public void putWallet(String address, BitcoinjWallet cachedWallet) {
		this.walletCache.put(address, cachedWallet);
	}
	public BitcoinjWallet getWallet(String address) {
		return this.walletCache.getIfPresent(address);
	}
}