package io.blocko.coinstack.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Base58;
import org.spongycastle.crypto.digests.RIPEMD160Digest;

import io.blocko.coinstack.ECKey;
import io.blocko.coinstack.exception.MalformedInputException;

public class Codecs {
	
	public static final byte VERSION_MAINNET = (byte) 0;
	public static final byte VERSION_TESTNET = (byte) 111;
	
	
	/**
	 * BinaryCodec provides binary-to-text encoding and text-to-binary decoding.
	 * 
	 */
	public interface BinaryCodec {
		/**
		 * Encodes binary data into a string.
		 * 
		 * @param data
		 * 			an array of bytes containing binary data.
		 * @return
		 * 			a string containing only characters declared in the codec.
		 */
		public String encode(byte[] data);
		/**
		 * Decodes a string into binary data.
		 * 
		 * @param data
		 * 			a string containing only characters declared in the codec.
		 * @return
		 * 			an array of bytes containing binary data.
		 */
		public byte[] decode(String data);
		/**
		 * Decodes a string into binary data and converts it to a string.
		 * 
		 * @param data
		 * 			a string containing only characters declared in the codec.
		 * @return
		 * 			a string converted from an array of bytes containing binary data.
		 */
		public String decodeToString(String data);
		/**
		 * Decodes a string into binary data and converts it to a BigInteger.
		 * 
		 * @param data
		 * 			a string containing only characters declared in the codec.
		 * @return
		 * 			a BigInteger converted from an array of bytes containing binary data.
		 */
		public BigInteger decodeToBigInteger(String data);
	}
	
	/**
	 * DigestCodec provides a hash computation.
	 * 
	 */
	public interface DigestCodec {
		/**
		 * Computes a hash of a specified array of bytes,
		 * starting at the specified offset.
		 * 
		 * @param data
		 * 			an array of bytes to compute a hash.
		 * @param offset
		 * 			an offset to start from in the array of bytes.
		 * @param len
		 * 			a number of bytes to use, starting at the offset.
		 * @return
		 * 			an array of bytes for the hash computation result.
		 */
		public byte[] digest(byte[] data, int offset, int len);
		/**
		 * Computes a hash of an array of bytes.
		 * 
		 * @param data
		 * 			an array of bytes to compute a hash.
		 * @return
		 * 			an array of bytes for the hash computation result.
		 */
		public byte[] digest(byte[] data);
		/**
		 * Computes a hash of an array of bytes,
		 * and encodes the result with HEX.
		 * 
		 * @param data
		 * 			an array of bytes.
		 * @return
		 * 			a hex-encoded string for the hash computation result.
		 */
		public String digestEncodeHex(byte[] data);
		/**
		 * Computes a hash of an array of bytes,
		 * and encodes the result with BASE64.
		 * 
		 * @param data
		 * 			an array of bytes.
		 * @return
		 * 			a base64-encoded string for the hash computation result.
		 */
		public String digestEncodeBase64(byte[] data);
		/**
		 * Computes a hash of an array of bytes,
		 * and get first 4 bytes
		 * 
		 * @param data
		 * 			an array of bytes.
		 * @param offset
		 * 			an offset to start from in the array of bytes.
		 * @param len
		 * 			a number of bytes to use, starting at the offset.
		 * @return
		 * 			an array of bytes for the hash computation result.
		 */
		public byte[] checksum(byte[] data, int offset, int len);
	}
	
	public static BinaryCodec HEX = BINARY.HEX;
	public static BinaryCodec BASE64 = BINARY.BASE64;
	public static BinaryCodec BASE58 = BINARY.BASE58;
	public static BinaryCodec BASE58CHECK = BINARY.BASE58CHECK;
	@Deprecated public static BinaryCodec BASE58CHECKED = BINARY.BASE58CHECK;
	
	public static DigestCodec HASH160 = DIGEST.HASH160;
	public static DigestCodec HASH256 = DIGEST.HASH256;
	public static DigestCodec SHA256 = DIGEST.SHA256;
	public static DigestCodec RIPEMD160 = DIGEST.RIPEMD160;
	public static DigestCodec MD5 = DIGEST.MD5;
	
	
	public static String encode(BinaryCodec codec, byte[] input) {
		return codec.encode(input);
	}
	public static byte[] decode(BinaryCodec codec, String input) {
		return codec.decode(input);
	}
	
	public static byte[] digest(DigestCodec codec, byte[] input) {
		return codec.digest(input);
	}
	
	
	/**
	 * Instance of BinaryCodec
	 */
	public enum BINARY implements BinaryCodec {
		HEX {
			@Override
			public String encode(byte[] data) {
				// org.bitcoinj.core.Utils.HEX
				// com.google.common.io.BaseEncoding.BaseEncoding.base16().lowerCase()
				// - String encode(byte[])
				// org.apache.commons.codec.binary.Hex
				// - char[] encodeHex(byte[])
				// - String encodeHexString(byte[])
				return Hex.encodeHexString(data);
				//return Utils.HEX.encode(input);
				//return BaseEncoding.base16().lowerCase().encode(input);
			}
			@Override
			public byte[] decode(String data) {
				// org.bitcoinj.core.Utils.HEX
				// com.google.common.io.BaseEncoding.BaseEncoding.base16().lowerCase()
				// - byte[] decode(CharSequence)
				// org.apache.commons.codec.binary.Hex
				// - byte[] decodeHex(char[]) throws DecoderException
				try {
					return Hex.decodeHex(data.toCharArray());
				} catch (DecoderException e) {
					throw new IllegalArgumentException(e);
				}
				//return Utils.HEX.decode(input);
				//return BaseEncoding.base16().lowerCase().decode(input);
			}
		},
		/**
		 * Provides Base64 encoding and decoding.
		 */
		BASE64 {
			@Override
			public String encode(byte[] data) {
				// org.apache.commons.codec.binary.Base64
				// - byte[] encodeBase64(byte[])
				// - String encodeBase64String(byte[])
				return Base64.encodeBase64String(data);
			}
			@Override
			public byte[] decode(String data) {
				// org.apache.commons.codec.binary.Base64
				// - byte[] decodeBase64(byte[])
				// - String decodeBase64(String)
				return Base64.decodeBase64(data);
			}
		},
		/**
		 * Provides Base58 encoding and decoding.
		 * Base58 uses "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz",
		 * excepts "0OIl" characters.
		 */
		BASE58 {
			@Override
			public String encode(byte[] data) {
				return Base58.encode(data);
			}
			@Override
			public byte[] decode(String data) {
				// org.bitcoinj.core.Base58
				// - byte[] decode(String input) throws AddressFormatException
				try {
					return Base58.decode(data);
				} catch (AddressFormatException e) {
					throw new IllegalArgumentException(e);
				}
			}
		},
		/**
		 * Provides Base58Checked encoding and decoding.
		 * In encoding, adds 4 bytes checksum to the end of data.
		 * In decoding, the 4 bytes at the end of data are checksum, so check them separately.
		 */
		BASE58CHECK {
			@Override
			public String encode(byte[] data) {
				// org.bitcoinj.core.Address extends VersionedChecksummedBytes
				// - toString()
				byte[] buffer = new byte[data.length+4];
				System.arraycopy(data, 0, buffer, 0, data.length);
				byte[] checksum = DIGEST.HASH256.checksum(buffer, 0, data.length);
				System.arraycopy(checksum, 0, buffer, data.length, checksum.length);
				return BASE58.encode(buffer);
			}
			@Override
			public byte[] decode(String data) {
				byte[] buffer = BASE58.decode(data);
				if (buffer.length < 4) {
					throw new IllegalArgumentException(new MalformedInputException(
							"Invalid format - too short"));
				}
				byte[] checksum = DIGEST.HASH256.checksum(buffer, 0, buffer.length-4);
				byte[] chkSrc = new byte[4];
				System.arraycopy(buffer, buffer.length-4, chkSrc, 0, 4);
				if (!Arrays.equals(chkSrc, checksum)) {
					throw new IllegalArgumentException(new MalformedInputException(
							"Invalid format - checksum not matched"));
				}
				byte[] res = new byte[buffer.length-4];
				System.arraycopy(buffer, 0, res, 0, buffer.length-4);
				return res;
			}
		},
		;
		
		public String decodeToString(String data) {
			byte[] buf = decode(data);
			return (buf == null) ? null : new String(buf);
		}
		public BigInteger decodeToBigInteger(String data) {
			// org.bitcoinj.core.Base58
			// - BigInteger decodeToBigInteger(String input) throws AddressFormatException
			byte[] buf = decode(data);
			return (buf == null) ? null : new BigInteger(1, buf);
		}
	}
	
	/**
	 * Instance of DigestCodec
	 */
	public enum DIGEST implements DigestCodec {
		/**
		 * Provides hash computation of combination RIPEMD and SHA-256.
		 */
		HASH160 {
			@Override
			public byte[] digest(byte[] data, int offset, int len) {
				byte[] sha256 = SHA256.digest(data, offset, len);
				byte[] hash160 = RIPEMD160.digest(sha256, 0, sha256.length);
				return hash160;
			}
		},
		/**
		 * Provides hash computation of double SHA-256.
		 */
		HASH256 {
			@Override
			public byte[] digest(byte[] data, int offset, int len) {
				byte[] sha256 = SHA256.digest(data, offset, len);
				byte[] hash256 = SHA256.digest(sha256, 0, sha256.length);
				return hash256;
			}
		},
		/**
		 * Provides SHA-256 hash computation.
		 */
		SHA256 {
			@Override
			public byte[] digest(byte[] data, int offset, int len) {
				try {
					MessageDigest digest = MessageDigest.getInstance("SHA-256");
					digest.update(data, offset, len);
					return digest.digest();
				} catch (NoSuchAlgorithmException e) {
					throw new RuntimeException(e);
				}
			}
		},
		/**
		 * Provides RIPEMD hash computation.
		 */
		RIPEMD160 {
			@Override
			public byte[] digest(byte[] data, int offset, int len) {
				RIPEMD160Digest digest = new RIPEMD160Digest();
				digest.update(data, offset, len);
				byte[] res = new byte[20];
				digest.doFinal(res, 0);
				return res;
			}
		},
		/**
		 * Provides MD5 hash computation.
		 */
		MD5 {
			@Override
			public byte[] digest(byte[] data, int offset, int len) {
				try {
					MessageDigest digest = MessageDigest.getInstance("MD5");
					digest.update(data, offset, len);
					return digest.digest();
				} catch (NoSuchAlgorithmException e) {
					throw new RuntimeException(e);
				}
			}
		},
		;
		
		/**
		 * Computes a hash of an array of bytes.
		 * 
		 * @param data
		 * 			an array of bytes to compute a hash.
		 * @return
		 * 			an array of bytes for the hash computation result.
		 */
		public byte[] digest(byte[] data) {
			return digest(data, 0, data.length);
		}
		/**
		 * Computes a hash of an array of bytes,
		 * and encodes the result with HEX.
		 * 
		 * @param data
		 * 			an array of bytes.
		 * @return
		 * 			a hex-encoded string for the hash computation result.
		 */
		public String digestEncodeHex(byte[] data) {
			return BINARY.HEX.encode(digest(data, 0, data.length));
		}
		/**
		 * Computes a hash of an array of bytes,
		 * and encodes the result with BASE64.
		 * 
		 * @param data
		 * 			an array of bytes.
		 * @return
		 * 			a base64-encoded string for the hash computation result.
		 */
		public String digestEncodeBase64(byte[] data) {
			return BINARY.BASE64.encode(digest(data, 0, data.length));
		}
		/**
		 * Get first 4 bytes of digested hash value to detect error.
		 */
		public byte[] checksum(byte[] data, int offset, int len) {
			byte[] hash = digest(data, offset, len);
			byte[] checksum = new byte[4];
			System.arraycopy(hash, 0, checksum, 0, 4);
			return checksum;
		}
	}
	
	
	
	///////////////////////
	
	public static byte[] versionedMainnet(byte[] data) {
		return versionedBytes(VERSION_MAINNET, data);
	}
	public static byte[] versionedBytes(byte version, byte[] data) {
		byte[] versioned = new byte[data.length+1];
		versioned[0] = version;
		System.arraycopy(data, 0, versioned, 1, data.length);
		return versioned;
	}
	public static byte[] unversionedBytes(byte[] data) {
		byte[] unversioned = new byte[data.length-1];
		System.arraycopy(data, 1, unversioned, 0, unversioned.length);
		return unversioned;
	}
	
	
	
	///////////////////////
	
	public static String encodeBase58CheckMainnet(byte[] data) {
		byte[] versioned = versionedBytes(VERSION_MAINNET, data);
		String encoded = BINARY.BASE58CHECK.encode(versioned);
		return encoded;
	}
	public static String encodeBase58CheckVersioned(byte versionByte, byte[] data) {
		byte[] versioned = versionedBytes(versionByte, data);
		String encoded = BINARY.BASE58CHECK.encode(versioned);
		return encoded;
	}
	public static byte[] decodeBase58CheckUnversioned(String data) {
		byte[] decoded = BINARY.BASE58CHECK.decode(data);
		byte[] unversioned = unversionedBytes(decoded);
		return unversioned;
	}
	
	
	
	///////////////////////
	
	/**
	 * Conversion of a byte array data into bitcoin address.
	 * Adds 1 byte for version in front of the data.
	 * Version is 'zero', as used in the bitcoin mainnet.
	 * 
	 * @param data
	 * 			an array of bytes to convert
	 * @return
	 * 			a string for bitcoin address
	 */
	public static String convertToAddress(byte[] data) {
		byte[] hash = DIGEST.HASH160.digest(data);
		byte[] versioned = versionedBytes(VERSION_MAINNET, hash);
		return BINARY.BASE58CHECK.encode(versioned);
		//return HASH160.digestEncodeBase58CheckMainnet(data);
	}
	
	/**
	 * See {@link #convertToAddress(byte[])}
	 * 
	 */
	public static String convertToAddress(String data) {
		return convertToAddress(data.getBytes());
		//return HASH160.digestEncodeBase58CheckMainnet(data.getBytes());
	}
	
	
	/**
	 * Conversion of a byte array data into private key.
	 * 
	 * @param data
	 * 			an array of bytes to convert
	 * @return
	 * 			a private key in the form of an integer
	 */
	public static BigInteger convertToPrivate(byte[] data) {
		byte[] hash = DIGEST.HASH256.digest(data);
		return new BigInteger(1, hash);
	}
	/**
	 * See {@link #convertToPrivate(byte[])}
	 * 
	 */
	public static BigInteger convertToPrivate(String data) {
		return convertToPrivate(data.getBytes());
	}
	
	/**
	 * See {@link #convertToPrivate(byte[])}
	 * See {@link io.blocko.coinstack.ECKey#fromPrivate(BigInteger)}
	 * 
	 */
	public static String convertToPrivateKey(byte[] data) {
		BigInteger privkey = convertToPrivate(data);
		return ECKey.fromPrivate(privkey);
	}
	/**
	 * See {@link #convertToPrivateKey(byte[])}
	 * 
	 */
	public static String convertToPrivateKey(String data) {
		return convertToPrivateKey(data.getBytes());
	}

}
