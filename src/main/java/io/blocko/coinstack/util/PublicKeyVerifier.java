package io.blocko.coinstack.util;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

import io.blocko.coinstack.AbstractEndpoint;

public class PublicKeyVerifier implements HostnameVerifier {
	private AbstractEndpoint endpoint;

	public PublicKeyVerifier(AbstractEndpoint endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public boolean verify(String host, SSLSession ssl) {
		// if endpoint key null, skip cert pinning
		if (endpoint.getPublicKey() == null) {
			return true;
		}
		
		X509Certificate cert = null;
		try {
			Certificate[] certificates = ssl.getPeerCertificates();
			cert = (X509Certificate) certificates[0]; // get first certificate
		} catch (SSLPeerUnverifiedException e) {
			return false;
		}
		
		if (cert != null && endpoint.getPublicKey().equals(cert.getPublicKey())) {
			// do nothing since match
			return true;
		}
		return false;
	}
}
