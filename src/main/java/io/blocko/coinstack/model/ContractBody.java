package io.blocko.coinstack.model;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.bitcoinj.core.ECKey;
import org.json.JSONObject;
import org.spongycastle.math.ec.ECPoint;

import io.blocko.coinstack.contract.AbstractContractCode;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.SecurityException;
import io.blocko.coinstack.util.Codecs;
import io.blocko.coinstack.util.Crypto;

public class ContractBody {
	public static final int CONTRACT_BODY_VERSION = 3;

	public static final String TYPE_ESC = "ESC"; // ethereum smart contract
	public static final String TYPE_LSC = "LSC"; // lua smart contract

	private String type;
	private AbstractContractCode code;

	/*
	 * Smart contract code is encrypted by an encrypted key (ECK),
	 * and the ECK is encrypted by designated sharing public keys.
	 *
	 * Using the private key corresponding to the input public key,
	 * can decipher the encrypted ECK, and then
	 * can decipher the encrypted smart contract code with ECK.
	 *
	 * If sharing public key list is empty and set an ECK,
	 * no one can decipher a smart contract code.
	 */
	// point list on the known elliptic curve
	// corresponding to sharing public keys
	private ArrayList<ECPoint> shrECPointList = new ArrayList<ECPoint>();

	public ContractBody(String type, AbstractContractCode code) {
		this.type = type;
		this.code = code;
	}

	public ContractBody(String type) {
		this(type, null);
	}

	public List<ECPoint> getShareECPointList() {
		return shrECPointList;
	}

	public boolean addSharePubKey(byte[] pubkey) {
		if (pubkey == null) {
			throw new NullPointerException("public key is null");
		}

		// derive a point on the known elliptic curve
		// corresponding to the input public key
		ECPoint point = ECKey.CURVE.getCurve().decodePoint(pubkey);
		if (point.isValid()) {
			shrECPointList.add(point);
			return true;
		}
		return false;
	}

	public String getType() {
		return type;
	}

	public AbstractContractCode getCode() {
		return code;
	}

	public void setCode(AbstractContractCode code) {
		this.code = code;
	}

	/**
	 * encrypt ECK which is used to encrypt the smart contract.
	 *
	 * @param pubkeyPoint
	 *            point on the elliptic curve corresponding to the public key
	 * @param privKeyD
	 *            D value of the elliptic curve
	 * @return encrypted ECK
	 * @throws GeneralSecurityException
	 */
	private byte[] encryptECK(ECPoint pubkeyPoint, BigInteger privKeyD) throws GeneralSecurityException {
		// multiply public key point and D value of the private key
		ECPoint mpoint = pubkeyPoint.multiply(privKeyD).normalize();
		return Crypto.encryptAES256CBC(code.getECK().getBytes(), mpoint.getXCoord().toString());
	}

	/**
	 * make a JSON string with the encrypted ECK array (EECKs). those are encrypted
	 * by the each point on the elliptic curve corresponding to sharing public keys.
	 *
	 * @return JSON string with EECK array
	 * @throws SecurityException
	 */
	private String makeEECKsJsonStr() throws SecurityException {
		if (shrECPointList.isEmpty()) {
			// no sharing public keys
			return "";
		}

		StringBuilder sb = new StringBuilder();

		// get and set an arbitrary public key
		// which will be used when to encrypt the ECK.
		// this public key is necessary when to decipher the encrypted ECK.
		ECKey newECKey = new ECKey(new SecureRandom());
		byte[] newPubkey = newECKey.getPubKeyPoint().getEncoded(true);
		sb.append("\"").append(Codecs.BASE64.encode(newPubkey)).append("\"");

		// encrypt ECK with the arbitrary public key above and sharing public keys.
		for (int i = 0; i < shrECPointList.size(); i++) {
			byte[] eeck;
			try {
				eeck = encryptECK(shrECPointList.get(i), newECKey.getPrivKey());
			} catch (GeneralSecurityException e) {
				throw new SecurityException("Fail to encrypt ECK: " + e.getMessage());
			}
			sb.append(",\"").append(Codecs.BASE64.encode(eeck)).append("\"");
		}

		return sb.toString();
	}

	public String marshal() throws CoinStackException {
		StringBuffer sb = new StringBuffer();
		sb.append("{\"type\":").append(JSONObject.quote(getType()));
		sb.append(",\"version\":").append(CONTRACT_BODY_VERSION);

		if (code.getECK() == null || code.getECK().length() == 0) {
			// enc = 0 : non-encrypted
			sb.append(",\"enc\":0");
		} else {
			if (code.isWholeEnc()) {
				// enc = 10 : whole code is encrypted
				sb.append(",\"enc\":10");
			} else {
				// enc = 20 : only arguments are encrypted
				sb.append(",\"enc\":20");
			}
			sb.append(",\"eecks\":[").append(makeEECKsJsonStr()).append("]");
		}

		String body = Codecs.BASE64.encode(code.toBytes());
		sb.append(",\"body\":\"").append(body).append("\"}");

		return sb.toString();
	}
}