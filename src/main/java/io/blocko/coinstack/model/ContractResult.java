package io.blocko.coinstack.model;

import org.json.JSONObject;

public class ContractResult {
	
	private JSONObject json = null;
	
	public ContractResult(JSONObject result) {
		this.json = result;
	}
	
	public boolean getSuccess() {
		return json.optBoolean("success", false);
	}
	public boolean hasError() {
		return json.has("error");
	}
	public boolean hasResult() {
		return json.has("result");
	}
	
	public JSONObject asJson() {
		return json;
	}
}
