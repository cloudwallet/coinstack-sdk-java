package io.blocko.coinstack.hsm;
import io.blocko.coinstack.exception.HsmKeystoreException;

import org.bouncycastle.util.encoders.Hex;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;

import sun.security.pkcs11.SunPKCS11;
import sun.security.pkcs11.wrapper.PKCS11Exception;
import sun.security.x509.AlgorithmId;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;

/**
 * The HsmStorage class provides tools for easily using EC keys on HSM with SUNPKCS11.
 * 
 * @author pierrealain
 *
 */
public class HsmStorage {
	
	private KeyStore keystore;
	private Provider provider;
	
	public HsmStorage(String stream, String ksPin) throws HsmKeystoreException {
		Provider p = new SunPKCS11(stream);

		if (p.getService("KeyStore", "PKCS11") == null) {
			throw new HsmKeystoreException("No PKCS#11 Service available: Probably Security Token (Smartcard) not inserted");
		}
	    
		Security.addProvider(p);

		// Get keystore object 
		KeyStore ks;
		// Multi-catch parameters are not allowed for source level below 1.7
		try {
			ks = KeyStore.getInstance("PKCS11", p);
			ks.load(null, ksPin.toCharArray());
		} catch (KeyStoreException e) {
			throw new HsmKeystoreException("Keystore error: Unable to get PKCS11 instance with provider");
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("Keystore error: Unable to load keystore");
		} catch (CertificateException e) {
			throw new HsmKeystoreException("Keystore error: Unable to load keystore");
		} catch (IOException e) {
			throw new HsmKeystoreException("Keystore error: Unable to load keystore");
		}
		this.keystore = ks;
		this.provider = p;
	}
	
	/**
	 * This method generates an EC keypair on the HSM.
	 * @param keyName This is the name to give to the new key
	 * @param newKeyPin This is the pin for the new keypair.
	 * @throws HsmKeystoreException 
	 */
	
	public void genECKeyPair(String keyName, String newKeyPin) throws HsmKeystoreException {
		
		// Generate Keypair
		KeyPairGenerator kpg;
		KeyPair kp;
		try {
			kpg = KeyPairGenerator.getInstance("EC", provider);
			kpg.initialize(new ECGenParameterSpec("secp256k1"));
			kp = kpg.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate EC keypair");
		} catch (InvalidAlgorithmParameterException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate EC keypair with secp256k1");
		}
	    
		// Save the keypair
		X509Certificate[] chain = new X509Certificate[1] ;
		try {
			chain[0] = getSelfSignedCertificate(kp);
		} catch (InvalidKeyException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate certificate");
		} catch (CertificateException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate certificate");
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate certificate");
		} catch (NoSuchProviderException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate certificate");
		} catch (SignatureException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate certificate");
		} catch (IOException e) {
			throw new HsmKeystoreException("EC Key generator error: Unable to generate certificate");
		}
	    
	    
		try {
			keystore.setKeyEntry(keyName, kp.getPrivate(), newKeyPin.toCharArray(), chain);
			keystore.store(null); //this no longer gives me the exception.
		} catch (KeyStoreException e) {
			throw new HsmKeystoreException("Keystore error: Unable to get key");
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("Keystore error: Unable to store key");
		} catch (CertificateException e) {
			throw new HsmKeystoreException("Keystore error: Unable to store key");
		} catch (IOException e) {
			throw new HsmKeystoreException("Keystore error: Unable to store key");
		}
	}
	
	/**
	 * Fetch the private and public keys of a given keypair name
	 * @param keyName Name of the key to fetch
	 * @param keyPin Pin of the key.
	 * @return An []String of length 2 containing the private and public key.
	 * @throws HsmKeystoreException 
	 
	 */
	public String[] fetchKeyPair(String keyName, String keyPin ) throws HsmKeystoreException {

		// Load keypair object
		Key keyObject = null;
		try {
			keyObject = keystore.getKey(keyName, keyPin.toCharArray());
		} catch (UnrecoverableKeyException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key");
		} catch (KeyStoreException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key");
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key");
		}

		String privKey = "";
		if (keyObject.getEncoded() != null) {
			privKey = Hex.toHexString(keyObject.getEncoded());
			privKey = privKey.substring(privKey.length()-64);
		}

		PublicKey pubKeyObject;
		try {
			pubKeyObject = keystore.getCertificate(keyName).getPublicKey();
		} catch (KeyStoreException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key certificate");
		} 

		String pubKey = Hex.toHexString(pubKeyObject.getEncoded());
		pubKey = pubKey.substring(pubKey.length()-130);
		
		return new String[] {privKey , pubKey};

	}
	/**
	 * Fetch the public key when the private key it non extractable.
	 * @param keyName Name of the key to fetch
	 * @return pubKey string The public key in hex string format with no encoding
	 * @throws HsmKeystoreException
	 */

	public String fetchPubKey(String keyName) throws HsmKeystoreException {

		PublicKey pubKeyObject;
		try {
			pubKeyObject = keystore.getCertificate(keyName).getPublicKey();
		} catch (KeyStoreException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key certificate");
		} 

		String pubKey = Hex.toHexString(pubKeyObject.getEncoded());
		pubKey = pubKey.substring(pubKey.length()-130);
		
		return pubKey; 
	}
	
	/**
	 * Creates a self signed certificate to store the public key 
	 * @param keyPair
	 * @return the certificate containing the pubkey.
	 * @throws InvalidKeyException
	 * @throws CertificateException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws SignatureException
	 * @throws IOException
	 */
	private X509Certificate getSelfSignedCertificate(KeyPair keyPair) throws InvalidKeyException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException {
		PrivateKey privateKey = keyPair.getPrivate();

		X509CertInfo info = new X509CertInfo();
		
		Date from = new Date();
		Date to = new Date();
		
		CertificateValidity interval = new CertificateValidity(from, to);
		BigInteger serialNumber = new BigInteger(64, new SecureRandom());
		X500Name owner = new X500Name("cn=Unknown");
		AlgorithmId sigAlgId = new AlgorithmId(AlgorithmId.sha256WithECDSA_oid);
		
		info.set(X509CertInfo.VALIDITY, interval);
		info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(serialNumber));
		info.set(X509CertInfo.SUBJECT, owner);
		info.set(X509CertInfo.ISSUER, owner);
		info.set(X509CertInfo.KEY, new CertificateX509Key(keyPair.getPublic()));
		info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
		info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(sigAlgId));
		
		// Sign the certificate to identify the algorithm that's used.
		X509CertImpl certificate = new X509CertImpl(info);
		certificate.sign(privateKey, "SHA256withECDSA");
		
		// Update the algorithm, and resign.
		sigAlgId = (AlgorithmId) certificate.get(X509CertImpl.SIG_ALG);
		info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, sigAlgId);
		certificate = new X509CertImpl(info);
		certificate.sign(privateKey, "SHA256withECDSA");
		
		return certificate;
	}
	
	/**
	 * Signs the raw transaction data 
	 * @param keyName The key used to sign.
	 * @param keyPin Pin of the key
	 * @param data Date to be signed
	 * @return signature bytes
	 * @throws HsmKeystoreException
	 */
	public byte[] signData(String keyName, String keyPin, byte[] data) throws HsmKeystoreException {

		// Load keypair object
		Key keyObject = null;
		try {
			keyObject = keystore.getKey(keyName, keyPin.toCharArray());
		} catch (UnrecoverableKeyException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key");
		} catch (KeyStoreException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key");
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("Keystore error: Unable to fetch requested key");
		}
		
		// Sign data with the key.
		Signature signature;
		byte[] signatureValue;
		try {
			signature = Signature.getInstance("SHA256withECDSA");
			signature.initSign((PrivateKey) keyObject);
			// the data itself, not a hash has to be given to the algorithm, ie tx bytecode.
			signature.update(data);
			signatureValue = signature.sign();
		} catch (NoSuchAlgorithmException e) {
			throw new HsmKeystoreException("Keystore algorithm error: Signing algorithm not available");
		} catch (InvalidKeyException e) {
			throw new HsmKeystoreException("Keystore error: Unable to sign with key");
		} catch (SignatureException e) {
			throw new HsmKeystoreException("Keystore error: Unable to make a signature of data");
		}

		return signatureValue.clone();
		
	}
}
