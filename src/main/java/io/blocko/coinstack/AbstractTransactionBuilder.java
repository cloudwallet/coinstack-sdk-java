package io.blocko.coinstack;

import static org.bitcoinj.core.Utils.doubleDigest;
import static org.bitcoinj.core.Utils.reverseBytes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.TransactionOutput;
import org.bitcoinj.core.Wallet.DustySendRequested;
import org.bitcoinj.core.Wallet.SendRequest;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptOpCodes;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.InsufficientFundException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.model.DataTransactionOutput;
import io.blocko.coinstack.model.DustyOutput;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.tx.BitcoinjWallet;
import io.blocko.coinstack.tx.Constant;
import io.blocko.coinstack.tx.ScriptBuilder;
import io.blocko.coinstack.util.BitcoinjUtil;
import io.blocko.coinstack.util.Codecs;

public abstract class AbstractTransactionBuilder {

	protected int maxTxSize = Constant.DEFAULT_MAX_TX_SIZE;
	protected int maxMetaSize =  Constant.DEFAULT_MAX_META_SIZE;
	
	protected long fee = Constant.DEFAULT_FEE;
	protected int utxoThreshold = Constant.DEFAULT_UTXO_THRESHOLD;
	
	protected boolean shuffleOutputs = true;
	
	protected Output[] unspents;
	protected List<Output> outputs;
	protected List<String> dataAddrs;
	protected List<String> postDataAddrs;
	protected byte[] data;
	protected boolean allowDustyOutput = false;
	protected boolean allowLargePayload = false;
	
	/**
	 * A helper class for creating a transaction template to be signed
	 */
	public AbstractTransactionBuilder() {
		this.unspents = null;
		this.outputs = new ArrayList<Output>();
		this.dataAddrs = new ArrayList<String>();
		this.postDataAddrs = new ArrayList<String>();
		this.data = null;
	}
	
	/**
	 * Reset saved data
	 */
	public void reset() {
		this.unspents = null;
		this.outputs.clear();
		this.dataAddrs.clear();
		this.postDataAddrs.clear();
		this.data = null;
	}

	//===============================================================
	// Abstract functions
	//===============================================================

	public abstract String buildTransaction(CoinStackClient client, String privateKeyWIF) throws IOException, CoinStackException;


	//===============================================================
	// Public functions; visible to all transaction builders
	//===============================================================
	
	public void setUnspents(Output[] unspents) {
		this.unspents = unspents;
	}
	
	public Output[] getUnspents() {
		return unspents;
	}

	public void setUtxoThreshold(int utxoThreshold) {
		this.utxoThreshold = utxoThreshold;
	}

	public int getUtxoThreshold() {
		return utxoThreshold;
	}
	
	/**
	 * return allowLargePayload boolean
	 */
	public boolean allowLargePayload() {
		return allowLargePayload;
	}

	/**
	 * Maximum size of data payload is limited to 80 bytes. If allowLargePayload
	 * is true, larger data is accepted by client.
	 * 
	 * @param allowLargePayload
	 *            boolean option to set allowLargePayload.
	 */
	public void allowLargePayload(boolean allowLargePayload) {
		this.allowLargePayload = allowLargePayload;
		if (this.allowLargePayload) {
			this.maxTxSize = this.maxTxSize < Constant.EXTENDED_MAX_TX_SIZE ?
					Constant.EXTENDED_MAX_TX_SIZE : this.maxTxSize;
			this.maxMetaSize = this.maxMetaSize < Constant.EXTENDED_MAX_META_SIZE ?
					Constant.EXTENDED_MAX_META_SIZE : this.maxMetaSize;
		}
	}
	
	/**
	 * Set maximum size of transaction.
	 * 
	 * @param maxTxSize
	 * 			size of bytes to set
	 * 
	 * @see Constant.DEFAULT_MAX_TX_SIZE
	 * @see ExceededMaxTransactionSize
	 */
	public void setMaxTxSize(int maxTxSize) {
		this.maxTxSize = maxTxSize;
	}
	
	/**
	 * Set maximum size of data payload.
	 * 
	 * @param maxMetaSize
	 * 			size of bytes to set
	 * 
	 * @see Constant.DEFAULT_MAX_META_SIZE
	 * @see #setData(byte[])
	 */
	public void setMaxMetaSize(int maxMetaSize) {
		this.maxMetaSize = maxMetaSize;
	}

	/**
	 * Set transfer fee for transaction
	 * 
	 * @param fee
	 *            amount of fee to pay to miners (minimum 0.0001 BTC = 10000
	 *            satoshi)
	 * @throws MalformedInputException
	 */
	public void setFee(long amount) {
		/*if (fee < 10000) {
		throw new MalformedInputException("Invalid fee", "Fee amount below dust threshold");
		}*/
		this.fee = amount;
	}

	public long getFee() {
		return fee;
	}
	
	
	//===============================================================
	// Protected functions; visible for internal usage
	//===============================================================

	protected void addOutput(String destinationAddress, long amount) {
		outputs.add(new Output("", 0, destinationAddress, false, amount, null));
	}
	
	protected void addOutput(Output output) {
		outputs.add(output);
	}
	
	protected Output[] getOutputs() {
		return outputs.toArray(new Output[outputs.size()]);
	}

	protected void addDataAddr(String dataAddress) {
		dataAddrs.add(dataAddress);
	}
	
	protected void addPostDataAddr(String dataAddress) {
		postDataAddrs.add(dataAddress);
	}
	
	protected String[] getDataAddrs() {
		return dataAddrs.toArray(new String[dataAddrs.size()]);
	}
	
	protected String[] getPostDataAddrs() {
		return postDataAddrs.toArray(new String[postDataAddrs.size()]);
	}
	
	protected void setData(byte[] data) throws MalformedInputException {
		if (data != null && data.length > maxMetaSize) {
			throw new MalformedInputException("Invalid data: payload length over " + maxMetaSize + " bytes");
		}

		if (null != this.data) {
			throw new MalformedInputException("Invalid data: multiple data payload not allowed");
		}

		this.data = data;
	}
	
	protected byte[] getData() {
		return data;
	}
	
	protected void allowDustyOutput(boolean allowDustyOutput) {
		this.allowDustyOutput = allowDustyOutput;
	}
	
	protected boolean allowsDustyOutput() {
		return allowDustyOutput;
	}
	
	protected boolean shuffleOutputs() {
		return shuffleOutputs;
	}
	
	protected void shuffleOutputs(boolean shuffleOutputs) {
		this.shuffleOutputs = shuffleOutputs;
	}
	
	/**
	 * This method is extracted from
	 * {@link #buildTransaction(CoinStackClient, String)} to return transaction
	 * instance also.
	 */
	protected BuildResult preBuild(CoinStackClient client, String privateKeyWIF)
			throws IOException, CoinStackException {
		long totalValue = 0L;

		// check sanity test for parameters
		org.bitcoinj.core.Transaction txTemplate = new org.bitcoinj.core.Transaction(this.getNetwork(client));
		for (Output output : this.getOutputs()) {
			totalValue += addOutputToTemplate(client, txTemplate, output);
		}

		// add data address
		for (String dataAddress : dataAddrs) {
			try {
				Script script = buildDataAddress(this.getNetwork(client), dataAddress);
				TransactionOutput output = new DataTransactionOutput(this.getNetwork(client), txTemplate, Coin.ZERO,
						script.getProgram());
				txTemplate.addOutput(output);
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid input: Parsing data address failed");
			}
		}

		// add OP_RETURN
		if (null != this.getData()) {
			Script script = new ScriptBuilder().op(ScriptOpCodes.OP_RETURN).data(this.getData()).build();
			TransactionOutput output = new DataTransactionOutput(this.getNetwork(client), txTemplate, Coin.ZERO,
					script.getProgram());
			txTemplate.addOutput(output);
		}
		
		// add post data address
		for (String postDataAddress : postDataAddrs) {
			try {
				Script script = buildDataAddress(this.getNetwork(client), postDataAddress);
				TransactionOutput output = new DataTransactionOutput(this.getNetwork(client), txTemplate, Coin.ZERO,
						script.getProgram());
				txTemplate.addOutput(output);
			} catch (AddressFormatException e) {
				throw new MalformedInputException("Invalid input: Parsing post data address failed");
			}
		}

		// derive address from private key
		final ECKey signingKey;
		try {
			signingKey = new DumpedPrivateKey(this.getNetwork(client), privateKeyWIF).getKey();
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid input: Parsing private key failed");
		}
		Address fromAddress = signingKey.toAddress(this.getNetwork(client));
		String from = fromAddress.toString();
		
		// tx fee control for large payload
		if (this.getData() != null && this.fee > 0) {
			int size = this.getData().length;
			if (this.fee/2 < size) {
				this.fee += size;
			}
		}

		// get explicitly assigned unspentout
		Output[] unspents = this.getUnspents();

		// if not assigned or empty
		if (unspents == null || unspents.length == 0) {
			// get unspentout from address
			unspents = (client.hasUtxoCache()) ? client.getUtxoCache().get(client, from, totalValue, fee)
					: client.getUnspentOutputs(from, totalValue + fee);

			if (unspents.length == 0) {
				throw new InsufficientFundException("Insufficient fund");
			}

			// In the case of null data tx, contains data address, there is no
			// outputs.
			// So, this manually selects input and sets output
			if (totalValue + fee == 0) {
				unspents = new Output[] { unspents[0] };
				addOutputToTemplate(client, txTemplate, unspents[0]);
			} else if (utxoThreshold > 0 && unspents.length > utxoThreshold) {
				// select only required amount of utxo
				List<Output> selected = selectUtxo(totalValue + fee, unspents);
				unspents = selected.toArray(new Output[selected.size()]);
			}
		}
		
		// tx fee control for dusty change output
		// - calculate changeValue
		long changeValue = 0 - (totalValue + fee);
		for (Output one : unspents) {
			changeValue += one.getValue();
		}
		// - remove dusty change output
		if (fee/2 > changeValue) {
			fee += changeValue;
		}
		
		BitcoinjWallet cachedWallet = null;
		if (client.hasUtxoCache()) {
			cachedWallet = client.getUtxoCache().getWallet(from);
		}
		if (cachedWallet == null) {
			cachedWallet = new BitcoinjWallet(this.getNetwork(client));
			//cachedWallet.allowLargePayload(this.allowLargePayload());
			cachedWallet.setMaxTxSize(this.maxTxSize);
			cachedWallet.allowSpendingUnconfirmedTransactions();
			cachedWallet.importKey(signingKey);
			if (client.hasUtxoCache()) {
				client.getUtxoCache().putWallet(from, cachedWallet);
			}
		}
		BitcoinjUtil.injectOutputs(cachedWallet, unspents, client.isMainNet());

		SendRequest request = SendRequest.forTx(txTemplate);
		request.changeAddress = fromAddress;
		request.fee = Coin.valueOf(fee);
		request.feePerKb = Coin.ZERO;
		request.shuffleOutputs = this.shuffleOutputs;

		org.bitcoinj.core.Transaction tx;
		try {
			// tx = tempWallet.sendCoinsOffline(request);

			cachedWallet.completeTx(request);
			cachedWallet.clearTransactions(0);
			tx = request.tx;

		} catch (InsufficientMoneyException e) {
			throw new InsufficientFundException("Insufficient fund");
		} catch (DustySendRequested e) {
			throw new MalformedInputException("Invalid output: Send amount below dust threshold");
		}
		byte[] rawTx = tx.bitcoinSerialize();

		// convert to string encoded hex
		String rawTxStr = Codecs.HEX.encode(rawTx);

		if (client.hasUtxoCache())
			// add and propose to cache
			client.getUtxoCache().propose(from, tx.getHashAsString(), rawTxStr, tx.getInputs(), tx.getOutputs());

		return new BuildResult("", rawTxStr, rawTx);
	}

	protected BuildResult build(CoinStackClient client, String privateKeyWIF) throws IOException, CoinStackException {
		BuildResult result = preBuild(client, privateKeyWIF);

		// calculate txid hash
		String txidHash = new Sha256Hash(reverseBytes(doubleDigest(result.getRawTx()))).toString();
		result.setTxid(txidHash);

		return result;
	}

	protected class BuildResult {
		String txid;
		String rawTxStr;
		byte[] rawTx;
		
		
		protected BuildResult(String txid, String rawTxStr, byte[] rawTx) {
			this.txid = txid;
			this.rawTxStr = rawTxStr;
			this.rawTx = rawTx;
		}

		protected void setTxid(String txid) {
			this.txid = txid;
		}
		
		public String getTxid() {
			return txid;
		}

		public String getRawTxStr() {
			return rawTxStr;
		}
		
		public byte[] getRawTx() {
			return rawTx;
		}
	}
	
	//===============================================================
	// Private functions
	//===============================================================

	private NetworkParameters getNetwork(CoinStackClient client) {
		return client.isMainNet() ? MainNetParams.get() : RegTestParams.get();
	}

	private org.bitcoinj.script.Script buildDataAddress(NetworkParameters network, String address)
			throws AddressFormatException, MalformedInputException {
		StringBuilder hexBuffer = new StringBuilder();
		hexBuffer.append("4f5a");
		hexBuffer.append("0100");
		byte[] prefixData = null;
		
		try {
			prefixData = Hex.decodeHex(hexBuffer.toString().toCharArray());
		} catch (org.apache.commons.codec.DecoderException e1) {
			throw new MalformedInputException("Fail to decode cmd header: " + e1.getMessage());
		}

		Address addr = new Address(MainNetParams.get(), address);
		byte[] addressData = addr.getHash160();

		byte addressBytes = (byte) network.getAddressHeader();

		// convert to byte; header + permission byte + address
		byte[] markerData = ByteBuffer.allocate(prefixData.length + 1 + addressData.length).put(prefixData)
				.put(addressBytes).put(addressData).array();

		return new ScriptBuilder().op(ScriptOpCodes.OP_RETURN).data(markerData).build();
    }

	private long addOutputToTemplate(CoinStackClient client, org.bitcoinj.core.Transaction txTemplate, Output output)
			throws MalformedInputException {

		Address destinationAddressParsed;
		try {
			destinationAddressParsed = new Address(this.getNetwork(client), output.getAddress());
		} catch (AddressFormatException e) {
			throw new MalformedInputException("Invalid output: Malformed address");
		}

		if (this.allowsDustyOutput()) {
			txTemplate.addOutput(new DustyOutput(this.getNetwork(client), txTemplate, Coin.valueOf(output.getValue()),
					destinationAddressParsed));
		} else {
			txTemplate.addOutput(Coin.valueOf(output.getValue()), destinationAddressParsed);
		}
		return output.getValue();
	}

	private static List<Output> selectUtxo(long requiredAmount, Output[] utxos) {
		List<Output> selected = new ArrayList<Output>();
		for (Output utxo : utxos) {
			selected.add(utxo);
			requiredAmount -= utxo.getValue();
			if (requiredAmount <= 0) {
				break;
			}
		}
		return selected;
	}
}