package io.blocko.coinstack.exception;

public class SecurityException extends CoinStackException {
	private static final long serialVersionUID = -4747893054496821415L;

	public SecurityException(String message) {
		super("io.coinstack#Security", 3000, 400, message, false);
	}
	
	@Deprecated
	public SecurityException(String message, String detail) {
		super("io.coinstack#Security", 3000, 400, message, false, detail);
	}
}
