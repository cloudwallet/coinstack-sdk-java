package io.blocko.coinstack.exception;

public class HsmKeystoreException extends CoinStackException {
	private static final long serialVersionUID = -1034038849776371413L;

	public HsmKeystoreException(String message) {
		super("io.coinstack#HsmKeystore", 3000, 400, message, false);
	}
	
	@Deprecated
	public HsmKeystoreException(String message, String detailedMessage) {
		super("io.coinstack#HsmKeystore", 3000, 400, message, false, detailedMessage);
	}
}
