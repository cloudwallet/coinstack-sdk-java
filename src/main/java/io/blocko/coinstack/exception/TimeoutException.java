package io.blocko.coinstack.exception;

public class TimeoutException extends CoinStackException {
	private static final long serialVersionUID = -2522287475243721135L;
	
	public TimeoutException(String message) {
		super("io.coinstack#Timeout", 3000, 408, message, false);
	}
}
