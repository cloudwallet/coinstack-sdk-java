package io.blocko.coinstack;

import java.security.PublicKey;

public class BasicEndpoint implements AbstractEndpoint {
	
	String endpoint;
	boolean mainnet;
	PublicKey pubkey;
	
	
	public BasicEndpoint(String endpointUrl, boolean isMainNet, PublicKey publicKey) {
		this.endpoint = endpointUrl;
		this.mainnet = isMainNet;
		this.pubkey = publicKey;
	}
	public BasicEndpoint(String endpointUrl, boolean isMainNet) {
		this(endpointUrl, isMainNet, null);
	}
	public BasicEndpoint(String endpointUrl) {
		this(endpointUrl, true, null);
	}
	
	
	@Override
	public String endpoint() {
		return endpoint;
	}
	
	@Override
	public boolean mainnet() {
		return mainnet;
	}
	
	@Override
	public PublicKey getPublicKey() {
		return pubkey;
	}
}
