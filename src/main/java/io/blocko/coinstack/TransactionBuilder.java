package io.blocko.coinstack;

import java.io.IOException;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.model.Output;

/**
 * @author shepelt
 *
 */
public class TransactionBuilder extends AbstractTransactionBuilder {
	
	/**
	 * A helper class for creating a transaction template to be signed
	 */
	public TransactionBuilder() {
		super();
	}
	
	@Override
	public String buildTransaction(CoinStackClient client, String privateKeyWIF)
			throws IOException, CoinStackException {
		BuildResult result = preBuild(client, privateKeyWIF);
		
		return result.getRawTxStr();
	}

	/**
	 * Add a standard output to transaction
	 * 
	 * @param destinationAddress
	 *            address to send fund
	 * @param amount
	 *            amount of fund to transfer
	 */
	public void addOutput(String destinationAddress, long amount) {
		super.addOutput(destinationAddress, amount);
	}
	
	public void addOutput(Output output) {
		super.addOutput(output);
	}

	public Output[] getOutputs() {
		return super.getOutputs();
	}

	/**
	 * This function indicates a target address of a data, given by setData(byte[] data)
	 * 
	 * @param dataAddress
	 *            target address of data
	 */
	public void addDataAddr(String dataAddress) {
		super.addDataAddr(dataAddress);
	}
	
	protected String[] getDataAddrs() {
		return super.getDataAddrs();
	}

	/**
	 * Set data transfered using OP_RETURN for transaction. Maximum size of data
	 * payload is limited to 80 bytes. If {@link #allowLargePayload()} is true,
	 * larger data is accepted by client.
	 * 
	 * @param data
	 *            data payload to be attached to transaction
	 * @throws MalformedInputException
	 * 
	 */
	public void setData(byte[] data) throws MalformedInputException {
		super.setData(data);
	}
	
	public byte[] getData() {
		return super.getData();
	}
	
	/**
	 * If output valus is less than 546 satoshis, it is dusty output If
	 * allowDustyOutput is true, this small value is accepted by client
	 */
	public void allowDustyOutput(boolean allowDustyOutput) {
		super.allowDustyOutput(allowDustyOutput);
	}
	
	public boolean allowsDustyOutput() {
		return allowDustyOutput;
	}
	
	/**
	 * return shuffleOutputs boolean
	 */
	public boolean shuffleOutputs() {
		return super.shuffleOutputs();
	}
	
	/**
	 * Set option whether to shuffle order of unspent outputs, for generating a
	 * transaction, or not In true (is default), the unspent outputs will be
	 * shuffled and used randomly. In other case, those will be used in a static
	 * way according to a response of a unspent output query
	 * 
	 * @param shuffleOutputs
	 *            true is enabling shuffle, false is disable (default = true)
	 * 
	 */
	public void shuffleOutputs(boolean shuffleOutputs) {
		super.shuffleOutputs(shuffleOutputs);
	}
	
}
