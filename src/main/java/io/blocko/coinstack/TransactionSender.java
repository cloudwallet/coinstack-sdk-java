package io.blocko.coinstack;

import java.io.IOException;

import io.blocko.coinstack.AbstractTransactionBuilder.BuildResult;
import io.blocko.coinstack.contract.LuaContractCode;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;

/**
 * This class provides a set of utility functions to easily create, build, and
 * send a transaction
 * 
 * @author BeomjunJeon
 * @since 3.1
 */
public class TransactionSender {

	private CoinStackClient coinstack;
	private String privateKeyWIF;
	private AbstractTransactionBuildTip txTip;

	public TransactionSender(CoinStackClient coinstack, String privateKeyWIF, AbstractTransactionBuildTip txTip) {
		this.coinstack = coinstack;
		this.privateKeyWIF = privateKeyWIF;
		this.txTip = txTip;
	}

	/**
	 * A simple utility function to send coin to someone. If you want to handle
	 * detail behavior, add an output using
	 * {@link TransactionBuilder#addOutput(String, long)}, tune detail
	 * parameters, build transaction using
	 * {@link TransactionBuilder#buildTransaction(CoinStackClient, String)}, and
	 * send its result (signed raw transaction) using
	 * {@link CoinStackClient#sendTransaction(String)}
	 * 
	 * @param receiverAddress
	 *            a target bitcoin address to send coin
	 * @param amount
	 *            an amount of coin to send in satoshi
	 * @return a transaction id
	 * @throws IOException
	 *             network failure
	 * @throws CoinStackException
	 *             something wrong with a transaction
	 */
	public String sendCoin(String receiverAddress, long amount) throws IOException, CoinStackException {
		TransactionBuilder txbuilder = new TransactionBuilder();
		txbuilder.addOutput(receiverAddress, amount);
		txbuilder.setFee(txTip.defaultFee());
		txbuilder.setUtxoThreshold(txTip.utxoThreashold());

		BuildResult result = txbuilder.build(coinstack, privateKeyWIF);
		coinstack.sendTransaction(result.getRawTxStr());

		return result.getTxid();
	}

	/**
	 * A simple utility function to record bytes to coinstack. If you want to
	 * handle detail behavior, set data using
	 * {@link TransactionBuilder#setData(byte[])}, tune detail parameters, build
	 * transaction using
	 * {@link TransactionBuilder#buildTransaction(CoinStackClient, String)}, and
	 * send its result (signed raw transaction) using
	 * {@link CoinStackClient#sendTransaction(String)}
	 * 
	 * @param data
	 *            serialized data, that you want to record
	 * @return a transaction id
	 * @throws IOException
	 *             network failure
	 * @throws CoinStackException
	 *             something wrong with a transaction
	 */
	public String writeData(byte[] data) throws MalformedInputException, IOException, CoinStackException {
		TransactionBuilder txbuilder = new TransactionBuilder();
		txbuilder.shuffleOutputs(false);
		txbuilder.allowLargePayload(true);
		txbuilder.setUtxoThreshold(txTip.utxoThreashold());

		txbuilder.setData(data);

		int dataKB = (data.length / 1000) + 1;

		txbuilder.setFee(txTip.defaultFee() + dataKB * txTip.feePerKb());

		BuildResult result = txbuilder.build(coinstack, privateKeyWIF);
		coinstack.sendTransaction(result.getRawTxStr());

		return result.getTxid();
	}

	/**
	 * A simple utility function to send data to another. If you want to handle
	 * detail behavior, set data using
	 * {@link TransactionBuilder#setData(byte[])}, add data receiver using
	 * {@link TransactionBuilder#addOutput(String, long)}, tune detail
	 * parameters, build transaction using
	 * {@link TransactionBuilder#buildTransaction(CoinStackClient, String)}, and
	 * send its result (signed raw transaction) using
	 * {@link CoinStackClient#sendTransaction(String)}
	 * 
	 * @param dataReceiverAddress
	 *            a target bitcoin address, that you want to let him know data
	 *            sending
	 * @param data
	 *            serialized data, that you want to record
	 * @return a transaction id
	 * @throws IOException
	 *             network failure
	 * @throws CoinStackException
	 *             something wrong with a transaction
	 */
	public String sendData(String dataReceiverAddress, byte[] data)
			throws MalformedInputException, IOException, CoinStackException {
		TransactionBuilder txbuilder = new TransactionBuilder();
		txbuilder.shuffleOutputs(false);
		txbuilder.allowLargePayload(true);
		txbuilder.setUtxoThreshold(txTip.utxoThreashold());

		txbuilder.setData(data);

		if (coinstack.isUseAddrData()) {
			txbuilder.addDataAddr(dataReceiverAddress);
		} else {
			txbuilder.allowDustyOutput(true);
			txbuilder.addOutput(dataReceiverAddress, 600);
		}

		// calculate fee according to the size of the data
		int dataKB = (data.length / 1000) + 1;
		txbuilder.setFee(txTip.defaultFee() + dataKB * txTip.feePerKb());

		BuildResult result = txbuilder.build(coinstack, privateKeyWIF);

		// send a sigend tx to a coinstack server
		coinstack.sendTransaction(result.getRawTxStr());

		return result.getTxid();
	}

	/**
	 * A simple utility function to define a smart contract
	 * 
	 * @param contractId
	 *            a target smart contract id to define
	 * @param code
	 *            a lua smart contract instance
	 * @return a transaction id
	 * @throws MalformedInputException
	 *             the given lua contract code is null
	 * @throws IOException
	 *             network failure
	 * @throws CoinStackException
	 *             something wrong with a transaction
	 */
	public String defineContract(String contractId, LuaContractCode code)
			throws MalformedInputException, IOException, CoinStackException {
		LuaContractBuilder contBuilder = new LuaContractBuilder();

		contBuilder.setContractId(contractId);
		contBuilder.setUtxoThreshold(txTip.utxoThreashold());
		contBuilder.setDefinition(code);

		if (coinstack.isUseAddrData()) {
			contBuilder.setMajorVersion("02");
		} else {
			contBuilder.setMajorVersion("01");
		}

		byte[] payload = contBuilder.buildPayload();

		// calculate fee according to the size of the data
		int dataKB = (payload.length / 1000) + 1;
		contBuilder.setFee(txTip.defaultFee() + dataKB * txTip.feePerKb());

		BuildResult result = contBuilder.buildUsingPayload(coinstack, payload, privateKeyWIF);

		// send a sigend tx to a coinstack server
		coinstack.sendTransaction(result.getRawTxStr());

		return result.getTxid();
	}

	/**
	 * A simple utility function to execute a smart contract
	 * 
	 * @param contractId
	 *            a target smart contract id to define
	 * @param code
	 *            a lua smart contract instance
	 * @return a transaction id
	 * @throws MalformedInputException
	 *             the given lua contract code is null
	 * @throws IOException
	 *             network failure
	 * @throws CoinStackException
	 *             something wrong with a transaction
	 */
	public String executeContract(String contractId, LuaContractCode code)
			throws MalformedInputException, IOException, CoinStackException {
		LuaContractBuilder contBuilder = new LuaContractBuilder();

		contBuilder.setContractId(contractId);
		contBuilder.setUtxoThreshold(txTip.utxoThreashold());
		contBuilder.setExecution(code);

		if (coinstack.isUseAddrData()) {
			contBuilder.setMajorVersion("02");
		} else {
			contBuilder.setMajorVersion("01");
		}

		byte[] payload = contBuilder.buildPayload();

		// calculate fee according to the size of the data
		int dataKB = (payload.length / 1000) + 1;
		contBuilder.setFee(txTip.defaultFee() + dataKB * txTip.feePerKb());

		BuildResult result = contBuilder.buildUsingPayload(coinstack, payload, privateKeyWIF);

		// send a sigend tx to a coinstack server
		coinstack.sendTransaction(result.getRawTxStr());

		return result.getTxid();
	}

}
