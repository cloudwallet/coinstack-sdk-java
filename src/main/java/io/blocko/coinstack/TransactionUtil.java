package io.blocko.coinstack;

import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.core.ScriptException;
import org.bitcoinj.core.TransactionOutPoint;


import io.blocko.coinstack.model.Input;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.util.Codecs;


public class TransactionUtil {

	/**
	 * Construct a transaction object from raw transaction
	 * 
	 * @param rawTransaction
	 *            transaction in hex-encoded string format
	 * @return a transaction object representing the given raw transaction
	 */
	public static Transaction parseRawTransaction(String rawTransaction) {
		return TransactionUtil.parseRawTransaction(rawTransaction, true);
	}

	/**
	 * parse raw transaction and return transaction object
	 * 
	 * @param rawTransaction
	 *            transaction in hex-encoded string format
	 * @param isMainNet
	 *            MainNet is true, TestNet is false
	 * @return a transaction object representing the given raw transaction
	 */
	public static Transaction parseRawTransaction(String rawTransaction, boolean isMainNet) {
		org.bitcoinj.core.Transaction tx = new org.bitcoinj.core.Transaction(
				isMainNet ? MainNetParams.get() : RegTestParams.get(),
						Codecs.HEX.decode(rawTransaction));
		tx.getInputs();
		tx.getOutputs();
		Input[] inputs = new Input[tx.getInputs().size()];
		for (int i = 0; i < tx.getInputs().size(); i++) {
			TransactionOutPoint outpoint = tx.getInput(i).getOutpoint();
			inputs[i] = new Input((int) outpoint.getIndex(), null, outpoint.getHash().toString(), 0l);
		}

		Output[] outputs = new Output[tx.getOutputs().size()];
		for (int i = 0; i < tx.getOutputs().size(); i++) {
			try {
				outputs[i] = new Output(tx.getHashAsString(), i,
						tx.getOutput(i).getScriptPubKey()
								.getToAddress(isMainNet ? MainNetParams.get() : RegTestParams.get()).toString(),
						false, tx.getOutput(i).getValue().value, Codecs.HEX.encode(tx.getOutput(i).getScriptBytes()));
			} catch (ScriptException e) {
				outputs[i] = new Output(tx.getHashAsString(), i, "", false, tx.getOutput(i).getValue().value,
						Codecs.HEX.encode(tx.getOutput(i).getScriptBytes()));
			}
		}

		Transaction parsedTx = new Transaction(tx.getHashAsString(), new String[] {}, tx.getUpdateTime(), false, inputs,
				outputs);
		
		return parsedTx;
	}

	public static String getTransactionHash(String rawTransaction, boolean isMainNet) {
		return new org.bitcoinj.core.Transaction(isMainNet ? MainNetParams.get() : RegTestParams.get(),
				Codecs.HEX.decode(rawTransaction)).getHashAsString();
	}

	/**
	 * Calculate transaction hash from raw transaction
	 * 
	 * @param rawTransaction
	 *            transaction in hex-encoded string format
	 * @return the hash (transaction ID) of given raw transaction
	 */
	public static String getTransactionHash(String rawTransaction) {
		return getTransactionHash(rawTransaction, true);
	}

}
