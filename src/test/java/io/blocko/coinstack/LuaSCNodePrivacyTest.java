package io.blocko.coinstack;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.PublicKey;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import io.blocko.coinstack.contract.LuaContractCode;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.SecurityException;
import io.blocko.coinstack.model.ContractResult;
import io.blocko.coinstack.util.Codecs;

public class LuaSCNodePrivacyTest {
	public static final AbstractEndpoint TEST_ENDPOINT = new AbstractEndpoint() {
		@Override
		public String endpoint() {
			return "http://testchain.blocko.io";
			//return "http://localhost:3000";
		}

		@Override
		public boolean mainnet() {
			return true;
		}

		@Override
		public PublicKey getPublicKey() {
			return null;
		}
	};

	//@formatter:off
	private static String DEF_LUA = "local system = require('system')\n\n"
			+ "function setValue(key, value)\n"
			+ "    system.setItem('LuaSCNodePrivacy-' .. key, value)\n"
			+ "end\n\n"
			+ "function getValue(key)\n"
			+ "    return system.getItem('LuaSCNodePrivacy-' .. key)\n"
			+ "end\n";
	//@formatter:on

	private static CoinStackClient client = null;

	private static String privateKeyWIF = "L1mRDfYqCo3e4n28YpEKqDdSWZy7kWj91H4jjSH3qAz3zJx6XhHm";
	private static String contractID = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		client = new CoinStackClient(null, TEST_ENDPOINT);

		try {
			// prepare test
			String address = null;
			if (privateKeyWIF != null && !privateKeyWIF.isEmpty()) {
				address = ECKey.deriveAddress(privateKeyWIF);
			} else {
				throw new RuntimeException("need a private key to test.");
			}

			System.out.println("# Test Information of address: " + address);

			// check balance
			long balance = client.getBalance(address);
			if (balance == 0) {
				throw new RuntimeException("need a balance to test.");
			} else {
				System.out.println("  - balance: " + balance);
			}
			
			if (!checkNodeVersion()) {
				return;
			}
			
			// check node group public key
			byte[] nodegroupPubKey = client.getNodegroupPubkey();
			if (nodegroupPubKey == null) {
				throw new RuntimeException("need to set a node group key in the server.");
			} else {
				String base64Ngpk = Codecs.BASE64.encode(nodegroupPubKey);
				System.out.println("  - nodegroup public key: " + base64Ngpk);
			}

			// LUA Smart Contract
			contractID = address;
			System.out.println("  - contract ID: " + contractID);

			// define LUA Smart Contract
			LuaContractBuilder lcb = new LuaContractBuilder();
			lcb.setContractId(contractID);
			lcb.setDefinition(DEF_LUA);
			String rawTx = lcb.buildTransaction(client, privateKeyWIF);
			String txId = TransactionUtil.getTransactionHash(rawTx);
			System.out.println("  - tx ID: " + txId);
			client.sendTransaction(rawTx);
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			throw new RuntimeException(e.getDetailedMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Test
	public void testLuaSCwithWholeEnc() throws Exception {
		try {
			System.out.println("# Node Privacy with Whole Encryption");
			
			if (!checkNodeVersion()) {
				return;
			}
			
			// execute
			LuaContractCode lcc = new LuaContractCode("res, ok = call($func, $key, $value)");
			lcc.setParam("func", "setValue");
			lcc.setParam("key", "ABC");
			lcc.setParam("value", 123);

			try {
				lcc.setWholeEnc(true);
				// without ECK, error
				lcc.toBytes();
			} catch (SecurityException e) {
				System.out.println(e.getMessage());
				assertEquals("fail to encrypt parameters: need password to encrypt", e.getMessage());
			}

			LuaContractBuilder lcb = new LuaContractBuilder();
			lcb.setContractId(contractID);
			lcb.setExecution(lcc);
			lcb.setWholeEnc(true);
			lcb.setRandEck(true);
			lcb.addSharePubKey(client.getNodegroupPubkey());
			System.out.println("  - ECK: " + lcb.getEck());

			String rawTx = lcb.buildTransaction(client, privateKeyWIF);
			String txId = TransactionUtil.getTransactionHash(rawTx);
			System.out.println("  - tx ID: " + txId);
			client.sendTransaction(rawTx);

			// query
			lcc = new LuaContractCode("ret, ok = call(?, ?); return ret");
			lcc.setParam(1, "getValue");
			lcc.setParam(2, "ABC");
			lcc.setWholeEnc(true);
			lcc.setRandomEck();

			ArrayList<byte[]> sharePubKeyList = new ArrayList<byte[]>();
			sharePubKeyList.add(client.getNodegroupPubkey());
	
			ContractResult res = client.queryContract(contractID, lcc, sharePubKeyList);
			if (res != null) {
				System.out.println("  - res: " + res.asJson());
			}
		} catch (CoinStackException e) {
			System.err.printf("%s: %s\n", e.getMessage(), e.getDetailedMessage());
		}
	}

	@Test
	public void testLuaSCwithEncParam() throws Exception {
		try {
			System.out.println("# Node Privacy with Parameters Encryption");
			
			if (!checkNodeVersion()) {
				return;
			}
			
			// execute
			LuaContractCode lcc = new LuaContractCode("res, ok = call($func, $key, $value)");
			lcc.setParam("func", "setValue");
			lcc.setParam("key", "DEF", true);
			lcc.setParam("value", "456", true, true);

			LuaContractBuilder lcb = new LuaContractBuilder();
			lcb.setContractId(contractID);
			lcb.setExecution(lcc);
			lcb.setRandEck(true);
			lcb.addSharePubKey(client.getNodegroupPubkey());
			System.out.println("  - ECK: " + lcb.getEck());

			String rawTx = lcb.buildTransaction(client, privateKeyWIF);
			String txId = TransactionUtil.getTransactionHash(rawTx);
			System.out.println("  - tx ID: " + txId);
			client.sendTransaction(rawTx);

			// query
			lcc = new LuaContractCode("ret, ok = call(?, ?); return ret");
			lcc.setParam(1, "getValue", true);
			lcc.setParam(2, "DEF");
			lcc.setRandomEck();

			ArrayList<byte[]> sharePubKeyList = new ArrayList<byte[]>();
			sharePubKeyList.add(client.getNodegroupPubkey());
	
			ContractResult res = client.queryContract(contractID, lcc, sharePubKeyList);
			if (res != null) {
				System.out.println("  - res: " + res.asJson());
			}
		} catch (CoinStackException e) {
			System.err.printf("%s: %s\n", e.getMessage(), e.getDetailedMessage());
		}
	}
	
	public static boolean checkNodeVersion() throws CoinStackException, IOException {
		// test node version: check response of getNodegroupPubkey method
		try {
			client.getNodegroupPubkey();
		} catch (CoinStackException e) {
			assertEquals("InvalidResponseException", e.getClass().getSimpleName());
			System.out.println("  - skip test: not supported");
			return false;
		}
		return true;
	}
}