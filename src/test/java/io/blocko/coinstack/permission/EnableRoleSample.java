package io.blocko.coinstack.permission;

import java.io.IOException;
import java.security.PublicKey;

import io.blocko.coinstack.AbstractEndpoint;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.permission.model.Role;

public class EnableRoleSample {

	public static String PRIVKEY_099 = "KxHfm5TMumRqcEb7poD94EpMNWbFDjp5n9UCUMEv9SyEAn3tjhKv";

	public static final AbstractEndpoint TEST_ENDPOINT = new AbstractEndpoint() {
		public String endpoint() {
			return "http://127.0.0.1:3000"; 
		}

		public boolean mainnet() {
			return true;
		}

		public PublicKey getPublicKey() {
			return null;
		}
	};

	public static void main(String[] args) throws CoinStackException, IOException {

		// initialize value
		CoinStackClient client = new CoinStackClient(null, TEST_ENDPOINT);

		// check current enabled roles
		Role currentEnabledRole = client.getEnabledRole();
		
		System.out.printf("Currently Enabled Roles: %s\n", currentEnabledRole);
		
		// create role
		Role role = new Role(Role.MINER|Role.WRITER);

		EnableRoleBuilder enRoleBuilder = new EnableRoleBuilder();
		enRoleBuilder.setRole(role);

		System.out.printf("Change Enabled Role to '%s'\n", role);

		// build transaction
		String rawTx = enRoleBuilder.buildTransaction(client, PRIVKEY_099);

		try {
			// send transaction to a server
			client.sendTransaction(rawTx);
		} catch (CoinStackException e) {
			System.out.println(e.getDetailedMessage());
			e.printStackTrace();
		}
	}
}
