package io.blocko.coinstack.permission;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Map;
import java.util.Map.Entry;

import io.blocko.coinstack.AbstractEndpoint;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.permission.model.Role;

public class SetRoleSample {

	public static String PRIVKEY_099 = "KxHfm5TMumRqcEb7poD94EpMNWbFDjp5n9UCUMEv9SyEAn3tjhKv";
	public static String PUBKEY_099 = "Au2B4PRddXouHaBFynDTKAbsirgD6gPmZca69s3aZYOa";
	public static String ADDRESS_099 = "16M3sM9EcywAaMc4tTtRMgZSgqnKRxpdBH";

	public static String TARGET_ADDRESS = "114x2sFequ9gbcbgZjYVXK11qtaCiTABjs";
	public static String TARGET_PRIVKEY = "KwdcSiVpME6tuM4ZdByes2eo7BcJaqU1nrK4XWrViJNX1YSEL2BK";

	public static final AbstractEndpoint TEST_ENDPOINT = new AbstractEndpoint() {
		public String endpoint() {
			return "http://192.168.0.93:3000"; 
		}

		public boolean mainnet() {
			return true;
		}

		public PublicKey getPublicKey() {
			return null;
		}
	};

	public static void main(String[] args) throws CoinStackException, IOException {

		// initialize value
		CoinStackClient client = new CoinStackClient(null, TEST_ENDPOINT);

		// check existance of an admin role to a tx maker
		Role currentRole = client.getRole(ADDRESS_099);
		if (currentRole.isAdmin())
			System.out.printf("%s has Admin Role: %s\n", ADDRESS_099, currentRole);
		else
			System.out.printf("%s has no Admin Role!: %s\n", ADDRESS_099, currentRole);

		// create transaction
		Role role = new Role(Role.MINER|Role.NODE);

		SetRoleBuilder roleBuilder = new SetRoleBuilder();
		roleBuilder.setAddress(TARGET_ADDRESS);
		roleBuilder.setRole(role);

		System.out.printf("Set Role '%s' to %s\n", role, TARGET_ADDRESS);

		// build transaction
		String rawTx = roleBuilder.buildTransaction(client, PRIVKEY_099);

		try {
			// send transaction to a server
			client.sendTransaction(rawTx);
		} catch (CoinStackException e) {
			System.out.println(e.getDetailedMessage());
			e.printStackTrace();
		}

		// fetch all role set
		Map<String, Role> addressRoleMap = client.listAllRole();

		System.out.println("[List All Address: Role]");

		for (Entry<String, Role> roleEntry : addressRoleMap.entrySet()) {
			System.out.printf(" * %s: %s \n", roleEntry.getKey(), roleEntry.getValue());
		}
	}
}
