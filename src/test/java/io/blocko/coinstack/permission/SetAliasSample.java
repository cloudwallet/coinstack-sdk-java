package io.blocko.coinstack.permission;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Map;
import java.util.Map.Entry;

import io.blocko.coinstack.AbstractEndpoint;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.contract.SetPubkeyAliasBuilder;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.util.Codecs;

public class SetAliasSample {

	public static String PRIVKEY_099 = "KxHfm5TMumRqcEb7poD94EpMNWbFDjp5n9UCUMEv9SyEAn3tjhKv";
	public static String PUBKEY_099 = "Au2B4PRddXouHaBFynDTKAbsirgD6gPmZca69s3aZYOa";
	public static String ADDRESS_099 = "16M3sM9EcywAaMc4tTtRMgZSgqnKRxpdBH";

	public static final AbstractEndpoint TEST_ENDPOINT = new AbstractEndpoint() {
		public String endpoint() {
			return "http://127.0.0.1:3000"; 
		}

		public boolean mainnet() {
			return true;
		}

		public PublicKey getPublicKey() {
			return null;
		}
	};

	public static void main(String[] args) throws CoinStackException, IOException {

		// initialize value
		CoinStackClient client = new CoinStackClient(null, TEST_ENDPOINT);
		String alias1 = "alias_1";

		// fetch a value before a setting alias
		byte[] puckey1 = client.getNodegroupPubkey(alias1);
		if (puckey1 != null)
			System.out.printf("previous pubkey for %s = %s\n", alias1, Codecs.BASE64.encode(puckey1));
		else {
			System.out.println("pubkey is null");
		}

		// create a transaction to set a alias
		SetPubkeyAliasBuilder aliasBuilder = new SetPubkeyAliasBuilder();
		aliasBuilder.setAlias(alias1);
		aliasBuilder.setPublickey(PUBKEY_099); // sign with admin privatekey

		String rawTx = aliasBuilder.buildTransaction(client, PRIVKEY_099);

		try {
			// send transaction to a server
			client.sendTransaction(rawTx);
		} catch (CoinStackException e) {
			System.out.println(e.getDetailedMessage());
			e.printStackTrace();
		}

		// fetch a value after the setting alias
		puckey1 = client.getNodegroupPubkey(alias1);
		if (puckey1 != null)
			System.out.printf("after pubkey for %s = %s\n", alias1, Codecs.BASE64.encode(puckey1));
		else {
			System.out.println("pubkey is null. until new alias is accepted, it takes some time. plz wait");
		}

		// fetch all alias pair
		Map<String, byte[]> aliasPubkeyMap = client.listAllPubkey();

		System.out.println("[List All Alias -> Publickey]");
		for (Entry<String, byte[]> pair : aliasPubkeyMap.entrySet()) {
			System.out.printf(" * %s -> %s\n", pair.getKey(), Codecs.BASE64.encode(pair.getValue()));
		}

	}
}
