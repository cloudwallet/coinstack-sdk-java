package io.blocko.coinstack;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.PublicKey;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.ContractBody;
import io.blocko.coinstack.model.ContractResult;

public class LuaContractTest {
	
	public static final AbstractEndpoint TEST_ENDPOINT = new AbstractEndpoint() {
		@Override
		public String endpoint() {
			return "http://testchain.blocko.io";
		}
		@Override
		public boolean mainnet() {
			return true;
		}
		@Override
		public PublicKey getPublicKey() {
			return null;
		}
	};
	
	CoinStackClient coinstack = null;
	
	String DEF_LUA = null;
	String EXEC_LUA = null;
	String QUERY_LUA = null;
	
	@Before
	public void before() {
		coinstack = new CoinStackClient(null, TEST_ENDPOINT);
		try {
			DEF_LUA = loadContract("contract/def.lua");
			EXEC_LUA = loadContract("contract/exec.lua");
			QUERY_LUA = loadContract("contract/query.lua");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	public static String loadContract(String path) throws IOException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream in = loader.getResourceAsStream(path);
		StringWriter writer = new StringWriter();
		IOUtils.copy(in, writer);
		in.close();
		return writer.toString();
	}
	
	
	@Test
	public void testLuaContract() throws Exception {
		try {
			System.out.println("# testLuaContract()");
			// prepare test
			String privateKeyWIF = "L1mRDfYqCo3e4n28YpEKqDdSWZy7kWj91H4jjSH3qAz3zJx6XhHm";
			String address = "1AiUcrN2wS6wypcdLUaU6eNkQt2g9AfQKx";
			
			if (privateKeyWIF != null && !privateKeyWIF.isEmpty()) {
				if (address == null || address.isEmpty()) {
					address = ECKey.deriveAddress(privateKeyWIF);
				}
			}
			long balance = coinstack.getBalance(address);
			System.out.println("- address: "+address);
			System.out.println("  balance: "+balance);
			//System.out.println("  contract:");
			//System.out.println("  - DEF_LUA  :\n"+DEF_LUA);
			//System.out.println("  - EXEC_LUA :\n"+EXEC_LUA);
			//System.out.println("  - QUERY_LUA:\n"+QUERY_LUA);
			
			// fund required
			if (balance == 0) {
				System.out.println("  test is skipped because of empty address");
				return;
			}
			
			System.out.println("## query contract");
			ContractResult res = testContractQuery(address, QUERY_LUA);
			if (res == null) {
				System.out.println("  res: is null");
				// contract definition
				System.out.println("## define contract");
				testContractDefine(privateKeyWIF, DEF_LUA);
			}
			else {
				System.out.println("  res: "+res.asJson());
				// contract execution
				System.out.println("## execute contract");
				testContractExecute(privateKeyWIF, EXEC_LUA);
			}
			
			System.out.println("## query contract");
			res = testContractQuery(address, QUERY_LUA);
			if (res != null) {
				System.out.println("  res: "+res.asJson());
			}
			else {
				System.out.println("  res: is null");
			}
			
		} catch (CoinStackException e) {
			System.err.printf("%s: %s\n", e.getMessage(), e.getDetailedMessage());
		}
	}
	
	
	public String testContractDefine(String privateKeyWIF, String code) throws Exception {
		String contractId = ECKey.deriveAddress(privateKeyWIF);
		System.out.println("- contractId: "+contractId);
		LuaContractBuilder builder = new LuaContractBuilder();
		builder.setContractId(contractId);
		builder.setDefinition(code.getBytes());
		String rawTx = builder.buildTransaction(coinstack, privateKeyWIF);
		String txId = TransactionUtil.getTransactionHash(rawTx);
		System.out.println("  txId: "+txId);
		//System.out.println("  raw : "+rawTx);
		coinstack.sendTransaction(rawTx);
		return contractId;
	}
	public String testContractExecute(String privateKeyWIF, String code) throws Exception {
		String contractId = ECKey.deriveAddress(privateKeyWIF);
		System.out.println("- contractId: "+contractId);
		LuaContractBuilder builder = new LuaContractBuilder();
		builder.setContractId(contractId);
		builder.setExecution(code.getBytes());
		String rawTx = builder.buildTransaction(coinstack, privateKeyWIF);
		String txId = TransactionUtil.getTransactionHash(rawTx);
		System.out.println("  txId: "+txId);
		//System.out.println("  raw : "+rawTx);
		coinstack.sendTransaction(rawTx);
		return contractId;
	}
	public ContractResult testContractQuery(String contractId, String code) throws Exception {
		System.out.println("- contractId: "+contractId);
		return coinstack.queryContract(contractId, ContractBody.TYPE_LSC, code.getBytes());
	}

}
