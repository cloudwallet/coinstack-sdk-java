package io.blocko.coinstack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import org.bitcoinj.core.Coin;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.CoinStackClient;

import io.blocko.coinstack.model.CredentialsProvider;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.openassets.ColoringEngine;
import io.blocko.coinstack.openassets.util.Leb128;
import io.blocko.coinstack.openassets.util.Util;
import io.blocko.coinstack.util.Codecs;

/**
 * @author Nathan Lee
 *
 */
public class OpenAssetTest {
	protected CoinStackClient coinStackClient;
	protected ColoringEngine coloringEngine;
	protected CoinStackClient mockCoinStackClient;
	private static final long defaultAssetBTC = CoinMath.convertToSatoshi("0.000006");

	private static final long defaultFee = CoinMath.convertToSatoshi("0.0001");
	
	/**
	 * @throws java.lang.Exception
	 */
//	@Before
//	public void setUp() throws Exception {
//		coinStackClient = new CoinStackClient(new MockCoinStackAdaptor());
//	}
	@Before
	public void setUp() throws Exception {
		coinStackClient = new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getAccessKey() {
				return "e84ddc87dbb93d577907d524748e39";
			}

			@Override
			public String getSecretKey() {
				return "843a557f883ecec603aab5377d5c2a";
			}
		}, new AbstractEndpoint() {
			public boolean mainnet() {
				return true;
			}

			public PublicKey getPublicKey() {
				return null;
			}

			public String endpoint() {
				return "http://52.78.93.213:8080"; // testchain1
				//return "http://testchain.blocko.io";
				//return "http://mainnet.cloudwallet.io";
			}
		});
		coloringEngine = new ColoringEngine(coinStackClient);
		
		mockCoinStackClient = new CoinStackClient(new MockCoinStackAdaptor());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		coinStackClient.close();
	}

	@Test
	public void testOpenAssetIssueAndTransfer() throws Exception {
		System.out.println("## testOpenAssetIssueAndTransfer");
		
		// issue asset from t1 to t1
		System.out.println("### testIssueAsset");
		testIssueAsset();
		
		// transfer asset from t1 to t2
		System.out.println("### testTransferAsset");
		testTransferAsset();
		
		// transfer asset from t2 to t3
		System.out.println("### testTransferAssetMultisig");
		testTransferAssetMultisig();
		
		// transfer asset from t3 to t1
		System.out.println("### testTransferAssetMultisig2");
		testTransferAssetMultisig2();
		
		// utxo: t1
		System.out.println("### testUnspentOutput1");
		testUnspentOutput1();
		
		// utxo: t2
		System.out.println("### testUnspentOutput2");
		testUnspentOutput2();
		
		// utxo: t3
		System.out.println("### testUnspentOutput3");
		testUnspentOutput3();
	}
	
	//@Test
	public void testIssueAsset() throws Exception {
		// issue asset from t1 to t1
		String privateKeyWIF = "5Km2c58jwKu32XEAWRYV4x6yERiex7AVRz5J9QiKBcDeGiGFuPo";
		
		long assetAmount = 6667;
		String to = "akDQMunRtK15fVpBR83Jnv4ezQLtyerTJTB";
		long fee = CoinMath.convertToSatoshi("0.0002");
		String rawTx = coloringEngine.issueAsset(privateKeyWIF, assetAmount, to, fee);
		assertNotNull(rawTx);
		System.out.println(rawTx);
		assertNotNull(TransactionUtil.getTransactionHash(rawTx));
		System.out.println(TransactionUtil.getTransactionHash(rawTx));
		coinStackClient.sendTransaction(rawTx);
	}
	
	//@Test
	public void testTransferAsset() throws Exception {
		// transfer asset from t1 to t2
		String privateKeyWIF = "5Km2c58jwKu32XEAWRYV4x6yERiex7AVRz5J9QiKBcDeGiGFuPo";
		String assetID = "AKdiAtZGMuEUnZXm9tA3TPN6YJHzUarxRn";
		long assetAmount = 50;
		String to = "anFRNN5tcVPDu2JK5PWb376ST9T692UcGBV";

		long fee = CoinMath.convertToSatoshi("0.0001");
		String rawTx = coloringEngine.transferAsset(privateKeyWIF, assetID, assetAmount, to, fee);

		assertNotNull(rawTx);
		System.out.println(rawTx);
		assertNotNull(TransactionUtil.getTransactionHash(rawTx));
		coinStackClient.sendTransaction(rawTx);
	}
	
	@Test
	public void testLeb128() throws Exception {
		long assetAmount = 1000;
		byte [] res = Leb128.writeUnsignedLeb128((int)assetAmount);
		long resLong = Leb128.readUnsignedLeb128(res);
		System.out.println(resLong);
		System.out.println("assetAmountLEB128 : " +Codecs.HEX.encode(res));
		byte [] res2 = Codecs.HEX.decode("ce02");
		int resint = Leb128.readUnsignedLeb128(res2);
		System.out.println("resint : " + resint);
	}
	
	@Test
	public void testOAAddress() throws Exception {
		String redeemScript = "5241047f78d43ff2db4c6b5e46bbc7ee6a9a253b1e66c2aece8422bd1412c6997acd8cf8561dc5ff3dbaa8a4edd94cd270204db833e0db3d6ee53276736b30184fe57741049de5e58cb6f2a3e059958a69934618b6d02277926b2d3fcf5ad8b387ffc54119ff3bd8369c893455ff7f06e1304252ccd19efb3198f1f8feb94710173123fcdd4104bc61f87c32d0262c81a0fe95979fc54133c1c97f1da7882e586728fa0e7743863841a0a09e02ac1e13ac022e0d66ac84a42aae90c9ef96866e663234dceab49353ae";
		
		String assetMultisigAddress
			= Util.deriveAssetAddressFromRedeemScipt(redeemScript, true);
		
		String multisigAddress = MultiSig.createAddressFromRedeemScript(redeemScript, true);
		String resAssetAddress = Util.deriveAssetAddressFromAddress(multisigAddress);
		
		assertEquals(assetMultisigAddress, resAssetAddress);
	}
	
	@Test
	public void testOAAddress2() throws Exception {
		String bitcoinMultisigAddress = "35TV7v5H1uLDA49iLtUrTEXsoquy2C6PCe";
		String assetAddress = Util.deriveAssetAddressFromAddress(bitcoinMultisigAddress);
		System.out.println("resAssetAddress : " +assetAddress );
		
		String resBitcoinMultisigAddress = Util.deriveAddressFromAssetAddress(assetAddress);
		
		assertEquals(bitcoinMultisigAddress, resBitcoinMultisigAddress);
	}
	
	//@Test
	public void testTransferAssetMultisig() throws Exception {
		// transfer asset from t2 to t3
		String privateKey1 = "5JpxHsHWAFDJmGhQTypRf62s4UQYf59a2BnzDD2P7YxhXywraNF";
		String privateKey2 = "5JMH6KfwymcUXo1FgLJS2hC8NwicLWwTMVdHkiEgkwzvuymbast";
		String redeemScript = "5241047f78d43ff2db4c6b5e46bbc7ee6a9a253b1e66c2aece8422bd1412c6997acd8cf8561dc5ff3dbaa8a4edd94cd270204db833e0db3d6ee53276736b30184fe57741049de5e58cb6f2a3e059958a69934618b6d02277926b2d3fcf5ad8b387ffc54119ff3bd8369c893455ff7f06e1304252ccd19efb3198f1f8feb94710173123fcdd4104bc61f87c32d0262c81a0fe95979fc54133c1c97f1da7882e586728fa0e7743863841a0a09e02ac1e13ac022e0d66ac84a42aae90c9ef96866e663234dceab49353ae";
		
		List<String> prikeys = new ArrayList<String>();

		prikeys.add(privateKey1);
		prikeys.add(privateKey2);
		
		String assetID = "AKdiAtZGMuEUnZXm9tA3TPN6YJHzUarxRn";
		long assetAmount = 114;
		String to = "anMi7q9xPjU84mxDhdT1bp1APFdFZonzRZC";
		long fee = CoinMath.convertToSatoshi("0.0001");
		String rawTx = coloringEngine.transferMultisigAsset(prikeys, redeemScript, assetID, assetAmount, to, fee);
		assertNotNull(rawTx);
		System.out.println(rawTx);
		assertNotNull(TransactionUtil.getTransactionHash(rawTx));
		coinStackClient.sendTransaction(rawTx);
	}
	
	//@Test
	public void testTransferAssetMultisig2() throws Exception {
		// transfer asset from t3 to t1
		String privateKey1 = "KzUf9xhRafh2nK6J5AU7Q8QFXTDp5VHXRbcoUrVZWvZHroqwDM3W";
		String redeemScript = "5141047dc49912ec677e6840e6f7e5cdab9d12febe29c969e9386cf4ef3dc107d3d635b6a97f891955e532bddebecf6f257a02b225bdb5f1652ea7e77072cbe7bf90eb4104de30f82f274a02d40cd0234e4e9e87c946a882060ecb2da19c8f5e84e4d959bcbff8b654fd4662fc22dd6be0132253b8c17fab03d66a81ce2a5d76f08c871ea652ae";
		
		List<String> prikeys = new ArrayList<String>();

		prikeys.add(privateKey1);
		
		String from = MultiSig.createAddressFromRedeemScript(redeemScript, coinStackClient.isMainNet());
		System.out.println("testTransferAssetMultisig2().from="+from+", balance="+coinStackClient.getBalance(from));
		
		String assetID = "AKdiAtZGMuEUnZXm9tA3TPN6YJHzUarxRn";
		long assetAmount = 114;
		String to = "akDQMunRtK15fVpBR83Jnv4ezQLtyerTJTB";
		long fee = CoinMath.convertToSatoshi("0.0001");
		String rawTx = coloringEngine.transferMultisigAsset(prikeys, redeemScript, assetID, assetAmount, to, fee);
		assertNotNull(rawTx);
		System.out.println(rawTx);
		assertNotNull(TransactionUtil.getTransactionHash(rawTx));
		coinStackClient.sendTransaction(rawTx);
	}
	
	@Test
	public void testRedeemScript() throws Exception {
		// redeem script: t2
		String pub1 = "04bc61f87c32d0262c81a0fe95979fc54133c1c97f1da7882e586728fa0e7743863841a0a09e02ac1e13ac022e0d66ac84a42aae90c9ef96866e663234dceab493";
		String pub2 = "049de5e58cb6f2a3e059958a69934618b6d02277926b2d3fcf5ad8b387ffc54119ff3bd8369c893455ff7f06e1304252ccd19efb3198f1f8feb94710173123fcdd";
		String pub3 = "047f78d43ff2db4c6b5e46bbc7ee6a9a253b1e66c2aece8422bd1412c6997acd8cf8561dc5ff3dbaa8a4edd94cd270204db833e0db3d6ee53276736b30184fe577";
		List<byte[]> pubkeys = new ArrayList<byte[]>();

		pubkeys.add(Codecs.HEX.decode(pub1));
		pubkeys.add(Codecs.HEX.decode(pub2));
		pubkeys.add(Codecs.HEX.decode(pub3));
		String redeemScript = MultiSig.createRedeemScript(2, pubkeys);
		System.out.println(redeemScript);
		assertEquals(redeemScript, "5241047f78d43ff2db4c6b5e46bbc7ee6a9a253b1e66c2aece8422bd1412c6997acd8cf8561dc5ff3dbaa8a4edd94cd270204db833e0db3d6ee53276736b30184fe57741049de5e58cb6f2a3e059958a69934618b6d02277926b2d3fcf5ad8b387ffc54119ff3bd8369c893455ff7f06e1304252ccd19efb3198f1f8feb94710173123fcdd4104bc61f87c32d0262c81a0fe95979fc54133c1c97f1da7882e586728fa0e7743863841a0a09e02ac1e13ac022e0d66ac84a42aae90c9ef96866e663234dceab49353ae");
	}
	
	//@Test
	public void testUnspentOutput2() throws Exception {
		// utxo: t2
		Output[] outputs = coinStackClient.getUnspentOutputs("35TV7v5H1uLDA49iLtUrTEXsoquy2C6PCe");
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null) {
				System.out.println(outputs[i].getMetaData().getAsset_id() + " : " +outputs[i].getMetaData().getQuantity() );
			}
		}
		long bal = coinStackClient.getBalance("35TV7v5H1uLDA49iLtUrTEXsoquy2C6PCe");
		System.out.println(bal);
	}
	
	//@Test
	public void testUnspentOutput1() throws Exception {
		// utxo: t1
		Output[] outputs = coinStackClient.getUnspentOutputs("13SUfccYqXByda245RCcGCkR4jiocV1npd");
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null) {
				System.out.println(outputs[i].getMetaData().getAsset_id() + " : " +outputs[i].getMetaData().getQuantity() );
			}
		}
		long bal = coinStackClient.getBalance("13SUfccYqXByda245RCcGCkR4jiocV1npd");
		System.out.println(bal);
	}
	
	@Test
	public void testOneofTwo() throws Exception {
		String prikey1 = ECKey.createNewPrivateKey();
		byte [] pubkey1 = ECKey.derivePubKey(prikey1, false);
		String pubkeyString1  = Codecs.HEX.encode(pubkey1);
		String prikey2 = ECKey.createNewPrivateKey();
		byte [] pubkey2 = ECKey.derivePubKey(prikey2, false);
		String pubkeyString2  = Codecs.HEX.encode(pubkey2);
		System.out.println("prikey1 : " + prikey1);
		System.out.println("pubkeyString1 : " + pubkeyString1);
		System.out.println("prikey2 : " + prikey2);
		System.out.println("pubkeyString2 : " + pubkeyString2);
		List<byte[]> pubkeys = new ArrayList<byte[]>();
		pubkeys.add(pubkey1);
		pubkeys.add(pubkey2);
		String redeemScript = MultiSig.createRedeemScript(1, pubkeys);
		System.out.println("redeemScript : " + redeemScript);
		String bitcoinAddress = MultiSig.createAddressFromRedeemScript(redeemScript, true);
		System.out.println("bitcoinAddress : " + bitcoinAddress);
		String openassetAddress1 = Util.deriveAssetAddressFromAddress(bitcoinAddress);
		String openassetAddress2 = Util.deriveAssetAddressFromRedeemScipt(redeemScript, true);
		System.out.println("openassetAddress1 : " + openassetAddress1);
		System.out.println("openassetAddress2 : " + openassetAddress2);
		String originalAddress = Util.deriveAddressFromAssetAddress(openassetAddress1);
		System.out.println("originalAddress : " + originalAddress);
	}
	
	//@Test
	public void testUnspentOutput3() throws Exception {
		// utxo: t3
		Output[] outputs = coinStackClient.getUnspentOutputs("3BkEaz94FzENui4LapuRA9Fov25PoQxo9t");
		for (int i = 0; i < outputs.length; i++) {
			if (outputs[i].getMetaData() != null) {
				System.out.println(outputs[i].getMetaData().getAsset_id() + " : " +outputs[i].getMetaData().getQuantity() );
			}
		}
		long bal = coinStackClient.getBalance("3BkEaz94FzENui4LapuRA9Fov25PoQxo9t");
		System.out.println(bal);
	}
	
	@Test
	public void testCoinSelection() throws Exception {
		Output[] outputs = mockCoinStackClient.getUnspentOutputs("1z7Xp8ayc1HDnUhKiSsRz7ZVorxrRFUg6");
		System.out.println("totoal UTXO : " + outputs.length );
		Output[] res = coloringEngine.defaultCoinSelection(Coin.valueOf(defaultAssetBTC + defaultFee), outputs);
		System.out.println("selected UTXO : " + res.length);
		long sum = 0;
		for(int i = 0 ; i < res.length ; i++) {
			sum += res[i].getValue();
		}
		System.out.println("sum = " + sum);
	}
}