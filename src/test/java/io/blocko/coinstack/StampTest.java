package io.blocko.coinstack;

import static org.junit.Assert.*;

import java.security.PublicKey;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.CredentialsProvider;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack.util.Codecs;

public class StampTest {
	
	public static final AbstractEndpoint TESTCHAIN = new AbstractEndpoint() {
		@Override
		public String endpoint() {
			return "http://testchain.blocko.io";
		}
		@Override
		public boolean mainnet() {
			return true;
		}
		@Override
		public PublicKey getPublicKey() {
			return null;
		}
	};
	
	Random rand = null;
	CoinStackClient coinstack = null;
	
	@Before
	public void before() {
		rand = new Random();
		coinstack = new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getAccessKey() {
				return "17155ccf15e603853c19a35559f3f5"; // coinstack api key: "coinstack-java-sample"
			}
			@Override
			public String getSecretKey() {
				return "4ffe022576916bf0d9c4c13718d582";
			}
		}, TESTCHAIN);
	}
	
	@After
	public void after() {
		coinstack.close();
	}
	
	@Test
	public void test() throws Exception {
		System.out.println("## StampTest");
		
		String pkey = "L43neg4BPkPpCthhp9EsH3K29SxAqrTzpRPGRXub73sMfKes6NBP";
		String addr = "18YTexFrvJJzA52jBv5nmvZdhxFdMBqE6d";
		assertEquals(addr, ECKey.deriveAddress(pkey));
		
		// testset for mainnet
		/*
		String MAINNET_HASH = "b1674191a88ec5cdd733e4240a81803105dc412d6c6708d53ab94fc248f4f553";
		String MAINNET_STAMP_ID = "779f03ef104e654d29c808d5f4a3660d8c9bf961d6c7a75420c22bd972fb90ac-0";
		assertEquals(MAINNET_HASH, testGetStamp(MAINNET_STAMP_ID));
		*/
		
		// testset for testchain
		String TESTCHAIN_HASH = "b1674191a88ec5cdd733e4240a81803105dc412d6c6708d53ab94fc248f4f553";
		String TESTCHAIN_STAMP_ID = "4b8214a2ae485e35ff393b7ee6525a684e413ce1db7a4b595773b551ab4818b9-0";
		assertEquals(TESTCHAIN_HASH, testGetStamp(TESTCHAIN_STAMP_ID));
		
		// stamp new hash and lookup
		assertTrue(coinstack.getBalance(addr) > 0);
		String hash = generateNewHash();
		String stampId = testStampDocument(pkey, hash);
		assertEquals(hash, testGetStamp(stampId));
	}
	
	public String generateNewHash() {
		byte[] sampleInput = new byte[rand.nextInt(512)];
		rand.nextBytes(sampleInput);
		return Codecs.SHA256.digestEncodeHex(sampleInput);
	}
	
	public String testStampDocument(String pkey, String hash) throws Exception {
		System.out.println("- testStampDocument(): hash="+hash);
		String stampId = coinstack.stampDocument(pkey, hash);
		System.out.println("  stampId="+stampId);
		assertNotNull(stampId);
		return stampId;
	}
	public String testGetStamp(String stampId) throws Exception {
		System.out.println("- testGetStamp(): stampId="+stampId);
		Stamp stamp = null;
		for (int i=0; i<3; i++) {
			try {
				stamp = coinstack.getStamp(stampId);
			} catch (CoinStackException e) {
				assertTrue(e.getMessage().startsWith("Requested resource not found"));
				Thread.sleep(500);
			}
		}
		assertNotNull(stamp);
		printStamp("  ", stamp);
		return stamp.getHash();
	}
	public void printStamp(String indent, Stamp stamp) {
		System.out.println(indent+"txId="+stamp.getTxId());
		System.out.println(indent+"vout="+stamp.getOutputIndex());
		System.out.println(indent+"confirmations="+stamp.getConfirmations());
		System.out.println(indent+"timestamp="+stamp.getTimestamp());
		System.out.println(indent+"hash="+stamp.getHash());
	}
}
