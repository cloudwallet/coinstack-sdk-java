package io.blocko.coinstack.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.security.GeneralSecurityException;
import java.util.Base64;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CryptoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDecryptCBytes() {
		String[] base64cbytes = {
				"AwAAAAEAAABbLFQB+ha33Yp9dsDMdpjbuZQBdw6WYuigSOBD4bl8KNX5poIbDbZlCy0qFnQ/oMdpiDCaTLIS/h5R/tKRf8BW1fEdtPVAALkLoAqqauU6uCnBg2Q+7zQf0ZLXIwjl4O4uTxv29VS0DuG4wnmsuovaO50zSJ0lnOOUUTlP4JzCEGavcKg05fRSIiwl8vHYu2nenS3Vy6B5Vp1eT2YcVAfH",
				"AwAAAAMAAAAerllCRpD36NDZn2MFMCDO29G6m4CxuI5lklTXB2lxJT7jS5vqIyPuQEgjh0MUX24Lp9gEPVKCP1/Rm7q0LRBAyNKAvW0CuVYEI9UJ4iiyOu/68qSiU8RAskCTlOhgl9jYfqUFXAaLtsEdTwLfrzYMQPHzsrDuzOq0Vik45C0dtbW07ogKBEinWcg6rDjUIw==",
				"AwAAAAMAAAA2Zerm4qgYLG55ZYBb51TCrE/qy4XGvMgyaWv95ADPRdAqHFFmtxfvPKzBbL4kDW6CcijowJa5roQH6SQfXQ96vm8XYVgqHCx4kmmGyYZmwUJr96QmcqrHheBWzfsZlGSR8P5Xej0p59JmsY4BuTo7nyHR62JTM1qQtCzMA1y5N4tC0CfubcS9a4xbgDsTug==",
				"AwAAAAIAAAAVEQZbxqf+099s+EY/2XsBA9h8fsrKo2AMGU1G8l6NcdxUMEVv1PdjOYtVqRsyRUNsNgcuF2JkP+H4U3+C8M7O6ZEIogmZ2EH52spd5O06x7kNhK399nHBSPJS6fwzK/QD4ACf6FJX6BuNqDtzouyo9PvJbkQClS2emNQmcqgHLlQ/XqLZXbBHLXXnyFiU7WgzrWzvriuzvLnaooLHQNrq",
		};

		for (int i = 0; i < base64cbytes.length; i++) {
			Crypto c = new Crypto("abcdefg12345^&*()");
			byte[] cbytes = Codecs.BASE64.decode(base64cbytes[i]);
			try {
				byte[] pbytes = c.decrypt(cbytes);
				System.out.println("plain text: " + new String(pbytes));
			} catch (GeneralSecurityException e) {
				System.err.println("[" + i + "]: " + e.getMessage());
				e.printStackTrace();
				fail(e.getMessage());
			}
		}
	}

	@Test
	public void testAES256Default() {
		String plainTxt = "Hi. This is an arbitrary plain text to test 'crypto' package of 'coinstack'.";
		String pw = "abcdefg12345^&*()";

		// Encrypt
		Crypto c = new Crypto(pw);
		byte[] cbytes = null;
		try {
			cbytes = c.encrypt(plainTxt.getBytes());
		} catch (GeneralSecurityException e) {
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.DEFAULT_TYPE);
		byte[] salt = c.getSalt();
		byte[] iv = c.getIv();
		byte[] secretKeyHash = c.getSkHash();

		System.out.println(Base64.getEncoder().encodeToString(cbytes));

		// Decrypt
		c = new Crypto(pw);
		byte[] pbytes = null;
		try {
			pbytes = c.decrypt(cbytes);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.DEFAULT_TYPE);
		assertEquals(new String(pbytes), plainTxt);
		assertArrayEquals(salt, c.getSalt());
		assertArrayEquals(iv, c.getIv());
		assertArrayEquals(secretKeyHash, c.getSkHash());
	}

	@Test
	public void testAES256withHMAC() {
		String plainTxt = "Hi. This is an arbitrary plain text to test 'crypto' package of 'coinstack'.";
		String pw = "abcdefg12345^&*()";

		// Encrypt
		Crypto c = new Crypto(pw, Crypto.Type.AES_256_CBC_HMAC);
		byte[] cbytes = null;
		try {
			cbytes = c.encrypt(plainTxt.getBytes());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.Type.AES_256_CBC_HMAC);
		byte[] salt = c.getSalt();
		byte[] iv = c.getIv();
		byte[] secretKeyHash = c.getSkHash();

		System.out.println(Base64.getEncoder().encodeToString(cbytes));

		// Decrypt
		c = new Crypto(pw);
		byte[] pbytes = null;
		try {
			pbytes = c.decrypt(cbytes);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.Type.AES_256_CBC_HMAC);
		assertEquals(new String(pbytes), plainTxt);
		assertArrayEquals(salt, c.getSalt());
		assertArrayEquals(iv, c.getIv());
		assertArrayEquals(secretKeyHash, c.getSkHash());
	}

	@Test
	public void testAES256withPBKDF2() {
		String plainTxt = "Hi. This is an arbitrary plain text to test 'crypto' package of 'coinstack'.";
		String pw = "abcdefg12345^&*()";

		// Encrypt
		Crypto c = new Crypto(pw, Crypto.Type.AES_256_CBC_PBKDF2);
		byte[] cbytes = null;
		try {
			cbytes = c.encrypt(plainTxt.getBytes());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.Type.AES_256_CBC_PBKDF2);
		byte[] salt = c.getSalt();
		byte[] iv = c.getIv();
		byte[] secretKeyHash = c.getSkHash();

		System.out.println(Base64.getEncoder().encodeToString(cbytes));

		// Decrypt
		c = new Crypto(pw);
		byte[] pbytes = null;
		try {
			pbytes = c.decrypt(cbytes);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.Type.AES_256_CBC_PBKDF2);
		assertEquals(new String(pbytes), plainTxt);
		assertArrayEquals(salt, c.getSalt());
		assertArrayEquals(iv, c.getIv());
		assertArrayEquals(secretKeyHash, c.getSkHash());
	}

	@Test
	public void testSimpleAES256() {
		String plainTxt = "Hi. This is Java SDK to test 'crypto' package of 'coinstack'.";
		String pw = "abcdefg12345^&*()";

		// Encrypt
		Crypto c = new Crypto(pw, Crypto.Type.AES_256_CBC_SIMPLE);
		byte[] cbytes = null;
		try {
			cbytes = c.encrypt(plainTxt.getBytes());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.Type.AES_256_CBC_SIMPLE);
		byte[] salt = c.getSalt();
		byte[] iv = c.getIv();
		byte[] secretKeyHash = c.getSkHash();

		System.out.println(Base64.getEncoder().encodeToString(cbytes));

		// Decrypt
		c = new Crypto(pw);
		byte[] pbytes = null;
		try {
			pbytes = c.decrypt(cbytes);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertEquals(c.getType(), Crypto.Type.AES_256_CBC_SIMPLE);
		assertEquals(new String(pbytes), plainTxt);
		assertArrayEquals(salt, c.getSalt());
		assertArrayEquals(iv, c.getIv());
		assertArrayEquals(secretKeyHash, c.getSkHash());
	}
}