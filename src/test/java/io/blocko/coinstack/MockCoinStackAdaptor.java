/**
 * 
 */
package io.blocko.coinstack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import io.blocko.coinstack.backendadaptor.AbstractCoinStackAdaptor;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Block;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.ContractResult;
import io.blocko.coinstack.model.Input;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack.model.Subscription;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.openassets.model.AssetOutput;
import io.blocko.coinstack.permission.model.Grantee;
import io.blocko.coinstack.permission.model.Role;

/**
 * @author nepho
 *
 */
public class MockCoinStackAdaptor extends AbstractCoinStackAdaptor {
	Map<String, Block> blockDB = new HashMap<String, Block>();
	{
		blockDB.put(
				"000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f",
				new Block(
						new Date(1231006505000L),
						"000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f",
						new String[] { "00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048" },
						0,
						null,
						new String[] { "4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b" }));
		blockDB.put(
				"00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048",
				new Block(
						new Date(1231469665000L),
						"00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048",
						new String[] { "000000006a625f06636b8bb6ac7b960a8d03705d1ace08b1a19da3fdcc99ddbd" },
						1,
						"000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f",
						new String[] { "0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098" }));
	}
	Map<String, Transaction> transactionDB = new HashMap<String, Transaction>();
	{
		transactionDB
				.put("4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b",
						new Transaction(
								"4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b",
								new String[] { "000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f" },
								new Date(1231006505000L),
								true,
								new Input[] {},
								new Output[] { new Output(
										null,
										0,
										"1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
										false,
										5000000000l,
										"4104678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5fac") }));

		transactionDB
				.put("001f5eba608a84ba97dd7ac1b21b822b74c91ffbd75c42c7b88abe178f632b31",
						new Transaction(
								"001f5eba608a84ba97dd7ac1b21b822b74c91ffbd75c42c7b88abe178f632b31",
								new String[] { "00000000000000000fad06ca404d52a779d452000057a8342b064618d05a4450" },
								new Date(1425014682000L),
								false,
								new Input[] { new Input(
										1,
										"1Dn86V7bJ7Knv716jj811aXHikyHFD1HQ1",
										"f693cadeacdbb2d980155fbafc82f00c607f2a1fb185cd27b054064b43d00f16",
										7998950000L) },
								new Output[] {
										new Output(
												null,
												0,
												"15Zf4AybWDV6QRcaJ4ErowVxhpdG89Qjni",
												false, 600000000L,
												"76a914320d9492f6b348e003a1ba30afca95eb8d0609e588ac"),
										new Output(
												null,
												0,
												"1Dn86V7bJ7Knv716jj811aXHikyHFD1HQ1",
												false, 7398940000L,
												"76a9148c2a2661cb4afd3ae0c1ea7b1beb5d34e769dbbc88ac") }));
	}
	Map<String, String> rawTransactionDB = new HashMap<String, String>();
	{
		rawTransactionDB.put(
				"hex_"+"001f5eba608a84ba97dd7ac1b21b822b74c91ffbd75c42c7b88abe178f632b31",
				"0100000001160fd0434b0654b027cd85b11f2a7f600cf082fcba5f1580d9b2dbacdeca93f601000"
				+ "0006a4730440220382e1657418c59988d976920496cb34ee29a97ec74ca7baa69bfc4a792d2dae9"
				+ "022032d4597e592f377827c7d8d31619acbf06ffeb99796e18d87599211b43a78e2001210226e4d"
				+ "93ed8fb1e251766b04ffc4b9cccbbf7f7c7baf954e694c1e4c7535219f2ffffffff020046c32300"
				+ "0000001976a914320d9492f6b348e003a1ba30afca95eb8d0609e588ac60dd02b9010000001976a"
				+ "9148c2a2661cb4afd3ae0c1ea7b1beb5d34e769dbbc88ac00000000");
	}
	Map<String, List<String>> addressHistoryDB = new HashMap<String, List<String>>();
	{
		List<String> list1 = new ArrayList<String>();
		list1.add("4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b");
		list1.add("3387418aaddb4927209c5032f515aa442a6587d6e54677f08a03b8fa7789e688");
		addressHistoryDB.put("1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa", list1);

		List<String> list2 = new ArrayList<String>();
		list2.add("f762df2ff3340171ce913bd3fc2534ee4f15166ffff46a85019eeb52efb8da9b");
		addressHistoryDB.put("1changeFu9bT4Bzbo8qQTcHS7pRfLcX1D", list2);

		List<String> list3 = new ArrayList<String>();
		list3.add("f762df2ff3340171ce913bd3fc2534ee4f15166ffff46a85019eeb52efb8da9b");
		addressHistoryDB.put("1z7Xp8ayc1HDnUhKiSsRz7ZVorxrRFUg6", list3);
	}
	Map<String, Long> addressBalanceDB = new HashMap<String, Long>();
	{
		addressBalanceDB.put("1z7Xp8ayc1HDnUhKiSsRz7ZVorxrRFUg6", new Long(
				4580000000L));
	}
	Map<String, List<Output>> unspentOutputDB = new HashMap<String, List<Output>>();
	{
		List<Output> outputList = new ArrayList<Output>();
		outputList
				.add(new Output(
						"9bdab8ef52eb9e01856af4ff6f16154fee3425fcd33b91ce710134f32fdf62f7",
						0, "1z7Xp8ayc1HDnUhKiSsRz7ZVorxrRFUg6", false,
						4580000000L,
						"76a9140acd296e1ba0b5153623c3c55f2d5b45b1a25ce988ac"));
		unspentOutputDB.put("1z7Xp8ayc1HDnUhKiSsRz7ZVorxrRFUg6", outputList);
	}
	Map<String, List<AssetOutput>> unspentAssetOutputDB = new HashMap<String, List<AssetOutput>>();
	{
		List<AssetOutput> outputList = new ArrayList<AssetOutput>();
		outputList
				.add(new AssetOutput(
						CoinStackClient.convertEndianness("048e913c1b1f7bdca558c26d7d9456a7fe2e2757a82536e70a06cf35ef907604"),
						0, "144jCjBySWvqVv22Upkm6ez6zTVYZYwU9p", false,
						600L,
						"76a914219cda96533a2048068a5ffcee2dc3b70c84282088ac",
						"AKJFoih7ioqPXAHgnDzJvHE8x2FMcFerfv",
						1000l));
		
		outputList
		.add(new AssetOutput(
				CoinStackClient.convertEndianness("85a44f3f8e0f25d1a7112056341e9ff698cbf54b69ae363ad8bd480758aead16"),
				1, "144jCjBySWvqVv22Upkm6ez6zTVYZYwU9p", false,
				184800L,
				"76a914219cda96533a2048068a5ffcee2dc3b70c84282088ac",
				null,
				0));  
		unspentAssetOutputDB.put("144jCjBySWvqVv22Upkm6ez6zTVYZYwU9p", outputList);
		
		
	}

	public int getBestHeight() throws IOException {
		return 345229;
	}

	public String getBestBlockHash() throws IOException {
		return "00000000000000000326307b927806f617277c8650b70e66d78eab8323423a33";
	}

	public BlockchainStatus getBlockchainStatus() throws IOException, CoinStackException {
		return new BlockchainStatus(getBestHeight(), getBestBlockHash());
	}
	
	public Block getBlock(String blockId) throws IOException {
		return blockDB.get(blockId);
	}

	public String[] getBlockHash(int blockHeight) throws IOException {
		return new String[] { "00000000000000000326307b927806f617277c8650b70e66d78eab8323423a33" };
	}

	public String getRawTransaction(String transactionId, String format) throws IOException {
		return rawTransactionDB.get(format.toLowerCase()+"_"+transactionId);
	}

	public Transaction getTransaction(String transactionId) throws IOException {
		return transactionDB.get(transactionId);
	}
	
	public ContractResult queryContract(String contractId, String type, String body)
			throws IOException, CoinStackException {
		return null;
	}
	
	public String[] getTransactions(String address) throws IOException {
		return addressHistoryDB.get(address).toArray(new String[0]);
	}
	public String[] getTransactions(String address, int skip, int limit, int confirmed) throws IOException {
		return addressHistoryDB.get(address).toArray(new String[0]);
	}

	public long getBalance(String address) throws IOException {
		return addressBalanceDB.get(address);
	}

	public Output[] getUnspentOutputs(String address, long amount) throws IOException {
		return unspentOutputDB.get(address).toArray(new Output[0]);
	}
	
	public AssetOutput[] getUnspentAssetOutputs(String address) throws IOException {
		return unspentAssetOutputDB.get(address).toArray(new AssetOutput[0]);
	}

	@Override
	public void init() {
	}

	@Override
	public void fini() {
	}

	@Override
	public void sendTransaction(String rawTransaction) throws IOException {
		// do nothing
	}

	@Override
	public Subscription[] listSubscriptions() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteSubscription(String id) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public String addSubscription(Subscription newSubscription)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isMainnet() {
		return true;
	}

	@Override
	public String stampDocument(String hash) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stamp getStamp(String stampId) throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getNodegroupPublicKey() throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getPublickey(String aliasName) throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, byte[]> listAllPubkey() throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Role> listAllRole() throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Role getRole(String address) throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Role getEnabledRole() throws IOException, CoinStackException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Grantee[] getGrantees(String contractId) throws IOException, CoinStackException {
		return new Grantee[0];
	}
}
