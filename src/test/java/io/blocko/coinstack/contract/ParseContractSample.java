package io.blocko.coinstack.contract;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.RegTestParams;
import org.json.JSONException;
import org.json.JSONObject;

import io.blocko.coinstack.TransactionUtil;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.util.Codecs;

public class ParseContractSample {

	// marker(4) + version(4) + opType(4) + contentHash(64)
	private static int ScPayloadHeaderSize = 4 + 4 + 4 + 64;
	private static String sampleRawTx = "0100000001ab9a4d4ef2fe801cb6aa4e6098399c5be539de8a654572a3601945606ed72a91020000006b483045022100c3793f335f53a923e0bf121b336245e5e0600c649bd01de05d31fe2c532a58e8022058988b205607835afa9a061d4b34bf64a06cb47bc2a8225d63b71c4fcbb046af0121033d2caaa3c76985976aa50257065011a17b1a93fa0cd340b9e179dd6c19ef0581ffffffff0358020000000000001976a914199c90b984e042303f1a594c4ee237dedef3772088ac0000000000000000fd1f016a4d1b014f4301000002b81a6237c75d6f0498a6f3a0934df1c1f5ea4cc10f32c24daa4288387f3c2359646174613a746578742f706c61696e3b636861727365743d7574662d383b6261736536342c65794a306558426c496a6f6954464e4449697769646d567963326c76626949364d6977695a57356a496a6f774c434a6962325235496a6f6951554642515546466330464251554a35576c684e63306c484f584a4a5244426e57544a4763324a445a326c6a4d6c597753576c335a306c75556d786a4d314a79576c68726155784451576c6b52315a365a45686161474a49566d784e616b6c3553576c724e306c48526e706a4d6c5a355a454e6f646d4635617a644a534570735a45685765574a70516e6c61574530335246467650534a39d0490e00000000001976a914199c90b984e042303f1a594c4ee237dedef3772088ac00000000";
	//private static String sampleRawTxUsingAddrData = "0100000001ab9a4d4ef2fe801cb6aa4e6098399c5be539de8a654572a3601945606ed72a91020000006a473044022026d60bcdaa21a7580ac379c16d7674946127e312bfb2df78da25710a21a07bf002207175e9e056b120b4960aad9a590f23e99e8bd5d68af480bef53b7e6f8b2dbaee0121033d2caaa3c76985976aa50257065011a17b1a93fa0cd340b9e179dd6c19ef0581ffffffff0300000000000000001b6a194f5a010000199c90b984e042303f1a594c4ee237dedef377200000000000000000fd97036a4d93034f4302000001adf22c19398fae11496fd07142e23d13017b451e2d90b9156eb37fa18aeecc8f646174613a746578742f706c61696e3b636861727365743d7574662d383b6261736536342c65794a306558426c496a6f6954464e4449697769646d567963326c76626949364d6977695a57356a496a6f774c434a6962325235496a6f69515546425155464c64304a4251554a7a596a4a4f61474a44516e706c57453477576c63775a314254516e6c61574559785956684b62457444536e706c57453477576c637761557455633035445a7a424c576d355764566b7a556e42694d6a526e5930684b63474a75556b4e69527a6c7159586c6e634552526230706a4d3278365a45645764457875516e6c68567a55775330684f4e574d7a556d7869557a5675576c685351324a484f5770684d6d686f597a4a6e62307454617a64455557394b597a4e73656d5248566e524d626b4a35595663314d457449546a566a4d314a73596c4d31626c7059556b4e69527a6c7159544a6f624746585a47396b513264775331527a546b4e74566e56615154424c52464677625752584e57706b52327832596d6c43626c7059556b4e69527a6c7159586c6e634552526230706a625659775a46684b64556c49633035445a32744b59556447656d464451546c4a53453431597a4e5362474a544e57356157464a44596b6335616d45796147686a4d6d647653314e33546b4e6e6130706852315a77576a4a6f4d456c454d47646a4d3278365a456457644578745a47786b5255707a596a4a4f636d4648566e42614d6d677753304e72546b4e6e62446c4555584273596d3152546b4e6e4d457461626c5a3157544e53634749794e47646a4d6c59775330647a63306c49575842455557394b597a4e73656d5248566e524d626b35735a4556734d4670584d4739686558646e5a476c72546b4e74566e56615154424c52464677625752584e57706b52327832596d6c43626c7059555739686557744f5132647365567059556a466a6254526e597a4e73656d5248566e524d625752735a4556734d4670584d4739686557744f51323157645670424d457445555842745a466331616d524862485a6961554977576c684f4d464a59536e6c694d306c765331457753304e59516e6c68567a557753304e4b4d467059546a4268567a56755355645765574e744f586c4a6157744f5132647361474d7a546d786a626c4676576d314763324d795658424555584273596d3152546b4e6e5054306966513d3d284c0e00000000001976a914199c90b984e042303f1a594c4ee237dedef3772088ac00000000";
	private static String sampleRawTxUsingAddrData = "01000000015d39350997f78605c30ec7c0ac7a0c0a3bc165cea0fbcbcf38a66725c6fa6f01000000006a473044022055b04e15d210bae5c6df087040ca07c08278584b948949505bd529d08e5a20ae02205ac0fe55b37974b16eca3ec068e3c87d45a58c2fc335a52a99abecb41b4cbc83012102ed81e0f45d757a2e1da045ca70d32806ec8ab803ea03e665c6baf6cdda65839affffffff0358020000000000001976a9143aa357a029d93f96136b58d1d4b2624c55e6d86d88ac0000000000000000fd97036a4d93034f43010000011a92a3b2116316d1ce6d382b899099e1162e5470e3aebe0070d09b278c6db511646174613a746578742f706c61696e3b636861727365743d7574662d383b6261736536342c65794a306558426c496a6f6954464e4449697769646d567963326c76626949364d5377695a57356a496a6f774c434a6962325235496a6f69636b46465155464865485a5a4d6b5a7a5355684f4e574d7a556d7869553045355355684b62474e59566e426a625656765357354f4e574d7a556d786955306c7754336377533052526347316b567a56715a456473646d4a70516e646a625778315a45564b63324979546e4a4c5132744f51326473656d5659546a4261567a42315930684b63474a755557396a4d3278365a456457644578745a47786b5255707a596a4a4f636d4648526e7068513264775331527a546b4e6e6248706c57453477576c637764574e49536e4269626c4676597a4e73656d5248566e524d625752735a45564b63324979546e4a6852315a77576a4a6f4d45744461334250647a424c576c6331613052526230354462566f78596d314f4d4746584f58564a523252735a45564b63324979546e4a4c5132744f5132647365567059556a466a6254526e5a58637753304e526247395a57453576535551775a324d7a6248706b52315a305447316b62475246536e4e694d6b357959556447656d46445a33424d5154424c5131467362317058624735685346466e55464e43656d5659546a4261567a4231576a4a574d46467465485a5a4d6e5276576c6473626d46495557394c5554424c51316777546b4e74566e56615154424c52464677625752584e57706b52327832596d6c43656c7059555739686558646e5a476c72546b4e6e6248706c57453477576c637764574d79566a425457464a73596c4e6f636b7844516a4a4c5554424c576c6331613052526230354462566f78596d314f4d4746584f58564a523252735a454e6f636b74524d457444574570735a45685765574a70516e706c57453477576c637764566f79566a425457464a73596c4e6f636b74524d457461567a567252464676546b4e74576a4669625534775956633564556c49556d786a4d314a475932354b646d4e705a3342455557394b5930684b63474a755557394a626c4a73597a4e5363474a745932646157457035596a4e4a615574524d45744456305a36597a4a57655752446147315a56336836576c4e72546b4e74566e56615154424c51554642515546425054306966513d3d98c8052a010000001976a9143aa357a029d93f96136b58d1d4b2624c55e6d86d88ac00000000";
	
	public static class SmartContract {
		public String majorVersion;
		public String minorVersion;
		public String languageType; // exe, def
		public int operatorType; // none=0, definition=1, execution=2
		public int encryptCode;
		
		public String contractId;
		public String code;
		
		public SmartContract() {
			// do something
		}
	}
	
	public static class AddressData {
		public String majorVersion;
		public String minorVersion;
		NetworkParameters network;
		public String address;
	}
	
	public static AddressData parseAddressData(Output output) {
		byte[] dataBytes = output.getData();
		
		if(dataBytes.length != 25)
			return null;
		
		byte[] header = new byte[4];
		System.arraycopy(dataBytes, 0, header, 0, 4);

		String dataStr = Codecs.HEX.encode(header);
		
		if(!dataStr.startsWith("4f5a")) {
			return null; // not a smart contract data output
		}
		
		AddressData addrData = new AddressData();
		
		// parse version info
		addrData.majorVersion = dataStr.substring(4, 6);
		addrData.minorVersion = dataStr.substring(6, 8);
		
		byte[] addrBytes = new byte[20];
		System.arraycopy(dataBytes, 5, addrBytes, 0, 20);
		
		// check network parameter; testnet, mainnet
		// and parse address
		if ((int) dataBytes[4] == 0) {
			addrData.network = MainNetParams.get();
			addrData.address =  new Address(MainNetParams.get(), addrBytes).toString();
			
		} else {
			addrData.network = RegTestParams.get();
			addrData.address =  new Address(RegTestParams.get(), addrBytes).toString();
		}
		
		return addrData;		
	}
	
	public static SmartContract parseFullSmartContract(Transaction tx) {
		// a number of tx output for smart contracts must be bigger than 2 
		if(tx == null || tx.getOutputs().length < 2) {
			return null; // not a smart contract transaction
		}
		
		// get a second tx output.
		Output scCanditateOut = tx.getOutputs()[1];
		
		// in the case of smart contract tx, it must be a null-data format
		// so address field will be empty
		if(!scCanditateOut.getAddress().isEmpty()) {
			return null; // not a data transaction
		}
		
		String dataStr = Codecs.HEX.encode(scCanditateOut.getData());
	
		// check smart contract tx's marker
		if(!dataStr.startsWith("4f43") || dataStr.length() < ScPayloadHeaderSize) {
			return null; // not a smart contract data output
		}

		// parse version info
		SmartContract result = new SmartContract();
		result.majorVersion = dataStr.substring(4, 6);
		result.minorVersion = dataStr.substring(6, 8);
		
		if (dataStr.substring(8, 12).equals("0001")) {
			result.operatorType = 1; 
		} else if (dataStr.substring(8, 12).equals("0002")) {
			result.operatorType = 2;
		} else {
			return null;
		}
		
		// parse target smartcontract address
		Output smartContractFirstOut = tx.getOutputs()[0];
		
		// when a miner version is 00, it has header. Cut it 
		if (result.majorVersion.equals("01")) {
			// dusty p2sh output
			result.contractId = smartContractFirstOut.getAddress();
		} else if (result.majorVersion.equals("02")) {
			// addr-data output
			AddressData addrData = parseAddressData(smartContractFirstOut);
			if(addrData == null) 
				return null;
			result.contractId = addrData.address;
		}

		String decodedData = new String(Codecs.HEX.decode(dataStr.substring(ScPayloadHeaderSize)));

		if (result.minorVersion.equals("00")) {
			decodedData = decodedData.substring("data:text/plain;charset=utf-8;base64,".length());
		}

		String decodedJsonStr = Codecs.BASE64.decodeToString(decodedData);

		try {
			JSONObject jsonObj = new JSONObject(decodedJsonStr);
			result.languageType = jsonObj.getString("type");
			result.encryptCode = jsonObj.getInt("enc");
			
			if (result.encryptCode != 0) {
				result.code = "(do not support decode an encrypted contract code)";
			} else {
				// Get body and Remove encrypt header: arg num(2) + earg num(2) + code len(4) + code
				result.code = Codecs.BASE64.decodeToString(jsonObj.getString("body")).substring(2 + 2 + 4);
			}
			
		} catch (JSONException e) {
			System.out.println(e.getMessage());
			return null;
		}
		
		return result;
	}
	
	public static String parseOnlySmartContractCode(Transaction tx) {

		// a number of tx output for smart contracts must be bigger than 2 
		if(tx == null || tx.getOutputs().length < 2) {
			return null; // not a smart contract transaction
		}
		
		// get a second tx output.
		Output scCanditateOut = tx.getOutputs()[1];
		
		// in the case of smart contract tx, it must be a null-data format
		// so address field will be empty
		if(!scCanditateOut.getAddress().isEmpty()) {
			return null; // not a data transaction
		}
		
		String dataStr = Codecs.HEX.encode(scCanditateOut.getData());
	
		// check smart contract tx's marker
		if(!dataStr.startsWith("4f43") || dataStr.length() < ScPayloadHeaderSize) {
			return null; // not a smart contract data output
		}

		// parse version info
		String minorVersion = dataStr.substring(6, 8);
		
		String decodedData = new String(Codecs.HEX.decode(dataStr.substring(ScPayloadHeaderSize)));

		// when a miner version is 00, it has header. Cut it
		if (minorVersion.equals("00")) {
			decodedData = decodedData.substring("data:text/plain;charset=utf-8;base64,".length());
		}

		String decodedJsonStr = Codecs.BASE64.decodeToString(decodedData);

		try {
			JSONObject jsonObj = new JSONObject(decodedJsonStr);
			// Get body and Remove encrypt header: arg num(2) + earg num(2) + code len(4) + code
			return Codecs.BASE64.decodeToString(jsonObj.getString("body")).substring(2 + 2 + 4);
			
		} catch (JSONException e) {
			System.out.println(e.getMessage());
			return "";
		}
	}

	public static void main(String[] args) {
		
		// This is temporal tx to test. 
		//Use coinstackclient.getTransaction(hash) for an actual usage
		Transaction tx = TransactionUtil.parseRawTransaction(sampleRawTx);

		// call a parse functions. Use parseOnlySmartContractCode() to simply parse a code 
		String resultStr = parseOnlySmartContractCode(tx);
		
		System.out.println("========== raw tx parsing result ==========");
		System.out.println(resultStr);
		
		// test address data format tx
		System.out.println("========== raw tx + addr data parsing result ==========");
		Transaction tx2 = TransactionUtil.parseRawTransaction(sampleRawTxUsingAddrData);
		SmartContract result2 = parseFullSmartContract(tx2);
		System.out.println("contract id: " + result2.contractId);
		System.out.println("version: " + result2.majorVersion + result2.minorVersion);
		System.out.println("language type: " + result2.languageType);
		System.out.println("op typo: " + result2.operatorType);
		System.out.println("encrypt type: " + result2.encryptCode);
		System.out.println(result2.code);		
	}
}
