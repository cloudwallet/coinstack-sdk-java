package io.blocko.coinstack.contract;

import static org.junit.Assert.assertEquals;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.GeneralSecurityException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.util.Codecs;
import io.blocko.coinstack.util.Crypto;

public class LuaContractCodeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNoEnc() {
		String code = "res, ok = call($FUNC, ? , ?, ?)";
		String expCode = "res, ok = call(?, ? , ?, ?)";
		LuaContractCode lcc = null;
		try {
			lcc = new LuaContractCode(code);
			assertEquals(lcc.getCode(), expCode);
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		}

		try {
			lcc.setParam(1, "test");
			lcc.setParam(2, 10);
			lcc.setParam(3, "1234");
			lcc.setParam(4, 0.001);
			System.out.println("  - code with parameters:\n" + lcc.toString());
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		}
	}

	@Test
	public void testParamName() {
		String code = "res, ok = $CALL($FUNC, $ABC , $FUNC, $ABC, $BC, $AB, $AC)";
		String expCode = "res, ok = ?(?, ? , ?, ?, ?, ?, ?)";
		LuaContractCode lcc = null;
		try {
			lcc = new LuaContractCode(code);
			assertEquals(expCode, lcc.getCode());
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		}

		try {
			lcc.setParam("BC", "1234", false, true);
			lcc.setParam("FUNC", "test");
			lcc.setParam("AC", -0.001);
			lcc.setParam("ABC", 10);
			lcc.setParam("AB", 0.001);
			lcc.setParam("CALL", "call", false, true);

			assertEquals("call", lcc.getParam(1).getValue());
			assertEquals("\"test\"", lcc.getParam(2).getValue());
			assertEquals("10", lcc.getParam(3).getValue());
			assertEquals("\"test\"", lcc.getParam(4).getValue());
			assertEquals("10", lcc.getParam(5).getValue());
			assertEquals("1234", lcc.getParam(6).getValue());
			assertEquals("0.001", lcc.getParam(7).getValue());
			assertEquals("-0.001", lcc.getParam(8).getValue());

			assertEquals(lcc.getParams("FUNC").length, 2);
			assertEquals(lcc.getParams("ABC").length, 2);

			System.out.println("  - code with parameters:\n" + lcc.toString());
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		}
	}

	@Test
	public void testWholeEncrypt() {
		String code = "res, ok = call(?, ?, ?, ?)";
		String expCode = "res, ok = call(?, ?, ?, ?)";
		LuaContractCode lcc = null;
		String password = "abcd1234";
		try {
			lcc = new LuaContractCode(code);
			lcc.setECK(password);
			lcc.setWholeEnc(true);
			assertEquals(expCode, lcc.getCode());
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		}

		try {
			lcc.setParam(1, "test", true);
			lcc.setParam(2, 10, true);
			lcc.setParam(3, "1234", true);
			lcc.setParam(4, 0.001, true);

			String base64cbytes = lcc.toString();
			System.out.println("  - code with parameters:\n" + base64cbytes);

			byte[] pbytes = Crypto.decryptAES256CBC(Codecs.BASE64.decode(base64cbytes), password);
			ByteBuffer bbuf = ByteBuffer.wrap(pbytes);
			bbuf.order(ByteOrder.LITTLE_ENDIAN);
			// read code
			int len = bbuf.getInt();
			byte[] bytes = new byte[len];
			bbuf.get(bytes);
			System.out.println("Code: " + new String(bytes));
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			assertEquals(true, false);
		}
	}

	@Test
	public void testParamEnc() {
		String code = "res, ok = call(\"test\", $A, $b12, $A)";
		String expCode = "res, ok = call(\"test\", ?, ?, ?)";
		LuaContractCode lcc = null;
		String password = "abcd1234";
		try {
			lcc = new LuaContractCode(code);
			lcc.setECK(password);
			assertEquals(expCode, lcc.getCode());
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		}

		try {
			lcc.setParam("A", 10, true);
			lcc.setParam(2, "1234");
			System.out.println("  - code with parameters:\n" + lcc.toString());

			ByteBuffer bbuf = ByteBuffer.wrap(lcc.toBytes());
			bbuf.order(ByteOrder.LITTLE_ENDIAN);
			
			// parameter count
			int cnt = bbuf.getShort();
			System.out.println("Parameter Count: " + cnt);

			// encrypted parameter count
			cnt = bbuf.getShort();
			System.out.println("Encrypted Parameter Count: " + cnt);

			// read code
			int len = bbuf.getInt();
			byte[] bytes = new byte[len];
			bbuf.get(bytes);
			System.out.println("Code: " + new String(bytes));

			// non-encrypted parameter chunk size
			len = bbuf.getInt();
			System.out.println("Non-encrypted parameter chunk size: " + len);

			// non-encrypted parameter chunk size
			bytes = new byte[len];
			bbuf.get(bytes);
			System.out.println("Non-encrypted parameter chunk: " + Codecs.BASE64.encode(bytes));

			// encrypted parameter chunk size
			len = bbuf.getInt();
			System.out.println("Encrypted parameter chunk size: " + len);

			// encrypted parameter chunk size
			byte[] ecbytes = new byte[len];
			bbuf.get(ecbytes);
			byte[] epbytes = Crypto.decryptAES256CBC(ecbytes, password);
			System.out.println("Encrypted parameter chunk: " + Codecs.BASE64.encode(epbytes));

			// read parameters
			bbuf = ByteBuffer.wrap(epbytes);
			bbuf.order(ByteOrder.LITTLE_ENDIAN);

			// parameter index 1
			// read index
			assertEquals(1, bbuf.getShort());
			// read type
			assertEquals(LuaContractCode.ParamType.INT.getValue(), bbuf.getShort());
			// read length of value
			int vallen = bbuf.getInt();
			System.out.println("[1] length of value: " + vallen);
			// read value
			byte[] val = new byte[vallen];
			bbuf.get(val);
			assertEquals("10", new String(val));

			// parameter index 3
			// read index
			assertEquals(3, bbuf.getShort());
			// read type
			assertEquals(LuaContractCode.ParamType.INT.getValue(), bbuf.getShort());
			// read length of value
			vallen = bbuf.getInt();
			System.out.println("[3] length of value: " + vallen);
			// read value
			val = new byte[vallen];
			bbuf.get(val);
			assertEquals("10", new String(val));
		} catch (CoinStackException e) {
			System.err.println(e.getDetailedMessage());
			e.printStackTrace();
			assertEquals(true, false);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			assertEquals(true, false);
		}
	}
}