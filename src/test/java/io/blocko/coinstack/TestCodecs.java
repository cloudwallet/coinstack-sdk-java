package io.blocko.coinstack;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.Utils;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet2Params;
import org.junit.Test;
import org.spongycastle.crypto.digests.RIPEMD160Digest;

import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.util.Codecs;
import io.blocko.coinstack.util.Codecs.BinaryCodec;
import io.blocko.coinstack.util.Codecs.DigestCodec;

public class TestCodecs {
	
	public static String EMPTY_STRING = "";
	public static String TIMESTAMP = String.valueOf(System.currentTimeMillis());
	public static String SAMPLE_INPUT = "17zaqbwaDb9UVajcSjNxb2nCzjksortjqq";
	static {
		//TIMESTAMP = EMPTY_STRING;
		//TIMESTAMP = SAMPLE_INPUT;
	}

	@Test
	public void testCodecs() throws Exception {
		System.out.println("# testCodecs");
		
		// compare sample testset with result of BinaryCodec
		testBinaryCodec();
		// compare sample testset with result of DigestCodec
		testDigestCodec();
		// compare sample testset with result of convertToAddress method
		testAddress();
		// compare sample testset with result of convertToPrivateKey method
		testPriv();
		// compare to external library
		testLib();
	}
	
	/**
	 * compare sample with result of external library (apache commons codec, spongycastle, bitcoinj)
	 * 
	 * @throws Exception
	 */
	public void testLib() throws Exception {
		System.out.println("## testLib: "+TIMESTAMP);
		testLibHex(TIMESTAMP);
		testLibBase64(TIMESTAMP);
		testLibBase58(TIMESTAMP);
		testLibBase58Checked(TIMESTAMP);
		testLibMd5(TIMESTAMP);
		testLibSha256(TIMESTAMP);
		testLibHash256(TIMESTAMP);
		testLibRipeMd160(TIMESTAMP);
		testLibHash160(TIMESTAMP);
		testLibAddress(TIMESTAMP);
		testLibPriv(TIMESTAMP);
	}
	
	/**
	 * compare sample testset with result of BinaryCodec
	 * 
	 * @throws Exception
	 */
	public void testBinaryCodec() throws Exception {
		System.out.println("## testBinaryCodec: (EMPTY_STRING)");
		testBinary(Codecs.HEX, EMPTY_STRING, "");
		testBinary(Codecs.BASE64, EMPTY_STRING, "");
		testBinary(Codecs.BASE58, EMPTY_STRING, "");
		testBinary(Codecs.BASE58CHECK, EMPTY_STRING, "3QJmnh");
		
		System.out.println("## testBinaryCodec: "+SAMPLE_INPUT);
		testBinary(Codecs.HEX, SAMPLE_INPUT, "31377a61716277614462395556616a63536a4e7862326e437a6a6b736f72746a7171");
		testBinary(Codecs.BASE64, SAMPLE_INPUT, "MTd6YXFid2FEYjlVVmFqY1NqTnhiMm5Demprc29ydGpxcQ==");
		testBinary(Codecs.BASE58, SAMPLE_INPUT, "27XpxGSsxzvFiB3DP2ntR1xvWehe42GvY1oWVMW4HSCBRHJ");
		testBinary(Codecs.BASE58CHECK, SAMPLE_INPUT, "8HGjrfFx45STPAPT5wRKdq6YtTxQBvZXkY8mTgfqVnW9tKBXaXeQ");
	}
	
	/**
	 * compare sample testset with result of DigestCodec
	 * 
	 * @throws Exception
	 */
	public void testDigestCodec() throws Exception {
		System.out.println("## testDigestCodec: (EMPTY_STRING)");
		testDigest(Codecs.MD5, EMPTY_STRING, "d41d8cd98f00b204e9800998ecf8427e");
		testDigest(Codecs.SHA256, EMPTY_STRING, "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
		testDigest(Codecs.HASH256, EMPTY_STRING, "5df6e0e2761359d30a8275058e299fcc0381534545f55cf43e41983f5d4c9456");
		testDigest(Codecs.RIPEMD160, EMPTY_STRING, "9c1185a5c5e9fc54612808977ee8f548b2258d31");
		testDigest(Codecs.HASH160, EMPTY_STRING, "b472a266d0bd89c13706a4132ccfb16f7c3b9fcb");
		
		System.out.println("## testDigestCodec: "+SAMPLE_INPUT);
		testDigest(Codecs.MD5, SAMPLE_INPUT, "3a0ed12740fb977bc96c211646d8df10");
		testDigest(Codecs.SHA256, SAMPLE_INPUT, "61c4fb3ab008f67c020dbf640fc1c89e52fda76dfcf2d62b66ab42c392687eab");
		testDigest(Codecs.HASH256, SAMPLE_INPUT, "5dcf67f9f5b73219b16b8ec97ddf86d3ccb014d0bb9875b4a1a08ae0638b3429");
		testDigest(Codecs.RIPEMD160, SAMPLE_INPUT, "246d2fe2fa62af0a7ae0e30d6c9f6c48330f0fd8");
		testDigest(Codecs.HASH160, SAMPLE_INPUT, "db02744856baf1ee91aa38feb12a840244387e82");
	}
	
	public void testAddress() {
		System.out.println("## testAddress");
		testAddress(EMPTY_STRING, "1HT7xU2Ngenf7D4yocz2SAcnNLW7rK8d4E");
		testAddress(SAMPLE_INPUT, "1Ly1vXxG93BVWmSzSnqTSUEkvcAVdoUmkJ");
		testAddress("asdf",       "1EoLDDePebAenBKLHSZscitjf4NjrUCpv5");
		testAddress("1234567890", "1NVNTvNtAL4Pqz94kXGBEHaAQWaC6BXSQE");
	}
	
	public void testPriv() throws Exception {
		System.out.println("## testPriv");
		testPriv(EMPTY_STRING, "1LCGKp8zkmU3jBSdsRNfLzqsJH3qSzSxyk");
		testPriv(SAMPLE_INPUT, "1Fo94iXHbV8XcxivmuYqWu9G2CmEbKnZZx");
		testPriv("asdf",       "1HBLQ2DXQujMpTTKVHP9ox6uRsZNx29vPs");
		testPriv("1234567890", "1EtYYPJQjQG4HhDaW4Zq62PAt3TVpxMm31");
	}
	
	
	/**
	 * compare sample testset with result of BinaryCodec
	 * 
	 * @param codec
	 * 			BinaryCodec
	 * @param input
	 * @param result
	 */
	public void testBinary(BinaryCodec codec, String input, String result) {
		System.out.println("- "+codec.toString()+": "+result);
		assertEquals(result, Codecs.encode(codec, input.getBytes()));
		assertTrue(Arrays.equals(input.getBytes(), Codecs.decode(codec, result)));
	}
	
	/**
	 * compare sample testset with result of DigestCodec
	 * 
	 * @param codec
	 * 			DigestCodec
	 * @param input
	 * @param resultHex
	 */
	public void testDigest(DigestCodec codec, String input, String resultHex) {
		System.out.println("- "+codec.toString()+": "+resultHex);
		assertEquals(resultHex, Hex.encodeHexString(Codecs.digest(codec, input.getBytes())));
	}
	
	/**
	 * compare sample testset with result of convertToAddress method
	 * 
	 * @param input
	 * 			sample source
	 * @param result
	 * 			converted address
	 */
	public void testAddress(String input, String result) {
		System.out.println("- ADDR: "+input+", "+result);
		String addr = Codecs.convertToAddress(input);
		assertEquals(addr, result);
	}
	
	/**
	 * compare sample testset with result of convertToPrivateKey method
	 * 
	 * @param input
	 * 			sample source
	 * @param result
	 * 			derived address from converted private key
	 * @throws MalformedInputException 
	 */
	public void testPriv(String input, String result) throws MalformedInputException {
		System.out.println("- PRIV: "+input+", "+result);
		String pkey = Codecs.convertToPrivateKey(input);
		String addr = ECKey.deriveAddress(pkey);
		assertEquals(addr, result);
	}
	
	
	/**
	 * compare HexCodec to external library (bitcoinj, apache commmons codec)
	 * 
	 * @param input
	 * @throws MalformedInputException
	 * @throws DecoderException
	 */
	public void testLibHex(String input) throws MalformedInputException, DecoderException {
		//System.out.println("- testLibHex: "+input);
		String encoded = Codecs.HEX.encode(input.getBytes());
		String decoded = Codecs.HEX.decodeToString(encoded);
		System.out.println("- testLibHex: "+encoded);
		assertEquals(decoded, input);
		
		String bcjEncoded = Utils.HEX.encode(input.getBytes());
		String bcjDecoded = new String(Utils.HEX.decode(bcjEncoded));
		assertEquals(bcjDecoded, input);
		assertEquals(bcjEncoded, encoded);
		
		String apcEncoded = Hex.encodeHexString(input.getBytes());
		String apcDecoded = new String(Hex.decodeHex(apcEncoded.toCharArray()));
		assertEquals(apcDecoded, input);
		assertEquals(apcEncoded, bcjEncoded);
	}
	
	/**
	 * compare Base64Codec to external library (bitcoinj, apache commmons codec)
	 * 
	 * @param input
	 * @throws MalformedInputException
	 * @throws DecoderException
	 */
	public void testLibBase64(String input) throws MalformedInputException, DecoderException {
		//System.out.println("- testLibBase64: "+input);
		String encoded = Codecs.BASE64.encode(input.getBytes());
		String decoded = Codecs.BASE64.decodeToString(encoded);
		System.out.println("- testLibBase64: "+encoded);
		assertEquals(decoded, input);
		
		String apcEncoded = Base64.encodeBase64String(input.getBytes());
		String apcDecoded = new String(Base64.decodeBase64(apcEncoded));
		assertEquals(apcDecoded, input);
		assertEquals(apcEncoded, encoded);
	}
	
	/**
	 * compare Base58Codec to external library (bitcoinj)
	 * 
	 * @param input
	 * @throws AddressFormatException
	 */
	public void testLibBase58(String input) throws AddressFormatException {
		//System.out.println("- testLibBase58: "+input);
		byte[] inputBytes = Codecs.HASH160.digest(input.getBytes());
		
		// encode
		String inputHex = Codecs.HEX.encode(inputBytes);
		String cduEncoded = Codecs.BASE58.encode(inputBytes);
		String bcjEncoded = Base58.encode(inputBytes);
		System.out.println("- testLibBase58: "+cduEncoded);
		assertEquals(cduEncoded, bcjEncoded);
		
		// decode
		String cduDecoded = Codecs.HEX.encode(Codecs.BASE58.decode(cduEncoded));
		String bcjDecoded = Codecs.HEX.encode(Base58.decode(bcjEncoded));
		assertEquals(cduDecoded, bcjDecoded);
		assertEquals(inputHex, bcjDecoded);
		
		// decodeToBigInteger
		BigInteger cduBInt = Codecs.BASE58.decodeToBigInteger(cduEncoded);
		BigInteger bcjBInt = Base58.decodeToBigInteger(cduEncoded);
		assertEquals(cduBInt, bcjBInt);
		assertEquals(String.valueOf(cduBInt), String.valueOf(bcjBInt));
	}
	
	/**
	 * compare Base58CheckedCodec to external library (bitcoinj)
	 * 
	 * @param input
	 * @throws AddressFormatException
	 */
	public void testLibBase58Checked(String input) throws AddressFormatException {
		//System.out.println("- testLibBase58Checked: "+input);
		byte[] inputBytes = Codecs.HASH160.digest(input.getBytes());
		
		// encode
		String inputHex = Codecs.HEX.encode(inputBytes);
		String cduEncoded = Codecs.BASE58CHECK.encode(Codecs.versionedMainnet(inputBytes));
		String bcjEncoded = new Address(MainNetParams.get(), inputBytes).toString();
		System.out.println("- testLibBase58Checked: "+cduEncoded);
		assertEquals(cduEncoded, bcjEncoded);
		String cduEncodedTNet = Codecs.BASE58CHECK.encode(Codecs.versionedBytes(Codecs.VERSION_TESTNET, inputBytes));
		String bcjEncodedTNet = new Address(TestNet2Params.get(), inputBytes).toString();
		System.out.println("- testLibBase58Checked: "+cduEncodedTNet);
		assertEquals(cduEncodedTNet, bcjEncodedTNet);
		
		// decode
		String cduDecoded = Codecs.HEX.encode(Codecs.BASE58CHECK.decode(cduEncoded));
		String bcjDecoded = Codecs.HEX.encode(Base58.decodeChecked(bcjEncoded));
		String cduUnversioned = Codecs.HEX.encode(Codecs.unversionedBytes(Codecs.BASE58CHECK.decode(cduEncoded)));
		byte cduVersion = Codecs.BASE58CHECK.decode(cduEncoded)[0];
		assertEquals(cduDecoded, bcjDecoded);
		assertEquals(inputHex, bcjDecoded.substring(2));
		assertEquals(inputHex, cduUnversioned);
		assertEquals(Codecs.VERSION_MAINNET, cduVersion);
	}
	
	// digest
	/**
	 * compare MD5Codec to external library (apache commons codec)
	 * 
	 * @param input
	 */
	public void testLibMd5(String input) {
		byte[] cduDigested = Codecs.MD5.digest(input.getBytes());
		byte[] apcDigested = DigestUtils.md5(input);
		System.out.println("- testLibMd5: "+Codecs.HEX.encode(cduDigested));
		assertTrue(Arrays.equals(cduDigested, apcDigested));
	}
	/**
	 * compare SHA256Codec to external library (apache commons codec)
	 * 
	 * @param input
	 */
	public void testLibSha256(String input) {
		byte[] cduDigested = Codecs.SHA256.digest(input.getBytes());
		byte[] apcDigested = DigestUtils.sha256(input);
		System.out.println("- testLibSha256: "+Codecs.HEX.encode(cduDigested));
		assertTrue(Arrays.equals(cduDigested, apcDigested));
	}
	/**
	 * compare HASH256Codec to external library (apache commons codec)
	 * 
	 * @param input
	 */
	public void testLibHash256(String input) {
		byte[] cduDigested = Codecs.HASH256.digest(input.getBytes());
		byte[] apcBuffer = DigestUtils.sha256(input.getBytes());
		byte[] apcDigested = DigestUtils.sha256(apcBuffer);
		System.out.println("- testLibHash256: "+Codecs.HEX.encode(cduDigested));
		assertTrue(Arrays.equals(cduDigested, apcDigested));
	}
	/**
	 * compare RIPEMD160Codec to external library (spongycastle)
	 * 
	 * @param input
	 */
	public void testLibRipeMd160(String input) {
		byte[] data = input.getBytes();
		byte[] cduDigested = Codecs.RIPEMD160.digest(data);
        RIPEMD160Digest digest = new RIPEMD160Digest();
        digest.update(data, 0, data.length);
        byte[] buf = new byte[20];
        digest.doFinal(buf, 0);
        byte[] scDigested = buf;
		System.out.println("- testLibRipeMd160: "+Codecs.HEX.encode(cduDigested));
		assertTrue(Arrays.equals(cduDigested, scDigested));
	}
	/**
	 * compare HASH160Codec to external library (bitcoinj)
	 * 
	 * @param input
	 */
	public void testLibHash160(String input) {
		//System.out.println("- testLibSha256Hash160: "+input);
		byte[] cduDigested = Codecs.HASH160.digest(input.getBytes());
		byte[] bcjDigested = Utils.sha256hash160(input.getBytes());
		System.out.println("- testLibHash160: "+Codecs.HEX.encode(cduDigested));
		assertTrue(Arrays.equals(cduDigested, bcjDigested));
		String cduAddr1 = Codecs.encodeBase58CheckMainnet(Codecs.HASH160.digest(input.getBytes()));
		String cduAddr2 = new Address(MainNetParams.get(), cduDigested).toString();
		String bcjAddr = new Address(MainNetParams.get(), bcjDigested).toString();
		assertEquals(cduAddr1, cduAddr2);
		assertEquals(cduAddr1, bcjAddr);
	}
	
	
	// address
	public void testLibAddress(String input) {
		//System.out.println("- testLibAddress: "+input);
		String addr = Codecs.convertToAddress(input);
		System.out.println("- testLibAddress: "+addr);
		byte[] hash160 = Utils.sha256hash160(input.getBytes());
		String btjAddr = new Address(MainNetParams.get(), hash160).toString();
		assertEquals(addr, btjAddr);
	}
	
	public static byte[] messageDigest(String algorithm, byte[] data, int offset, int len) {
		try {
			MessageDigest digest;
			digest = MessageDigest.getInstance(algorithm);
			digest.update(data, offset, len);
			return digest.digest();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	// private
	public void testLibPriv(String input) throws MalformedInputException {
		BigInteger priv = Codecs.convertToPrivate(input);
		String pkey = ECKey.fromPrivate(priv);
		String addr = ECKey.deriveAddress(pkey);
		System.out.println("- testLibPriv: "+addr+" (derived address)");
		assertEquals(pkey, Codecs.convertToPrivateKey(input));
	}
}
