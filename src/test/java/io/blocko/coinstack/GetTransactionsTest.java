package io.blocko.coinstack;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Transaction;

public class GetTransactionsTest {
	
	//public static final CoinStackClient LOCAL_DEVS = new CoinStackClient("http://localhost:3000");
	public static final CoinStackClient TESTCHAIN = new CoinStackClient("http://52.78.93.213:8080");
	
	
	final static int SKIP_NONE = 0;
	final static int FLAG_ALL = -1;
	final static int IS_CONFIRMED = 1;
	final static int NOT_CONFIRMED = 0;
	
	
	CoinStackClient coinstack = null;
	
	@Before
	public void before() {
		//coinstack = LOCAL_DEVS;
		coinstack = TESTCHAIN;
	}
	@After
	public void after() {
		coinstack.close();
	}
	
	@Test
	public void test() throws Exception {
		System.out.println("# "+this.getClass().getSimpleName());
		
		String testerPkey = "L3JXy3787QNCgGDmbtintqJEFiUQW3BbWdR8wvoXcqA7hFZVgqJv"; // 1QxotcrMi5RzeXmenPan61Eydci2Tftx7
		String temporalReceiverPkey = ECKey.createNewPrivateKey();
		temporalReceiverPkey = "Kz57tobWZRt8vFStVGRpu4xvDtWHBpzhcgkYSMb46eZKQeTwDMT9"; // 198VpuJxdwLSCdzTAnLSq9LL73tAsTddZh
		
		String testerAddr = ECKey.deriveAddress(testerPkey);
		String temporalReceiverAddr = ECKey.deriveAddress(temporalReceiverPkey);
		
		System.out.printf("- testAccount: %s, %s\n", testerAddr, testerPkey);
		System.out.printf("- recvAccount: %s, %s\n", temporalReceiverAddr, temporalReceiverPkey);
		
		
		// private key for address '12NziPURoP4H7rr6WBpyjhepBptjYBZavJ'
		String initPkey = "L1g2876SGrCNSgK7VB5WBVdx1EvYrwe6h5ydHtm7AgRaHz4AyQtL"; // SECRET
		if (initPkey != null && !initPkey.isEmpty()) {
			// if tester address is empty, then send some coin to test.
			if (coinstack.getBalance(testerAddr) < CoinMath.convertToSatoshi("0.1")) {
				sendTo(initPkey, testerAddr, "1", "0.0001", false);
			}
		}
		// check skip conditions
		if (checkTestSkip(testerAddr)) {
			// skipped
			return;
		}
		
		int txcount = 10;
		int limit = 7;
		int test_repeats = 1;
		long test_sleep = 5000;
		
		// unconfirmed txs for test
		if (!hasUnconfirmedTx(temporalReceiverAddr)) {
			// generate txs for test
			generateTxs(txcount, testerPkey, temporalReceiverAddr);
		}
		for (int i=0; i<test_repeats; i++) {
			if (i > 0) {
				Thread.sleep(test_sleep);
			}
			getTxs(temporalReceiverAddr);
			getTxsPaging(temporalReceiverAddr, limit);
			getTxsNotConfirmed(temporalReceiverAddr);
			getTxsConfirmed(temporalReceiverAddr, limit);
		}
	}
	
	
	
	public boolean checkTestSkip(String testerAddr) throws IOException, CoinStackException {
		if (coinstack.getBalance(testerAddr) == 0) {
			System.out.println("- skipped: empty address, balance is zero, "+testerAddr);
			return true;
		}
		String[] txs = coinstack.getTransactions(testerAddr);
		if (txs.length == 0) {
			System.out.println("- skipped: address has no transactions, "+testerAddr);
			return true;
		}
		String[] confirmedTx = coinstack.getTransactions(testerAddr, SKIP_NONE, FLAG_ALL, IS_CONFIRMED);
		String[] mempoolTx = coinstack.getTransactions(testerAddr, SKIP_NONE, FLAG_ALL, NOT_CONFIRMED);
		if (confirmedTx.length + mempoolTx.length != txs.length) {
			System.out.println("- skipped: backend does not support getTransaction method with skip, limit, confirmed");
			return true;
		}
		return false;
	}
	public boolean hasUnconfirmedTx(String addr) throws CoinStackException, IOException {
		String[] txs = coinstack.getTransactions(addr);
		if (txs == null || txs.length == 0) {
			return false;
		}
		Transaction tx = coinstack.getTransaction(txs[0]);
		int[] heights = tx.getBlockHeights();
		int height = (heights == null || heights.length == 0) ? -1 : heights[0];
		return (height < 0);
	}
	
	public String sendTo(String pkey, String receiver, String amount, String fee, boolean verbose) throws CoinStackException, IOException {
		TransactionBuilder tx = new TransactionBuilder();
		tx.addOutput(receiver, CoinMath.convertToSatoshi(amount));
		tx.setFee(CoinMath.convertToSatoshi(fee));
		String rawTx = tx.buildTransaction(coinstack, pkey);
		String txHash = TransactionUtil.getTransactionHash(rawTx, true);
		coinstack.sendTransaction(rawTx);
		if (verbose) {
			System.out.printf("- sendTo: addr=%s, amount=%s, fee=%s, txId=%s\n", receiver, amount, fee, txHash);
		}
		return txHash;
	}
	
	public void prepareCoinToTestAddress(String pkey, String receiver) throws CoinStackException, IOException {
		System.out.println("## prepareCoinToTestAddress: "+receiver);
		sendTo(pkey, receiver, "10", "0.0001", true);
	}
	public void generateTxs(int count, String pkey, String receiver) throws CoinStackException, IOException {
		System.out.println("## generateTxs: "+receiver+", count="+count);
		for (int i=0; i<count; i++) {
			sendTo(pkey, receiver, "0.0001", "0.0001", (i%10 == 0));
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
	public void getTxs(String addr) throws CoinStackException, IOException {
		System.out.println("## getTxsAllConfirmed: "+addr);
		String[] txs = coinstack.getTransactions(addr);
		String[] txsAll = coinstack.getTransactions(addr, SKIP_NONE, FLAG_ALL, FLAG_ALL);
		String[] txsIsConfirmed = coinstack.getTransactions(addr, SKIP_NONE, FLAG_ALL, IS_CONFIRMED);
		String[] txsNotConfirmed = coinstack.getTransactions(addr, SKIP_NONE, FLAG_ALL, NOT_CONFIRMED);
		System.out.println("- txs.len="+txs.length);
		System.out.println("- txsAll.len="+txsAll.length);
		System.out.println("- txsIsConfirmed.len="+txsIsConfirmed.length);
		System.out.println("- txsNotConfirmed.len="+txsNotConfirmed.length);
		assertEquals(txs.length, txsAll.length);
		assertEquals(txs.length, txsIsConfirmed.length+txsNotConfirmed.length);
	}
	public void getTxsPaging(String addr, int limit) throws CoinStackException, IOException {
		System.out.println("## getTxsPaging: "+addr+", limit="+limit);
		getTxsPaging(addr, limit, FLAG_ALL);
		getTxsPaging(addr, limit, IS_CONFIRMED);
		getTxsPaging(addr, limit, NOT_CONFIRMED);
	}
	public void getTxsPaging(String addr, int limit, int confirmed) throws CoinStackException, IOException {
		System.out.println("### getTxsPaging: "+addr+", limit="+limit+", confirmed="+confirmed);
		Set<String> utx = new HashSet<String>();
		String[] txs = coinstack.getTransactions(addr, SKIP_NONE, FLAG_ALL, confirmed);
		System.out.println("- txs.len="+txs.length);
		System.out.println("- utx");
		int total = 0;
		int skip = 0;
		while (true) {
			String[] paged = coinstack.getTransactions(addr, skip, limit, confirmed);
			if (skip < limit*2 || skip > (txs.length-limit*2)) {
				System.out.printf("  paging: skip=%d, limit=%d, get=%d\n", skip, limit, paged.length);
			}
			else if (skip == limit*3) {
				System.out.printf("  ...\n");
			}
			for (String tx : paged) {
				utx.add(tx);
			}
			total += paged.length;
			skip += limit;
			if (paged.length < limit) {
				break;
			}
		}
		assertEquals(total, txs.length);
		System.out.println("  size="+utx.size());
	}
	public void getTxsNotConfirmed(String addr) throws CoinStackException, IOException {
		System.out.println("### getTxsNotConfirmed: "+addr);
		String[] txs = coinstack.getTransactions(addr, SKIP_NONE, FLAG_ALL, NOT_CONFIRMED);
		System.out.println("- txs.len="+txs.length);
		System.out.println("- unconfirmedTxs");
		String[] unconfirmedTxs = coinstack.getTransactionsNotConfirmed(addr);
		System.out.println("  size="+unconfirmedTxs.length);
		assertEquals(txs.length, unconfirmedTxs.length);
		Arrays.sort(txs);
		Arrays.sort(unconfirmedTxs);
		assertTrue(Arrays.equals(txs, unconfirmedTxs));
	}
	public void getTxsConfirmed(String addr, int limit) throws CoinStackException, IOException {
		System.out.println("### getTxsConfirmed: "+addr+", limit="+limit);
		String[] txs = coinstack.getTransactionsConfirmed(addr, SKIP_NONE, FLAG_ALL);
		System.out.println("- txs.len="+txs.length);
		System.out.println("- confirmedTxs");
		List<String> buf = new ArrayList<String>();
		int skip = 0;
		while (true) {
			String[] paged = coinstack.getTransactionsConfirmed(addr, skip, limit);
			if (skip < limit*2 || skip > (txs.length-limit*2)) {
				System.out.printf("  paging: skip=%d, limit=%d, get=%d\n", skip, limit, paged.length);
			}
			else if (skip == limit*3) {
				System.out.printf("  ...\n");
			}
			for (String tx : paged) {
				buf.add(tx);
			}
			skip += limit;
			if (paged.length < limit) {
				break;
			}
		}
		String[] confirmedTxs = buf.toArray(new String[0]);
		System.out.println("  size="+txs.length);
		assertEquals(buf.size(), txs.length);
		assertTrue(Arrays.equals(confirmedTxs, txs));
	}
}
