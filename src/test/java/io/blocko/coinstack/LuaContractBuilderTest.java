package io.blocko.coinstack;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack.util.Codecs;

public class LuaContractBuilderTest {
	
	
	CoinStackClient coinstack = null;
	
	@Before
	public void before() {
		coinstack = new CoinStackClient(null, StampTest.TESTCHAIN);
	}

	@Test
	public void testMain() throws Exception {
		try {
			testInitial();
			String txId = testDef();
			testViewCode(txId);
		} catch (CoinStackException e) {
			System.err.printf("%s: %s\n", e.getMessage(), e.getDetailedMessage());
		}
	}
	public void testInitial() throws CoinStackException {
		LuaContractBuilder lcBuilder = new LuaContractBuilder();
		assertTrue(lcBuilder.getFee() > 0);
		assertTrue(lcBuilder.getContractId() == null);
		assertTrue(lcBuilder.getDefinitionCode() == null);
		assertTrue(lcBuilder.getExecutionCode() == null);
	}
	
	
	public String TEST_CODE = "";
	{
		TEST_CODE = "";
		TEST_CODE += "local system = require(\"system\");\n";
		TEST_CODE += "function foo(msg)\n";
		TEST_CODE += "\tsystem.print(msg .. \" from lua contract\");\n";
		TEST_CODE += "\treturn msg .. \" from lua contract\"\n";
		TEST_CODE += "end";
	}
	
	public String testDef() throws IOException, CoinStackException {
		String privateKeyWIF = "L3Q7j66G6yFpJrNrkVwg1ThjamLR1W8brcj5ExzwMfdXH4Sfvb2V";
		String address = "12HVtCxE9wBtASVWavzYGo68Beh1Hd4JB2";
		
		LuaContractBuilder lcBuilder = new LuaContractBuilder();
		lcBuilder.setContractId(address);
		lcBuilder.setDefinition(TEST_CODE);
		String rawTx = lcBuilder.buildTransaction(coinstack, privateKeyWIF);
		String txHash = TransactionUtil.getTransactionHash(rawTx);
		System.out.println("- tx: "+txHash);
		coinstack.sendTransaction(rawTx);
		return txHash;
	}
		
	public void testViewCode(String txHash) throws IOException, CoinStackException, JSONException {
		Transaction tx = coinstack.getTransaction(txHash);
		Output[] outputs = tx.getOutputs();
		for (Output output : outputs) {
			byte[] data = output.getData();
			if (data == null) {
				continue;
			}
			String hexData = Codecs.HEX.encode(data);
			//System.out.println("  hex:  "+hexData);
			if (hexData.length() < 12) {
				continue;
			}
			String marker = hexData.substring(0, 4);
			String version = hexData.substring(4, 8);
			String type = hexData.substring(8, 12);
			String hash = hexData.substring(12, 12+64);
			String body = hexData.substring(12+64);
			byte[] decoded = Codecs.HEX.decode(body);
			String dataUri = new String(decoded);
			assertEquals(hash, Codecs.SHA256.digestEncodeHex(decoded));
			String code = getContractCode(dataUri);
			if (marker.equals("4f43")) {
				System.out.printf("  mark: %s,%s,%s\n", marker, version, type);
				System.out.println("  hash: "+hash);
				System.out.println("  data: "+dataUri);
				System.out.println("  code: \n"+code);
			}
			assertEquals(TEST_CODE, code);
		}
	}
	public static String getContractCode(String dataUri) throws JSONException {
		String rawData = dataUri.substring(dataUri.indexOf("base64,")+7);
		String jsonBody = Codecs.BASE64.decodeToString(rawData);
		JSONObject contractBody = new JSONObject(jsonBody);
		String contractBytes = contractBody.optString("body");
		int contractVersion = contractBody.getInt("version");
		byte[] contract = Codecs.BASE64.decode(contractBytes);
		String code = getContractCode(contractVersion, contract);
		return code;
	}
	public static String getContractCode(int version, byte[] contract) {
		if (version < 1) {
			return new String(contract);
		}
		ByteBuffer bbuf = ByteBuffer.wrap(contract);
		bbuf.order(ByteOrder.LITTLE_ENDIAN);
		short paramsCnt = bbuf.getShort();
		short eparamsCnt = bbuf.getShort();
		int codeLen = bbuf.getInt();
		System.out.printf("contract: version=%d, paramsCnt=%d, eparamsCnt=%d, codeLen=%d\n",
				version, paramsCnt, eparamsCnt, codeLen);
		byte[] codeChunk = new byte[codeLen];
		bbuf.get(codeChunk);
		String code = new String(codeChunk);
		return code;
	}
}
