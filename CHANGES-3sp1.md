# CHANGES (3sp1)

## 3.5.4

- add contract parsing sample
- enlarge tx payload size
  - increase maximum tx size and maximum payload size
  - add method to set maximum size of tx and metadata payload


## 3.5.3

- Fix to set the options before the data entry.
- update library: org.json
- update shade plug-in and add option about resource transformer
  - transform package name of services resource


## 3.5.2

(skip)


## 3.5.1

- add addOutput method to txbuilder for low level API
- use light encoded contract body without base64 encoding
- cached wallet
  - remove transactions immediately from cached wallet
  - add cachedWallet to utxoCache
- reusable transaction builder
  - implement reset method for reusable TransactionBuilder


## 3.5.0

- intelligent tx fee controll for large payload and dusty change output
- improve error handling
  - detailed exception message
  - use parseError function in JsonToModel class
