# CHANGES

## 3.0.27 (2017-11-27)

### Added

- add backend adaptor using websocket
- add OP_PUSHDATA4 to support large payload
- add utxocache to build transaction 

### Changed

- update version of http client
- improve transaction builder to build faster


## 3.0.26 (2017-10-13)

### Added

- add lua smart contract builder
- add default implementation of endpoint
- add methods to control utxo in transaction builder
- add method to fetch raw tx in coinstack client
- add method to query contract in coinstack client


### Changed

- fix hmac signature creation issue, use authority to generate auth string instead of host
- change class name of Math to CoinMath, Math class will be deprecated due to the inconvenience of using the same name as java.lang.Math
- refactoring binary and digest codecs


## 3.0.25 (2017-04-11)

- refactored http request
- removed unnecessary eclipse config file '.project'
- moved classes of util.codec package to the inner classes of 'Codecs'
- fixed name of HASH160 and RIPEMD160
- changed to use hmac sign when there is proper credentials
- added methods to create private key from specific source
- added getTransactionsNotConfirmed and getTransactionsConfirmed, but mainnet does not support


## 3.0.24 (2017-02-20)

- added allowLargePayload method to TransactionBuilder
- added Codecs class to support binary and digest codecs
- refactored getBlockchainStatus in CoinStackClient
- changed javadoc stylesheet to java 8 style
- changed to use testchain in unit tests
- merged GS branch
